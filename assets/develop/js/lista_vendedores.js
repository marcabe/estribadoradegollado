import { mostrarmensajes, funcionesxhr, errorcon } from './mensajes.js';

$(document).ready(function () {
	$(".editar").on("click", function () {
		window.location = "../Vendedores/editar/" + $(this).attr("data-vendedor");
	});

	$(document).on("click", ".compras", function () {
		window.location = "comisiones/" + $(this).attr("data-vendedor");
	});

	let tbl_vendedores = $('#tbl_general').DataTable({
		"lengthChange": false,
		"language": {
			"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
		}
	});

	$(".stat_m").on("click", function () {
		let vendedor = $(this).attr("data-content");
		let favorito = 0;
		if ($(this).hasClass("fa-user-times")) {
			$(this).removeClass("fa-user-times");
			$(this).addClass("fa-user-check");
			favorito = 1;
		} else if ($(this).hasClass("fa-user-check")) {
			$(this).removeClass("fa-user-check");
			$(this).addClass("fa-user-times");
			favorito = 0;
		}
		$.ajax({
			type: "POST",
			url: "../Vendedores/update/" + vendedor,
			data: {
				status: parseInt(favorito)
			}
		});
	});


	/*
	$(".editar").on("click", function () {
		window.location = "../Usuarios/editar/" + $(this).attr("id");
	});
	*/

});
