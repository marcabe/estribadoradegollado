import { mostrarmensajes, funcionesxhr, errorcon } from './mensajes.js';

$(document).ready(function () {



	let tbl_clientes = $('#tblgeneral').DataTable({
		"lengthChange": false,
		"language": {
			"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
		},
		columns: [{
			data: "nombre"
		},
		{
			data: "telefono"
		},
		{
			data: "credito"
		},
		{
			data: "credito_usado"
		},
		{
			data: "correo"
		},
		{
			data: "btn"
		}
		]
	});



	$(".btn_clientes_pagos").on("click", function () {
		let plazo = $(this).attr("data-plazo");
		tbl_clientes.clear().draw();
		$.ajax({
			type: "POST",
			url: "../Clientes/filtro_plazo/" + plazo,
			dataType: "json",
			success: function (response) {
				console.log(response);
				$.each(response, function (i, e) {
					var rowNode = tbl_clientes
						.row.add(e)
						.draw()
						.node();
				});
				$(tbl_clientes.column(2).header()).text('CREDITO');
				$(tbl_clientes.column(3).header()).text('CREDITO USADO');
				$(tbl_clientes.column(4).header()).text('CORREO');

			},
			xhr: function () {
				var xhr = $.ajaxSettings.xhr();
				xhr.onloadstart = function (e) {
					$("#cargando_info").show();
					$("#contenerdor_principal").hide();
				};
				xhr.onloadend = function (e) {
					$("#cargando_info").hide();
					$("#contenerdor_principal").show();
				}
				return xhr;
			}
		});
		$("#titulo_table").text("Posible Pago");
	});


	$(".editar").on("click", function () {
		window.location = "../clientes/editar/" + $(this).attr("id");
	});
	$(".eliminar").on("click", function () {
		let elemento = $(this);
		let conectividad = 1;
		let idcliente = $(this).attr("id");
		$.ajax({
			url: '../Clientes/update/' + idcliente,
			data: {
				status: 0
			},
			type: 'POST',
			success: function (response) {
				elemento.parent().parent().parent().parent().remove();
			},
			error: function (x, t, m) {
				mostrarmensajes();
				conectividad = errorcon(t, conectividad)
			},
			xhr: function () {
				mostrarmensajes();
				var xhr = $.ajaxSettings.xhr();
				xhr.upload.onprogress = function (e) {
					funcionesxhr(conectividad, "Cargando información");
				};
				xhr.onloadstart = function (e) {
					funcionesxhr(conectividad, "Iniciando proceso");
				};
				xhr.onloadend = function (e) {
					if (conectividad != 0) {
						$("#bloquear").hide();
						$("#msjsys").hide();
						$("#lblmsntop").hide();
					}

				};
				return xhr;
			}
		});
	});


	$("#tblgeneral tr").on("click", function () {
		$("#tblgeneral tr").removeClass("highlight");
		$(this).addClass("highlight");
	});

	$(document).on("click", ".compras", function () {
		window.location = "../clientes/compras/" + $(this).attr("data-idcliente");
	});

});
