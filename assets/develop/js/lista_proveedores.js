import { mostrarmensajes, funcionesxhr, errorcon } from './mensajes.js';


$(document).ready(function () {

	$('#tblgeneral').DataTable({
		"language": {
			"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
		}
	});

	$(".editar").on("click", function (){
		window.location = "../proveedores/editar/"+$(this).attr("id");
	});
	$(".eliminar").on("click", function (){
		let conectividad =1;
		let elemento = $(this);
		let idproveedor = $(this).attr("id");
		$.ajax({
			url: '../Proveedores/update/'+idproveedor,
			data: {
				status: 0
			},
			type: 'POST',
			success: function (response) {
				elemento.parent().parent().parent().parent().remove();
			},
			error: function (x, t, m) {
				mostrarmensajes();
				conectividad = errorcon(t, conectividad)
			},
			xhr: function () {
				mostrarmensajes();
				var xhr = $.ajaxSettings.xhr();
				xhr.upload.onprogress = function (e) {
					funcionesxhr(conectividad, "Cargando información");
				};
				xhr.onloadstart = function (e) {
					funcionesxhr(conectividad, "Iniciando proceso");
				};
				xhr.onloadend = function (e) {
					if (conectividad != 0) {
						$("#bloquear").hide();
						$("#msjsys").hide();
						$("#lblmsntop").hide();
					}

				};
				return xhr;
			}
		});
	});


	$("#tblgeneral tr").on("click",function() {
		$("#tblgeneral tr").removeClass("highlight");
		$(this).addClass("highlight");
	});
});
