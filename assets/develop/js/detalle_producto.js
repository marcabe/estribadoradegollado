$(document).ready(function () {

	$(document).on("blur", "#menudeo", function () {
		$(this).val(accounting.formatMoney($("#menudeo").val()));
	});
	$(document).on("blur", "#mayoreo", function () {
		$(this).val(accounting.formatMoney($("#mayoreo").val()));
	});

	$(".menudeo").each(function () {
		$(this).html(accounting.formatMoney($(this).html()));
	});

	$(".mayoreo").each(function () {
		$(this).html(accounting.formatMoney($(this).html()));
	});

	$('#tblgeneral').DataTable({
		"searching":false,
		"paging": false,
		"info":false,
		"language": {
			"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
		}
	});
	$(".eliminar").on("click", function (){
		let elemento = $(this);
		let idpresentacion = $(this).attr("id");
		$.ajax({
			url: '../../Presentaciones/update/'+idpresentacion,
			data: {
				status: 0
			},
			type: 'POST',
			success: function (response) {
				elemento.parent().parent().parent().parent().remove();
			}
		});
	});

	$("#regresar").on("click", function (){
		window.location="../lista";
	});
});
