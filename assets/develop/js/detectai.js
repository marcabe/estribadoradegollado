function showStatus(online) {
    if (online) {
		$("#bloquear").hide();
		$("#msjsys").hide();
		$("#lblmsntop").hide();
    } else {
		$("#bloquear").show();
		$("#msjsys").show();
		$("#lblmsntop").show();
		$("#lblmsntop").text("Sin acceso a internet");
    }
}

window.addEventListener('load', () => {
    // 1st, we set the correct status when the page loads
    navigator.onLine ? showStatus(true) : showStatus(false);

    // now we listen for network status changes
    window.addEventListener('online', () => {
        showStatus(true);
    });

    window.addEventListener('offline', () => {
        showStatus(false);
    });
});