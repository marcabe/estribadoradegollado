$(document).ready(function () {
	productoglobal = '';
	idvendedorglobal = '';

	const mostrarmensajes = () => {
		$("#bloquear").show();
		$("#msjsys").show();
		$("#lblmsntop").show();
		return;
	}

	const funcionesxhr = (conectividad, mensaje) => {
		if (conectividad == 0) {
			$("#lblmsntop").text("No hay acceso a internet");
		} else {
			$("#lblmsntop").text(mensaje);
		}
		return;
	}
	const errorcon = (t, conectividad = 1) => {
		$("#lblmsntop").text("Error en la peticion al servidor");
		if (t === "timeout") {
			console.log("Error en la conexion a internet");
		} else {
			conectividad = 0;
		}
		return conectividad;
	}


	$("#pespecial").on("click", function () {
		let check = $(this).prop('checked');
		if (check == true) {
			$("#subtotal").attr("readonly", false);
			$("#costopresentacion").hide();
			$("#costopresentacionespecial").show();
			$("#textopresentacion").text("Costo unitario especial");
		} else {
			$("#subtotal").attr("readonly", true);
			$("#costopresentacion").show();
			$("#costopresentacionespecial").hide();
			$("#textopresentacion").text("Costo presentacion:");
		}
	});

	$("#subtotal").on("blur", function () {
		$(this).val(accounting.formatMoney($("#subtotal").val()));
	});

	let total = accounting.formatMoney($("#total").text());
	$("#total").text(total);
	$('#productos').select2({
		placeholder: '--- Seleccione un producto ---',
		ajax: {
			url: '../Productos/get_like',
			dataType: 'json',
			delay: 250,
			processResults: function (response) {
				console.log(response);
				return {
					results: response
				};
			}
		}
	});

	$('#cliente').select2({
		placeholder: '--- Seleccione un cliente ---',
		ajax: {
			url: '../Clientes/get_like',
			dataType: 'json',
			delay: 250,
			processResults: function (response) {
				return {
					results: response
				};
			}
		}
	});
	$('#cliente').on('select2:select', function (e) {
		var data = e.params.data;
		idclienteglobal = data.id;
	});

	$('#vendedor').select2({
		placeholder: '--- Seleccione un cliente ---',
		ajax: {
			url: '../Vendedores/get_like',
			dataType: 'json',
			delay: 250,
			processResults: function (response) {
				return {
					results: response
				};
			}
		}
	});
	$('#vendedor').on('select2:select', function (e) {
		var data = e.params.data;
		idvendedorglobal = data.id;
	});



	$('#productos').on('select2:select', function (e) {
		var data = e.params.data;
		let idproducto = data.id;
		idproductoglobal = idproducto;
		productoglobal = data.text;
		productoglobal = productoglobal.split("(", 3);
		productoglobal = productoglobal[0];

		$.ajax({
			url: "../Presentaciones/get_where",
			type: "POST",
			dataType: "json",
			data: {
				idproducto: idproducto,
				status: 1
			},
			success: function (response) {
				$("#presentacion").empty();
				$("#presentacion").append("<option value=0>--- Selecciona una presentacion ---</option>");
				$.each(response, function (i, d) {
					$("#presentacion").append(`<option value="${d.idpresentacion}">${d.presentacion}</option>`);
				});
			}
		});
	});

	$("#presentacion").on("change", function () {
		$.ajax({
			url: "../Presentaciones/get_where",
			type: "POST",
			dataType: "json",
			data: {
				idpresentacion: $("#presentacion").val()
			},
			success: function (response) {
				$("#costopresentacion").empty();
				$("#costopresentacion").append("<option value=0>--- Selecciona un costo ---</option>");
				$("#costopresentacion").append(`<option value='${response[0].menudeo}'>Menudeo</option>`);
				$("#costopresentacion").append(`<option value='${response[0].mayoreo}'>Mayoreo</option>`);

			}
		});
	});

	$("#costopresentacion").on("change", function () {
		if ($(this).val() != 0) {
			let costopresentacion = parseFloat($(this).val());
			$("#subtotal").val(accounting.formatMoney(costopresentacion * $("#cantidad").val()));
		}
	});

	$("#cantidad").on("blur", function () {
		let check = $("#pespecial").prop('checked');
		if (check == true) {
			let cantidad = parseFloat($(this).val());
			$("#subtotal").val(accounting.formatMoney(cantidad * accounting.unformat($("#costopresentacionespecial").val())));
		} else {
			let cantidad = parseFloat($(this).val());
			$("#subtotal").val(accounting.formatMoney(cantidad * $("#costopresentacion").val()));
		}
		/**
		let checksi = $("#si").prop("checked");
		let checkno = $("#no").prop("checked");
			console.log(checksi);
			console.log(checkno);
		if (checksi == true && checkno == false) {
			alert("Si ");
			let cantidad = parseFloat($(this).val());
			$("#subtotal").val(accounting.formatMoney(cantidad * accounting.unformat($("#costopresentacionespecial").val())));
		} else if (checksi == false && checkno == true){
			alert("No");
			let cantidad = parseFloat($(this).val());
			$("#subtotal").val(accounting.formatMoney(cantidad * $("#costopresentacion").val()));
		}
		 **/
	});


	$("#costopresentacionespecial").on("blur", function () {
		$(this).val(accounting.formatMoney($("#costopresentacionespecial").val()));
		$("#subtotal").val(accounting.formatMoney($("#cantidad").val() * accounting.unformat($("#costopresentacionespecial").val())));

	});


	$("#agregar").on("click", function () {
		let pespecial = '';
		let producto = $("#productos").text();
		let npresentacion = $("#presentacion option:selected").text();
		let presentacion = $("#presentacion").val();
		let cunitario = accounting.formatMoney($("#costopresentacion").val());
		let cantidad = $("#cantidad").val();
		let subtotal = $("#subtotal").val();
		let subtotalg = accounting.unformat(subtotal);
		let avanza = 1;
		let datacontent = 0;
		let ncostop = $("#costopresentacion option:selected").text();
		let costop = $("#costopresentacion").val();

		let check = $("#pespecial").prop('checked');
		if (check == true) {
			//alert("Si hay precio especial");
			pespecial = 1;
			cunitario = accounting.formatMoney($("#costopresentacionespecial").val());
			ncostop = "Precio especial";
			costop = 0;
			datacontent = 1;
		} else {
			//alert("No hay precio especial");
			datacontent = 0;
			pespecial = 0;
			unitarioespecial = 0;


			if (costop == 0) {
				avanza = 0;
				$("#msj_costopresentacion").show();
				$("#msj_costopresentacion").text("Debe seleccionar un costo");
				$("#msj_costopresentacion").fadeOut(3000);
			}
		}

		if (productoglobal == '') {
			avanza = 0;
			$("#msj_producto").show();
			$("#msj_producto").text("Debe ingresar un producto");
			$("#msj_producto").fadeOut(3000);
		}
		if (presentacion == 0) {
			avanza = 0;
			$("#msj_presentacion").show();
			$("#msj_presentacion").text("Debe seleccionar una presentacion");
			$("#msj_presentacion").fadeOut(3000);
		}

		if (cantidad == 0) {
			avanza = 0;
			$("#msj_cantidad").show();
			$("#msj_cantidad").text("Ingresa cantidad");
			$("#msj_cantidad").fadeOut(3000);
		}
		if (avanza == 1) {
			let totalgeneral = accounting.unformat($("#total").text()) + accounting.unformat($("#subtotal").val());
			$("#total").text(accounting.formatMoney(totalgeneral));
			let listap =
				`<tr class="text-center" data-content="${datacontent}">
					<td id="${idproductoglobal}">${productoglobal}</td>
					<td id="${presentacion}">${npresentacion}</td>
					<td id="${ncostop}">${ncostop}</td>
					<td id="${cantidad}">${cantidad}</td>
					<td>${cunitario}</td>
					<td id="${subtotalg}">${subtotal}</td>
					<td><button class="btn btn-sm btn-danger eliminarproducto" data-content="${subtotal}"><i class="far fa-trash-alt"></i></button></td>
				</tr>`;
			$("#listap").append(listap);
			$("#subtotal").val(accounting.formatMoney(0));
			$("#cantidad").val('')
			$("#costopresentacion").empty();
			$("#costopresentacion").append("<option value=0>--- Selecciona un costo ---</option>");
			$("#presentacion").empty();
			$("#presentacion").append("<option value=0>--- Selecciona una presentacion ---</option>");
			$('#select2-productos-container').text('');

			let comision_porcentaje = $("#comision_porcentaje").val();
			let comision = accounting.unformat($("#comision").val());

			comision = parseFloat(comision) + parseFloat(parseFloat(subtotalg) * (parseFloat(comision_porcentaje) / 100));

			$("#comision").val(accounting.formatMoney(comision));
		}

	});
	$(document).on("change", "#comision_porcentaje", function () {
		let total = accounting.unformat($("#total").text());
		let comision = parseFloat(parseFloat(total) * (parseFloat($("#comision_porcentaje").val()) / 100));
		$("#comision").val(accounting.formatMoney(comision));
	});




	$(document).on("click", ".eliminarproducto", function () {
		let totalgeneral = accounting.unformat($("#total").text()) - accounting.unformat($(this).attr("data-content"));
		$("#total").text(accounting.formatMoney(totalgeneral));
		$(this).parent().parent().remove();

		let total = accounting.unformat($("#total").text());
		let comision = parseFloat(parseFloat(total) * (parseFloat($("#comision_porcentaje").val()) / 100));
		$("#comision").val(accounting.formatMoney(comision));

	});

	$("#finalizar").on("click", function () {
		let checksi = $("#si").prop("checked");
		let checkno = $("#no").prop("checked");
		let credito = 0;
		let liquidado = 0;
		let cliente = $("#cliente").text();
		let direccion = $("#direccion").val();
		let avanza = 1;
		let avanzacredito = 1;
		if (cliente == "") {
			$("#msj_cliente").show();
			$("#msj_cliente").text("Debes ingresar el nombre del cliente");
			$("#msj_cliente").fadeOut(3000);
			avanza = 0;
		}
		if (direccion == '') {
			$("#msj_direccion").show();
			$("#msj_direccion").text("Debes ingresar una direccion");
			$("#msj_direccion").fadeOut(3000);
			avanza = 0;
		}

		if (checksi == false && checkno == false) {
			$("#msj_check").show();
			$("#msj_check").text("Debes seleccionar si usara credito");
			$("#msj_check").fadeOut(3000);
			avanza = 0;
		}
		if (avanza == 1) {
			if (checksi == true && checkno == false) {
				credito = 1;
				liquidado = 0;

			} else if (checksi == false && checkno == true) {
				credito = 0;
				liquidado = 1;
			}
			let listaventas = [];
			$.each($("#listap tr"), function () {
				let especial = $(this).attr("data-content");
				let cell = 0;
				let elementoslista = {};
				if (especial == 0) {
					$.each(this.cells, function () {
						if (cell == 1) {
							elementoslista.presentacion = $(this).attr("id");
						} else if (cell == 2) {
							elementoslista.tipoventa = $(this).attr("id");
						} else if (cell == 3) {
							elementoslista.cantidad = $(this).attr("id");
						} else if (cell == 5) {
							elementoslista.subtotal = $(this).attr("id");
						}
						cell++;
					});
				} else if (especial == 1) {
					elementoslista.pespecial = 1;
					$.each(this.cells, function () {
						if (cell == 1) {
							elementoslista.presentacion = $(this).attr("id");
						} else if (cell == 2) {
							elementoslista.tipoventa = "Precio especial";
						} else if (cell == 3) {
							elementoslista.cantidad = $(this).attr("id");
						} else if (cell == 4) {
							elementoslista.unitarioespecial = accounting.unformat($(this).text());
						} else if (cell == 5) {
							elementoslista.subtotal = $(this).attr("id");
						}
						cell++;
					});
				}

				listaventas.push(elementoslista);
			});


			if (listaventas.length == 0) {
				$("#msj_alerta").show();
				$("#msj_alerta").text("Debe ingresar al menos un producto para poder finalizar la venta.");
				$("#msj_alerta").fadeOut(6000);
			} else {
				let venta = {
					cliente: idclienteglobal,
					direccion: direccion,
					credito: credito,
					liquidado: liquidado,
					total: accounting.unformat($("#total").text()),
					status: 1,
					vendedor: idvendedorglobal
				}
				let datosventa = {
					ventas: venta,
					elementoslista: listaventas
				};
				if (credito == 1) {
					datosventa.ventas.liquidado = 0;
					datosventa.ventas.creditoabonado = 0;
					$.ajax({
						url: "../Clientes/get_by_id/" + idclienteglobal,
						type: "POST",
						data: {},
						dataType: "json",
						success: (response) => {
							let creditodisponible = response.credito - response.creditousado;
							if (creditodisponible >= accounting.unformat($("#total").text())) {
								realizaventa(datosventa);
							} else {
								let rolactual = $("#sesionrol").val();
								if (rolactual == 3 || rolactual == 2) {
									let faltante = accounting.unformat($("#total").text()) - creditodisponible;
									let creditosugerido = parseFloat(response.credito) + parseFloat(faltante);
									let contenido = `
								<div class="container-fluid">
									<div class="row">
										<div class="col-md-12 text-center">
											<label style="color:red !important;">El credito del cliente no es suficiente para realizar la venta</label>
											<p>¿Deseas aumentar el credito?</p>
										</div>
									</div>
									<div class="row">
										<div class="col-6 offset-3 text-center">
											<table class="table table-bordered">
												<tbody>
												<tr>
													<td style="color: red;">${accounting.formatMoney(faltante)}<br>Faltante</td>
													<td style="color: #05CC02;">${accounting.formatMoney(creditosugerido)}<br>Credito sugerido</td>
												</tr>
												</tbody>
											</table>
										</div>
									</div>
									<div class="form-group row text-right">
										<label for="inputPassword" class="col-md-3 col-form-label">Credito: </label>
										<div class="col-md-9">
											<div class="input-group input-group-sm">
												<div class="input-group-prepend">
													<div class="input-group-text"><i class="fas fa-box"></i></div>
												</div>
												<input type="text" class="form-control form-control-sm" id="creditonuevo" value="${accounting.formatMoney(response.credito)}">
											</div>
											<div class="text-left">
												<small id="msj_creditonuevo" class="msj_presentacion"></small>
											</div>
										</div>
									</div>
								</div>`;

									let credi = $.confirm({
										title: 'Credito disponible: ' + accounting.formatMoney(creditodisponible),
										content: contenido,
										type: 'green',
										columnClass: 'col-md-6 col-md-offset-2',
										containerFluid: true,
										typeAnimated: true,
										buttons: {
											Aceptar: {
												btnClass: 'btn-success',
												action: () => {
													let cn = accounting.unformat($("#creditonuevo").val());
													if (parseFloat(cn) >= parseFloat(creditosugerido)) {
														$.ajax({
															url: "../Clientes/update/" + idclienteglobal,
															data: {
																credito: cn
															},
															type: "POST",
															success: (response) => {
																realizaventa(datosventa);
															}
														});

													} else {
														alert("El credito del cliente no es disponible");
														return 0;
													}
												}
											},
											Cancelar: {
												btnClass: 'btn-danger',
												action: () => {
													credi.close();
												}
											}
										}
									});
								} else {
									alert("El cliente no cuenta con el credito suficiente, favor de solicitar el permiso a administrador para aumentarlo");
								}

							}
						}
					});
				} else {
					datosventa.ventas.liquidado = 1;
					datosventa.ventas.creditoabonado = datosventa.ventas.total;
					realizaventa(datosventa);
				}
			}
		}
	});


	$(document).on("blur", "#creditonuevo", function () {
		$(this).val(accounting.formatMoney($("#creditonuevo").val()));
	});
	let realizaventa = datosventa => {
		let conectividad = 1;

		$.ajax({
			url: "../Ventas/insert",
			type: "POST",
			data: {
				datosventa: JSON.stringify(datosventa)
			},
			success: function (response) {
				$("#msj_hecho").show();
				$("#msj_hecho").text("Venta realizada correctamente");
				$("#msj_hecho").fadeOut(6000);
				$("#subtotal").val(accounting.formatMoney(0));
				$("#cantidad").val('')
				$("#costopresentacion").empty();
				$("#costopresentacion").append("<option value=0>--- Selecciona un costo ---</option>");
				$("#presentacion").empty();
				$("#presentacion").append("<option value=0>--- Selecciona una presentacion ---</option>");
				$('#select2-productos-container').text('');
				$("#direccion").val("");
				$("#noventa").val(response);
				$("#total").text(0);
				let total = accounting.formatMoney($("#total").text());
				$("#total").text(total);
				$("#listap").empty();
				window.open("../ventas/impresion_venta/" + $("#noventa").val(), '_blank');
				$("#noventa").val(parseInt($("#noventa").val()) + parseInt(1));
			},
			error: function (x, t, m) {
				mostrarmensajes();
				conectividad = errorcon(t, conectividad)
			},
			xhr: function () {
				mostrarmensajes();
				var xhr = $.ajaxSettings.xhr();
				xhr.upload.onprogress = function (e) {
					funcionesxhr(conectividad, "Cargando información");
				};
				xhr.onloadstart = function (e) {
					funcionesxhr(conectividad, "Iniciando proceso");
				};
				xhr.onloadend = function (e) {
					if (conectividad != 0) {
						$("#bloquear").hide();
						$("#msjsys").hide();
						$("#lblmsntop").hide();
					}

				};
				return xhr;
			}
		});
	}

});
