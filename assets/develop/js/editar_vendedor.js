import { mostrarmensajes, funcionesxhr, errorcon } from './mensajes.js';

$(document).ready(function () {



	$("#editar").on("click", function () {
		let conectividad = 1;
		let idvendedor = $(this).attr("data-vendedor");
		let nombre = $("#nombre").val();

		if (nombre == '') {
			$("#msj_nombre").html("Debe ingresar el nombre");
			$("#nombre").css({ "background": "#EFD3D2" });
			$("#msj_nombre").show();
			return 0;
		}
		$.confirm({
			title: '<i class="fas fa-robot"></i> Mensaje del sistema',
			content: '¿Estas a punto de editar la información del vendedor?',
			draggable: true,
			type: 'green',
			typeAnimated: true,

			buttons: {
				guardar: {
					text: '<i class="far fa-thumbs-up"></i> Si', // text for button
					btnClass: 'btn-success success-modal',
					action: function () {
						$.ajax({
							url: '../update/' + idvendedor,
							data: {
								nombre: nombre
							},
							type: 'POST',
							success: function (response) {
								if (response != 0) {
									alert("Vendedor editado correctamente");
								}
							},
							error: function (x, t, m) {
								mostrarmensajes();
								conectividad = errorcon(t, conectividad)
							},
							xhr: function () {
								mostrarmensajes();
								var xhr = $.ajaxSettings.xhr();
								xhr.upload.onprogress = function (e) {
									funcionesxhr(conectividad, "Cargando información");
								};
								xhr.onloadstart = function (e) {
									funcionesxhr(conectividad, "Iniciando proceso");
								};
								xhr.onloadend = function (e) {
									if (conectividad != 0) {
										$("#bloquear").hide();
										$("#msjsys").hide();
										$("#lblmsntop").hide();
									}

								};
								return xhr;
							}
						});
					}
				},
				cerrar: {
					text: '<i class="fas fa-times"></i> No', // text for button
					btnClass: 'btn-danger', // multiple classes.
				}
			}
		});
	});
});
