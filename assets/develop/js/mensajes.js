export const mostrarmensajes = () => {
    $("#bloquear").show();
    $("#msjsys").show();
    $("#lblmsntop").show();
    return;
}

export const funcionesxhr = (conectividad, mensaje) => {
    if (conectividad == 0) {
        $("#lblmsntop").text("No hay acceso a internet");
    } else {
        $("#lblmsntop").text(mensaje);
    }
    return;
}
export const errorcon = (t, conectividad = 1) => {
    $("#lblmsntop").text("Error en la peticion al servidor");
    if (t === "timeout") {
        console.log("Error en la conexion a internet");
    } else {
        conectividad = 0;
    }
    return conectividad;
}