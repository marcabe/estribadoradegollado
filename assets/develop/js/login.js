$(document).ready(function () {
	$("#usuario").click(function () {
		$(this).val('');
		$("#usuario").css({ "background": "white" });
		$("#msj_usuario").fadeOut(1500);
	});
	$("#clave").click(function () {
		$(this).val('');
		$("#clave").css({ "background": "white" });
		$("#msj_clave").fadeOut(1500);
	});


	//Funcion para que ingrese con enter
	$(document).keypress(function (e) {
		if (e.which == 13) {
			var avanza = 1;
			var usuario = $("#usuario").val();
			var clave = $("#clave").val();
			if (usuario == '') {
				$("#msj_usuario").html("Ingrese su usuario");
				$("#usuario").css({ "background": "#EFD3D2" });
				$("#msj_usuario").show();
				avanza = 0;
			}
			if (clave == '') {
				$("#msj_clave").html("Ingrese su clave");
				$("#clave").css({ "background": "#EFD3D2" });
				$("#msj_clave").show();
				avanza = 0;
			}
			if (avanza == 1) {
				$.ajax({
					url: 'valida_login',
					data: {
						usuario: usuario,
						clave: clave
					},
					type: 'POST',
					success: function (response) {
						if (response == 0) {
							/*Usuario no valido*/
							$("#usuario").css({ "background": "#EFD3D2" });
							$("#msj_correo").html("Usuario no válido");
							$("#msj_correo").show();
						} else if (response == 1) {
							/*Contraseña no valida*/
							$("#clave").css({ "background": "#EFD3D2" });
							$("#msj_clave").html("Contraseña no válida");
							$("#msj_clave").show();
						} else {
							window.location = response;
						}
					}
				});
			}
		}
	});


	$("#ingresar").on("click", function () {
		var avanza = 1;
		var usuario = $("#usuario").val();
		var clave = $("#clave").val();
		if (usuario == '') {
			$("#msj_usuario").html("Ingrese su usuario");
			$("#usuario").css({ "background": "#EFD3D2" });
			$("#msj_usuario").show();
			avanza = 0;
		}
		if (clave == '') {
			$("#msj_clave").html("Ingrese su clave");
			$("#clave").css({ "background": "#EFD3D2" });
			$("#msj_clave").show();
			avanza = 0;
		}
		if (avanza == 1) {
			$.ajax({
				url: 'valida_login',
				data: {
					usuario: usuario,
					clave: clave
				},
				type: 'POST',
				success: function (response) {
					if (response == 0) {
						/*Usuario no valido*/
						$("#usuario").css({ "background": "#EFD3D2" });
						$("#msj_correo").html("Usuario no válido");
						$("#msj_correo").show();
					} else if (response == 1) {
						/*Contraseña no valida*/
						$("#clave").css({ "background": "#EFD3D2" });
						$("#msj_clave").html("Contraseña no válida");
						$("#msj_clave").show();
					} else {
						window.location = response;
					}
				}
			});
		}
	});
});
