import { mostrarmensajes, funcionesxhr, errorcon } from './mensajes.js';

$(document).ready(function () {



	$("#nombre").on("click", function () {
		$(this).css({ "background": "white" });
		$("#msj_nombre").fadeOut(1500);
	});

	$("#usuario").on("click", function () {
		$(this).css({ "background": "white" });
		$("#msj_usuario").fadeOut(1500);
	});

	$("#roles").on("click", function () {
		$(this).css({ "background": "white" });
		$("#msj_rol").fadeOut(1500);
	});

	$("#clave").on("click", function () {
		$(this).css({ "background": "white" });
		$("#msj_clave").fadeOut(1500);
	});

	$("#clave_confirm").on("click", function () {
		$(this).css({ "background": "white" });
		$("#msj_clave_confirm").fadeOut(1500);
	});


	$("#clave").keyup(function () {
		console.log($(this).val().length);
		if ($(this).val().length < 6) {
			$("#msj_clave").html('La clave debe ser mayor a 6 caracteres');
			$("#msj_clave").show();
		} else {
			$("#msj_clave").html('');
		}
	});
	$("#clave_confirm").keyup(function () {
		if ($(this).val() != $("#clave").val()) {
			$("#msj_clave_confirm").html('Las claves no coiciden');
			$("#msj_clave_confirm").show();
		} else {
			$("#msj_clave_confirm").html('');
		}
	});



	$("#guardar").on("click", function () {
		let conectividad = 1;
		let nombre = $("#nombre").val();
		let usuario = $("#usuario").val();
		let rol = $("#roles").val();

		let clave = $("#clave").val();
		let clave_confirm = $("#clave_confirm").val();

		if (nombre == '') {
			$("#msj_nombre").html("Debe ingresar el nombre del usuario");
			$("#nombre").css({ "background": "#EFD3D2" });
			$("#msj_nombre").show();
			return 0;
		}

		if (usuario == '') {
			$("#msj_usuario").html("Debe ingresar el usuario");
			$("#usuario").css({ "background": "#EFD3D2" });
			$("#msj_usuario").show();
			return 0;
		}
		if (rol == 0) {
			$("#msj_rol").html("Debe ingresar el rol del usuario");
			$("#rol").css({ "background": "#EFD3D2" });
			$("#msj_rol").show();
			return 0;
		}

		if (clave == '') {
			$("#msj_stock").html("Debe ingresar la clave del usuario");
			$("#clave").css({ "background": "#EFD3D2" });
			$("#msj_clave").show();
			return 0;
		}

		if (clave_confirm == '') {
			$("#msj_minimo").html("Debe ingresar la confirmacion de la clave");
			$("#clave_confirm").css({ "background": "#EFD3D2" });
			$("#msj_clave_confirm").show();
			return 0;
		}

		if ($("#clave_confirm").val() != $("#clave").val()) {
			$("#msj_clave_confirm").html('Las claves no coiciden');
			$("#msj_clave_confirm").show();
			return 0;
		} else {
			$("#msj_clave_confirm").html('');
		}




		$.confirm({
			title: '<i class="fas fa-robot"></i> Mensaje del sistema',
			content: '¿Estas a punto de ingresar un usuario, deseas continuar?',
			draggable: true,
			type: 'red',
			typeAnimated: true,

			buttons: {
				guardar: {
					text: '<i class="far fa-thumbs-up"></i> Si', // text for button
					btnClass: 'btn-success success-modal',
					action: function () {
						$.ajax({
							url: 'insert',
							data: {
								nombre: nombre,
								usuario: usuario,
								clave: clave,
								rol: rol,
								status: 1
							},
							type: 'POST',
							success: function (response) {
								if (response != 0) {
									$.confirm({
										title: '<i class="fas fa-robot"></i> Mensaje del sistema',
										content: '¿Deseas guardar otro usuario?',
										draggable: true,
										buttons: {
											aceptar: {
												text: '<i class="far fa-thumbs-up"></i> Si', // text for button
												btnClass: 'btn-success success-modal',
												action: function () {
													$("#nombre").val('');
													$("#usuario").val('');
													$("#rol").val(0);
													$("#clave").val();
													$("#clave_confirm").val();
												}
											},
											cerrar: {
												text: '<i class="fas fa-times"></i> No', // text for button
												btnClass: 'btn-danger', // multiple classes.
												action: function () {
													window.location = "lista";
												}
											}
										}
									});
								} else {
									alert("El usuario ya existe en el sistema");
								}
							},
							error: function (x, t, m) {
								mostrarmensajes();
								conectividad = errorcon(t, conectividad)
							},
							xhr: function () {
								mostrarmensajes();
								var xhr = $.ajaxSettings.xhr();
								xhr.upload.onprogress = function (e) {
									funcionesxhr(conectividad, "Cargando información");
								};
								xhr.onloadstart = function (e) {
									funcionesxhr(conectividad, "Iniciando proceso");
								};
								xhr.onloadend = function (e) {
									if (conectividad != 0) {
										$("#bloquear").hide();
										$("#msjsys").hide();
										$("#lblmsntop").hide();
									}

								};
								return xhr;
							}
						});
					}
				},
				cerrar: {
					text: '<i class="fas fa-times"></i> No', // text for button
					btnClass: 'btn-danger', // multiple classes.
				}
			}
		});
	});
});
