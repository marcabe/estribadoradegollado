import { mostrarmensajes, funcionesxhr, errorcon } from './mensajes.js';


$(document).ready(function () {

	$(".imprimir").on("click", function () {
		window.open("../Entradas/impresion/" + $(this).attr("id"), '_blank');
	});

	$(".costotbl").each(function () {
		$(this).html(accounting.formatMoney($(this).html()));
	});

	$(".eliminar").on("click", function () {
		let elemento = $(this);
		let identrada = $(this).attr("id");
		let conectividad = 1;
		$.ajax({
			url: '../Entradas/eliminar/' + identrada,
			data: {
				status: 0
			},
			type: 'POST',
			success: function (response) {
				elemento.parent().parent().parent().parent().remove();
			},
			error: function (x, t, m) {
				mostrarmensajes();
				conectividad = errorcon(t, conectividad)
			},
			xhr: function () {
				mostrarmensajes();
				var xhr = $.ajaxSettings.xhr();
				xhr.upload.onprogress = function (e) {
					funcionesxhr(conectividad, "Cargando información");
				};
				xhr.onloadstart = function (e) {
					funcionesxhr(conectividad, "Iniciando proceso");
				};
				xhr.onloadend = function (e) {
					if (conectividad != 0) {
						$("#bloquear").hide();
						$("#msjsys").hide();
						$("#lblmsntop").hide();
					}

				};
				return xhr;
			}
		});
	});
	$('#tblgeneral').DataTable({
		"language": {
			"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
		}
	});

	$(".abonar").on("click", function () {
		let identrada = $(this).attr("id");
		$.ajax({
			url: "../Entradas/get_where",
			type: "POST",
			data: {
				identrada: identrada
			},
			dataType: "json",
			success: (response) => {
				console.log(response);
				let faltante = parseFloat(response.total) - parseFloat(response.creditoabonado);
				let contenido = `
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12 text-center">
								<label>¿Deseas abonar a la compra?</label>
							</div>
						</div>
						<div class="row">
							<div class="col-6 offset-3 text-center">
								<table class="table table-bordered">
									<tbody>
									<tr>
										<td style="color: red;">${accounting.formatMoney(response.total)}<br>Credito a liquidar</td>
										<td style="color: #05CC02;">${accounting.formatMoney(faltante)}<br>Faltante</td>
									</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="form-group row text-right">
							<label for="inputPassword" class="col-md-3 col-form-label">Abono: </label>
							<div class="col-md-9">
								<div class="input-group input-group-sm">
									<div class="input-group-prepend">
										<div class="input-group-text"><i class="fas fa-box"></i></div>
									</div>
									<input type="text" class="form-control form-control-sm" id="abono" value="${accounting.formatMoney(0)}">
								</div>
								<div class="text-left">
									<small id="msj_abono" class="msj"></small>
								</div>
							</div>
						</div>
					</div>`;
				let confirmabono = $.confirm({
					title: '',
					content: contenido,
					type: 'green',
					columnClass: 'col-md-6 col-md-offset-2',
					containerFluid: true,
					typeAnimated: true,
					buttons: {
						Abonar: {
							btnClass: 'btn-success',
							action: () => {
								let avanza = 1;
								let abono = accounting.unformat($("#abono").val());
								console.log(abono);
								if (abono == 0 || abono == "") {
									$("#msj_abono").show();
									$("#msj_abono").text("El abono debe ser mayor a 0");
									$("#msj_abono").fadeOut(3000);
									return 0;
								}
								if (avanza == 1) {
									let conectividad=1;
									$.ajax({
										url: "../Abonosentradas/insert",
										type: "POST",
										data: {
											identrada: identrada,
											monto: abono
										},
										success: (response) => {
											confirmabono.close();

										},
										error: function (x, t, m) {
											mostrarmensajes();
											conectividad = errorcon(t, conectividad)
										},
										xhr: function () {
											mostrarmensajes();
											var xhr = $.ajaxSettings.xhr();
											xhr.upload.onprogress = function (e) {
												funcionesxhr(conectividad, "Cargando información");
											};
											xhr.onloadstart = function (e) {
												funcionesxhr(conectividad, "Iniciando proceso");
											};
											xhr.onloadend = function (e) {
												if (conectividad != 0) {
													$("#bloquear").hide();
													$("#msjsys").hide();
													$("#lblmsntop").hide();
												}
							
											};
											return xhr;
										}
									});
								}
							}
						},
						Liquidar: {
							btnClass: 'btn-success',
							action: () => {
								let abono = accounting.unformat($("#abono").val());
								let conectividad=1;
								$.ajax({
									url: "../Abonosentradas/insert/1",
									type: "POST",
									data: {
										identrada: identrada,
										monto: faltante
									},
									success: (response) => {
										confirmabono.close();
										window.location = "lista";
									},
									error: function (x, t, m) {
										mostrarmensajes();
										conectividad = errorcon(t, conectividad)
									},
									xhr: function () {
										mostrarmensajes();
										var xhr = $.ajaxSettings.xhr();
										xhr.upload.onprogress = function (e) {
											funcionesxhr(conectividad, "Cargando información");
										};
										xhr.onloadstart = function (e) {
											funcionesxhr(conectividad, "Iniciando proceso");
										};
										xhr.onloadend = function (e) {
											if (conectividad != 0) {
												$("#bloquear").hide();
												$("#msjsys").hide();
												$("#lblmsntop").hide();
											}
						
										};
										return xhr;
									}
								});
							}
						},
						Cerrar: {
							btnClass: 'btn-danger',
							action: () => {
								confirmabono.close();
							}
						}
					}
				});
			}
		})
	});



});
