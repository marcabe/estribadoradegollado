$(document).ready(function () {

	const mostrarmensajes = () => {
		$("#bloquear").show();
		$("#msjsys").show();
		$("#lblmsntop").show();
		return;
	}

	const funcionesxhr = (conectividad, mensaje) => {
		if (conectividad == 0) {
			$("#lblmsntop").text("No hay acceso a internet");
		} else {
			$("#lblmsntop").text(mensaje);
		}
		return;
	}
	const errorcon = (t, conectividad = 1) => {
		$("#lblmsntop").text("Error en la peticion al servidor");
		if (t === "timeout") {
			console.log("Error en la conexion a internet");
		} else {
			conectividad = 0;
		}
		return conectividad;
	}


	$("#subtotal").on("blur", function () {
		$(this).val(accounting.formatMoney($("#subtotal").val()));
	});

	let total = accounting.formatMoney($("#total").text());
	$("#total").text(total);
	$('#productos').select2({
		placeholder: '--- Seleccione un producto ---',
		ajax: {
			url: '../Productos/get_like',
			dataType: 'json',
			delay: 250,
			processResults: function (response) {
				return {
					results: response
				};
			}
		}
	});

	$('#proveedor').select2({
		placeholder: '--- Seleccione un proveedor ---',
		ajax: {
			url: '../Proveedores/get_like',
			dataType: 'json',
			delay: 250,
			processResults: function (response) {
				return {
					results: response
				};
			}
		}
	});

	$('#proveedor').on('select2:select', function (e) {
		var data = e.params.data;
		console.log(data);
		idproveedorglobal = data.id;
	});

	$('#productos').on('select2:select', function (e) {
		var data = e.params.data;
		let idproducto = data.id;
		idproductoglobal = idproducto;
		productoglobal = data.text;
		$.ajax({
			url: "../Presentaciones/get_where",
			type: "POST",
			dataType: "json",
			data: {
				idproducto: idproducto
			},
			success: function (response) {
				$("#presentacion").empty();
				$("#presentacion").append("<option value=0>--- Selecciona una presentacion ---</option>");
				$.each(response, function (i, d) {
					$("#presentacion").append(`<option value="${d.idpresentacion}">${d.presentacion}</option>`);
				});
			}
		});
	});


	$("#agregar").on("click", function () {
		let producto = $("#productos").text();
		let npresentacion = $("#presentacion option:selected").text();
		let presentacion = $("#presentacion").val();
		let cantidad = $("#cantidad").val();
		let subtotal = $("#subtotal").val();
		let subtotalg = accounting.unformat(subtotal);

		let avanza = 1;
		if (producto == '') {
			avanza = 0;
			$("#msj_producto").show();
			$("#msj_producto").text("Debe ingresar un producto");
			$("#msj_producto").fadeOut(3000);
		}
		if (presentacion == 0) {
			avanza = 0;
			$("#msj_presentacion").show();
			$("#msj_presentacion").text("Debe seleccionar una presentacion");
			$("#msj_presentacion").fadeOut(3000);
		}
		if (subtotal == 0) {
			avanza = 0;
			$("#msj_subtotal").show();
			$("#msj_subtotal").text("Debe ingresar el costo del producto");
			$("#msj_subtotal").fadeOut(3000);
		}
		if (cantidad == 0) {
			avanza = 0;
			$("#msj_cantidad").show();
			$("#msj_cantidad").text("Ingresa cantidad");
			$("#msj_cantidad").fadeOut(3000);
		}
		if (avanza == 1) {
			let totalgeneral = accounting.unformat($("#total").text()) + accounting.unformat($("#subtotal").val());
			$("#total").text(accounting.formatMoney(totalgeneral));
			let listap =
				`<tr class="text-center">
					<td id="${idproductoglobal}">${productoglobal}</td>
					<td id="${presentacion}">${npresentacion}</td>
					<td id="${cantidad}">${cantidad}</td>
					<td id="${subtotalg}">${subtotal}</td>
					<td><button class="btn btn-sm btn-danger eliminarproducto" data-content="${subtotal}"><i class="far fa-trash-alt"></i></button></td>
				</tr>`;
			$("#listap").append(listap);
			$("#subtotal").val(accounting.formatMoney(0));
			$("#cantidad").val('')
			$("#presentacion").empty();
			$("#presentacion").append("<option value=0>--- Selecciona una presentacion ---</option>");
		}
	});

	$(document).on("click", ".eliminarproducto", function () {
		let totalgeneral = accounting.unformat($("#total").text()) - accounting.unformat($(this).attr("data-content"));
		$("#total").text(accounting.formatMoney(totalgeneral));
		$(this).parent().parent().remove();
	});

	$("#si").on("click", function () {
		if ($(this).prop("checked") == true) {
			$("#diascreditodiv").show();
		}
	});

	$("#no").on("click", function () {
		if ($(this).prop("checked") == true) {
			$("#diascreditodiv").hide();
		}
	});

	$("#finalizar").on("click", function () {
		let checksi = $("#si").prop("checked");
		let checkno = $("#no").prop("checked");
		let credito = 0;
		let fecha = $("#fecha").val();

		let proveedor = $("#proveedor").text();
		let diascredito = $("#diascredito").val();
		let avanza = 1;
		let avanzacredito = 1;
		let liquidado = 0;

		if (checksi == true && checkno == false) {
			credito = 1;
			liquidado = 0;

			if (diascredito == "") {
				$("#msj_diascredito").show();
				$("#msj_diascredito").text("Debes ingresar los dias de credito de la compra");
				$("#msj_diascredito").fadeOut(3000);
				avanza = 0;
			}
		} else if (checksi == false && checkno == true) {
			credito = 0;
			liquidado = 1;
		}
		if (proveedor == "") {
			$("#msj_proveedor").show();
			$("#msj_proveedor").text("Debes ingresar el nombre del proveedor");
			$("#msj_proveedor").fadeOut(3000);
			avanza = 0;
		}

		if (fecha == "") {
			$("#msj_fecha").show();
			$("#msj_fecha").text("Debes ingresar el nombre del proveedor");
			$("#msj_fecha").fadeOut(3000);
			avanza = 0;
		}

		if (checksi == false && checkno == false) {
			$("#msj_check").show();
			$("#msj_check").text("Debes seleccionar si se uso credito");
			$("#msj_check").fadeOut(3000);
			avanza = 0;
		}
		if (avanza == 1) {

			let listaentradas = [];
			$.each($("#listap tr"), function () {
				let cell = 0;
				let elementoslista = {};
				$.each(this.cells, function () {
					if (cell == 1) {
						elementoslista.presentacion = $(this).attr("id");
					} else if (cell == 2) {
						elementoslista.cantidad = $(this).attr("id");
					} else if (cell == 3) {
						elementoslista.subtotal = $(this).attr("id");
					}
					cell++;
				});
				listaentradas.push(elementoslista);
			});
			if (listaentradas.length == 0) {
				$("#msj_alerta").show();
				$("#msj_alerta").text("Debe ingresar al menos un producto para poder finalizar la entrada.");
				$("#msj_alerta").fadeOut(6000);
			} else {
				let entrada = {
					idproveedor: idproveedorglobal,
					diascredito: diascredito,
					credito: credito,
					liquidado: liquidado,
					total: accounting.unformat($("#total").text()),
					fecha: fecha,
					status: 1
				}
				let datosentrada = {
					entrada: entrada,
					elementoslista: listaentradas
				};
				if (credito == 1) {
					datosentrada.entrada.liquidado = 0;
					datosentrada.entrada.creditoabonado = 0;
					realizaventa(datosentrada);

				} else {
					datosentrada.entrada.liquidado = 1;
					datosentrada.entrada.creditoabonado = datosentrada.entrada.total;
					realizaventa(datosentrada);
				}
			}
		}
	});

	let realizaventa = datosentrada => {
		let conectividad=1;
		$.ajax({
			url: "../Entradas/insert",
			type: "POST",
			data: {
				datosentrada: JSON.stringify(datosentrada)
			},
			success: function (response) {
				$("#msj_hecho").show();
				$("#msj_hecho").text("Venta realizada correctamente");
				$("#msj_hecho").fadeOut(6000);
				$("#subtotal").val(accounting.formatMoney(0));
				$("#cantidad").val('')
				$("#costopresentacion").empty();
				$("#costopresentacion").append("<option value=0>--- Selecciona un costo ---</option>");
				$("#presentacion").empty();
				$("#presentacion").append("<option value=0>--- Selecciona una presentacion ---</option>");
				$('#select2-productos-container').text('');
				$("#direccion").val("");
				$("#noventa").val(response);
				$("#total").text(0);
				let total = accounting.formatMoney($("#total").text());
				$("#total").text(total);
				$("#listap").empty();
				window.open("../Entradas/impresion/" + $("#noventa").val(), '_blank');
				$("#noventa").val(parseInt($("#noventa").val()) + parseInt(1));
			},
			error: function (x, t, m) {
				mostrarmensajes();
				conectividad = errorcon(t, conectividad)
			},
			xhr: function () {
				mostrarmensajes();
				var xhr = $.ajaxSettings.xhr();
				xhr.upload.onprogress = function (e) {
					funcionesxhr(conectividad, "Cargando información");
				};
				xhr.onloadstart = function (e) {
					funcionesxhr(conectividad, "Iniciando proceso");
				};
				xhr.onloadend = function (e) {
					if (conectividad != 0) {
						$("#bloquear").hide();
						$("#msjsys").hide();
						$("#lblmsntop").hide();
					}

				};
				return xhr;
			}
		});
	}

});