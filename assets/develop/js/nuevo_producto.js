import { mostrarmensajes, funcionesxhr, errorcon } from './mensajes.js';

$(document).ready(function () {



	$("#producto").on("click", function () {
		$(this).val();
		$(this).css({ "background": "white" });
		$("#msj_producto").fadeOut(1500);
	});
	$("#minimo").on("click", function () {
		$(this).val();
		$(this).css({ "background": "white" });
		$("#msj_minimo").fadeOut(1500);
	});



	$("#stock").on("click", function () {
		$(this).val();
		$(this).css({ "background": "white" });
		$("#msj_stock").fadeOut(1500);
	});

	$("#medida").on("click", function () {
		$(this).val();
		$(this).css({ "background": "white" });
		$("#msj_medida").fadeOut(1500);
	});

	$("#guardar").on("click", function () {
		let conectividad = 1;
		let avanza = 1;
		let producto = $("#producto").val();
		let medida = $("#medida").val();
		let stock = $("#stock").val();
		let minimo = $("#minimo").val();

		if (producto == '') {
			$("#msj_producto").html("Debe ingresar el nombre del producto");
			$("#producto").css({ "background": "#EFD3D2" });
			$("#msj_producto").show();
			avanza = 0;
		}
		if (producto.length <= 2) {
			$("#msj_producto").html("El nombre del producto debe tener 3 caracteres o más");
			$("#producto").css({ "background": "#EFD3D2" });
			$("#msj_producto").show();
			avanza = 0;
		}



		if (medida == 0) {
			$("#msj_medida").html("Debe ingresar la medida del producto");
			$("#medida").css({ "background": "#EFD3D2" });
			$("#msj_medida").show();
			avanza = 0;
		}

		if (stock == '') {
			$("#msj_stock").html("Debe ingresar el stock del producto");
			$("#stock").css({ "background": "#EFD3D2" });
			$("#msj_stock").show();
			avanza = 0;
		}

		if (minimo == '') {
			$("#msj_minimo").html("Debe ingresar el minimo para mostrar alerta");
			$("#stock").css({ "background": "#EFD3D2" });
			$("#msj_minimo").show();
			avanza = 0;
		}





		if (avanza == 1) {
			$.confirm({
				title: '<i class="fas fa-robot"></i> Mensaje del sistema',
				content: '¿Estas a punto de ingresar un producto, deseas continuar?',
				draggable: true,
				type: 'red',
				typeAnimated: true,

				buttons: {
					guardar: {
						text: '<i class="far fa-thumbs-up"></i> Si', // text for button
						btnClass: 'btn-success success-modal',
						action: function () {
							$.ajax({
								url: 'insert',
								data: {
									producto: producto,
									stock: stock,
									medida: medida,
									minimo: minimo,
									status: 1
								},
								type: 'POST',
								success: function (response) {
									if (response != 0) {
										$.confirm({
											title: '<i class="fas fa-robot"></i> Mensaje del sistema',
											content: '¿Deseas guardar otro producto?',
											draggable: true,
											buttons: {
												aceptar: {
													text: '<i class="far fa-thumbs-up"></i> Si', // text for button
													btnClass: 'btn-success success-modal',
													action: function () {
														$("#producto").val('');
														$("#stock").val('');
														$("#medida").val(0);
													}
												},
												cerrar: {
													text: '<i class="fas fa-times"></i> No', // text for button
													btnClass: 'btn-danger', // multiple classes.
													action: function () {
														window.location = "lista";
													}
												}
											}
										});
									}
								},
								error: function (x, t, m) {
									mostrarmensajes();
									conectividad = errorcon(t, conectividad)
								},
								xhr: function () {
									mostrarmensajes();
									var xhr = $.ajaxSettings.xhr();
									xhr.upload.onprogress = function (e) {
										funcionesxhr(conectividad, "Cargando información");
									};
									xhr.onloadstart = function (e) {
										funcionesxhr(conectividad, "Iniciando proceso");
									};
									xhr.onloadend = function (e) {
										if (conectividad != 0) {
											$("#bloquear").hide();
											$("#msjsys").hide();
											$("#lblmsntop").hide();
										}

									};
									return xhr;
								}
							});
						}
					},
					cerrar: {
						text: '<i class="fas fa-times"></i> No', // text for button
						btnClass: 'btn-danger', // multiple classes.
					}
				}
			});
		}
	});
});
