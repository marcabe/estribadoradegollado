import { mostrarmensajes, funcionesxhr, errorcon } from './mensajes.js';

$(document).ready(function () {


	$("#nombre").on("click", function () {
		$(this).val();
		$(this).css({ "background": "white" });
		$("#msj_nombre").fadeOut(1500);
	});

	$("#telefono").on("click", function () {
		$(this).val();
		$(this).css({ "background": "white" });
		$("#msj_telefono").fadeOut(1500);
	});

	$("#razonsocial").on("click", function () {
		$(this).val();
		$(this).css({ "background": "white" });
		$("#msj_razonsocial").fadeOut(1500);
	});

	$("#direccion").on("click", function () {
		$(this).val();
		$(this).css({ "background": "white" });
		$("#msj_direccion").fadeOut(1500);
	});



	$("#guardar").on("click", function () {
		let conectividad =1;
		let avanza = 1;
		let nombre = $("#nombre").val();
		let telefono = $("#telefono").val();
		let razonsocial = $("#razonsocial").val();
		let rfc = $("#rfc").val();
		let correo = $("#correo").val();
		let direccion = $("#direccion").val();
		let cp = $("#cp").val();

		if (nombre == '') {
			$("#msj_nombre").html("Debe ingresar el nombre");
			$("#nombre").css({ "background": "#EFD3D2" });
			$("#msj_nombre").show();
			avanza = 0;
		}
		if (nombre.length <= 2) {
			$("#msj_nombre").html("El nombre ingresado debe tener 3 caracteres o más");
			$("#nombre").css({ "background": "#EFD3D2" });
			$("#msj_nombre").show();
			avanza = 0;
		}

		if (telefono == '') {
			$("#msj_telefono").html("Debe ingresar el telefono");
			$("#telefono").css({ "background": "#EFD3D2" });
			$("#msj_telefono").show();
			avanza = 0;
		}
		if (razonsocial == '') {
			$("#msj_razonsocial").html("Debe ingresar el correo");
			$("#razonsocial").css({ "background": "#EFD3D2" });
			$("#msj_razonsocial").show();
			avanza = 0;
		}

		if (direccion == '') {
			$("#msj_direccion").html("Debe ingresar la direccion");
			$("#direccion").css({ "background": "#EFD3D2" });
			$("#msj_direccion").show();
			avanza = 0;
		}

		if (avanza == 1) {
			$.confirm({
				title: '<i class="fas fa-robot"></i> Mensaje del sistema',
				content: '¿Estas a punto de ingresar información, deseas continuar?',
				draggable: true,
				type: 'red',
				typeAnimated: true,

				buttons: {
					guardar: {
						text: '<i class="far fa-thumbs-up"></i> Si', // text for button
						btnClass: 'btn-success success-modal',
						action: function () {
							$.ajax({
								url: 'insert',
								data: {
									nombre: nombre,
									telefono: telefono,
									razonsocial: razonsocial,
									rfc: rfc,
									correo: correo,
									direccion: direccion,
									cp: cp,
									status: 1
								},
								type: 'POST',
								success: function (response) {
									if (response != 0) {
										let confirmar = $.confirm({
											title: '<i class="fas fa-robot"></i> Mensaje del sistema',
											content: '¿Deseas guardar otro registro?',
											draggable: true,
											buttons: {
												aceptar: {
													text: '<i class="far fa-thumbs-up"></i> Si', // text for button
													btnClass: 'btn-success success-modal',
													action: function () {
														confirmar.close();
														$("#nombre").val('');
														$("#telefono").val('');
														$("#razonsocial").val('');
														$("#rfc").val('');
														$("#correo").val('');
														$("#direccion").val('');
														$("#cp").val('');
													}
												},
												cerrar: {
													text: '<i class="fas fa-times"></i> No', // text for button
													btnClass: 'btn-danger', // multiple classes.
													action: function () {
														window.location = "lista";
													}
												}
											}
										});
									}
								},
								error: function (x, t, m) {
									mostrarmensajes();
									conectividad = errorcon(t, conectividad)
								},
								xhr: function () {
									mostrarmensajes();
									var xhr = $.ajaxSettings.xhr();
									xhr.upload.onprogress = function (e) {
										funcionesxhr(conectividad, "Cargando información");
									};
									xhr.onloadstart = function (e) {
										funcionesxhr(conectividad, "Iniciando proceso");
									};
									xhr.onloadend = function (e) {
										if (conectividad != 0) {
											$("#bloquear").hide();
											$("#msjsys").hide();
											$("#lblmsntop").hide();
										}
					
									};
									return xhr;
								}
							});
						}
					},
					cerrar: {
						text: '<i class="fas fa-times"></i> No', // text for button
						btnClass: 'btn-danger', // multiple classes.
					}
				}
			});
		}
	});
});
