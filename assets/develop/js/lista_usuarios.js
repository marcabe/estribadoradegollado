import { mostrarmensajes, funcionesxhr, errorcon } from './mensajes.js';

$(document).ready(function () {



	let tbl_clientes = $('#tbl_usuarios').DataTable({
		"lengthChange": false,
		"language": {
			"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
		}
	});

	$(".stat_m").on("click", function () {
		let usuario = $(this).attr("data-content");
		let favorito = 0;
		if ($(this).hasClass("fa-user-times")) {
			$(this).removeClass("fa-user-times");
			$(this).addClass("fa-user-check");
			favorito = 1;
		} else if ($(this).hasClass("fa-user-check")) {
			$(this).removeClass("fa-user-check");
			$(this).addClass("fa-user-times");
			favorito = 0;
		}
		$.ajax({
			type: "POST",
			url: "../Usuarios/update/" + usuario,
			data: {
				status: parseInt(favorito)
			}
		});
	});



	$(".editar").on("click", function () {
		window.location = "../Usuarios/editar/" + $(this).attr("id");
	});

});
