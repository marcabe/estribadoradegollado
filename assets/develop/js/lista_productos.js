$(document).ready(function () {
	$(document).on("blur", "#menudeo", function () {
		$(this).val(accounting.formatMoney($("#menudeo").val()));
	});
	$(document).on("blur", "#mayoreo", function () {
		$(this).val(accounting.formatMoney($("#mayoreo").val()));
	});

	$(".detalle").on("click", function (){
		window.location="detalle/"+$(this).attr("id");
	});

	$(".editar").on("click", function (){
		window.location="editar/"+$(this).attr("id");
	});

	let table=$('#tblgeneral').DataTable({
		"language": {
			"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
		}
	});
	//table.page(3).draw('page');

	$(".eliminar").on("click", function (){

		let elemento = $(this);
		let idproducto = $(this).attr("id");
		$.ajax({
			url: '../Productos/update/'+idproducto,
			data: {
				status: 0
			},
			type: 'POST',
			success: function (response) {
				elemento.parent().parent().parent().parent().remove();
			}
		});
	});

	$("#tblgeneral tr").on("click",function() {
		$("#tblgeneral tr").removeClass("highlight");
		$(this).addClass("highlight");
	});
});
