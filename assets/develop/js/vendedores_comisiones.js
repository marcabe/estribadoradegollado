import { mostrarmensajes, funcionesxhr, errorcon } from './mensajes.js';

$(document).ready(function () {

	let idvendedorglobal = '';

	$('#vendedor').select2({
		placeholder: '--- Seleccione un cliente ---',
		ajax: {
			url: '../Vendedores/get_like',
			dataType: 'json',
			delay: 250,
			processResults: function (response) {
				return {
					results: response
				};
			}
		}
	});
	$('#vendedor').on('select2:select', function (e) {
		var data = e.params.data;
		idvendedorglobal = data.id;
	});


	$("#filtrar").on("click", function () {

		let fecha_inicial = $("#fecha_inicial").val();
		let fecha_final = $("#fecha_final").val();

		if (fecha_inicial == '') {
			alert("Debe ingresar una fecha inicial");
			return 0;
		}

		if (fecha_final == '') {
			alert("Debe ingresar una fecha final");
			return 0;
		}

		if (idvendedorglobal == '') {
			alert("Debe ingresar un vendedor");
			return 0;
		}

		$.ajax({
			type: "post",
			url: "filtro_comision",
			data: {
				idvendedor: idvendedorglobal,
				fecha_inicial: fecha_inicial,
				fecha_final: fecha_final
			},
			dataType: "json",
			success: function (response) {
				let respuesta = response;
				console.log(respuesta);
				$("#lista_ventas").empty();
				let cuerpo_ventas = '';
				$.each(respuesta, function (i, venta) {
					let filas = '';
					$.each(venta.lista_p, function (j, lista) {
						let cantidad = accounting.formatMoney(lista.cantidad);
						let subtotal = accounting.formatMoney(lista.subtotal);
						filas += `
							<tr>
								<td>${lista.presentacion.productos.producto}</td>
								<td>${lista.presentacion.presentacion}</td>
								<td>${lista.tipoventa}</td>
								<td>${cantidad}</td>
								<td>${subtotal}</td>
							</tr>`;
					});
					let total_venta_c = accounting.formatMoney(venta.total_venta_c);
					cuerpo_ventas += `
						<div class="col-md-12">
							<div class="accordion" id="accordionExample">
								<div class="card">
		
									<div class="card-header">
										<div class="row"
											style="background:#B4E391;>
											<div class="col-md-6">
												<h5 class="mb-0">
													<button class="btn btn-link" type="button"
														data-toggle="collapse"
														data-target="#ventas_${venta.idventa}"
														aria-expanded="true" aria-controls="collapseOne">
														${venta.idventa}
														(${venta.fecha})
														<label>Total: ${total_venta_c}</label>
													</button>
												</h5>
											</div>
										</div>
									</div>
									<div id="ventas_${venta.idventa}"
										class="collapse show" aria-labelledby="headingOne"
										data-parent="#accordionExample">
										<div class="card-body">
											<table class="table table-striped table-bordered" >
												<thead>
													<tr>
														<td>PRODUCTO</td>
														<td>PRESENTACION</td>
														<td>TIPO DE VENTA</td>
														<td>CANTIDAD</td>
														<td>SUB TOTAL</td>
													</tr>
												</thead>
												<tbody id="body_ventas_${venta.idventa}">
												${filas}
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>`;

				});
				$("#lista_ventas").append(cuerpo_ventas);
			}
		});

	});

	/*
	$(".editar").on("click", function () {
		window.location = "../Usuarios/editar/" + $(this).attr("id");
	});
	*/

});
