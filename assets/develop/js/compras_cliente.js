import { mostrarmensajes, funcionesxhr, errorcon } from './mensajes.js';


$(".abonar").on("click", function () {
	let idventa = $(this).attr("data-venta");
	$.ajax({
		url: "../../Ventas/get_where",
		type: "POST",
		data: {
			idventa: idventa
		},
		dataType: "json",
		success: (response) => {
			console.log(response);
			let faltante = parseFloat(response.total) - parseFloat(response.creditoabonado);
			let contenido = `
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12 text-center">
							<label>¿Deseas abonar a la compra?</label>
						</div>
					</div>
					<div class="row">
						<div class="col-6 offset-3 text-center">
							<table class="table table-bordered">
								<tbody>
								<tr>
									<td style="color: red;">${accounting.formatMoney(response.total)}<br>Credito a liquidar</td>
									<td style="color: #05CC02;">${accounting.formatMoney(faltante)}<br>Faltante</td>
								</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="form-group row text-right">
						<label for="inputPassword" class="col-md-3 col-form-label">Abono: </label>
						<div class="col-md-9">
							<div class="input-group input-group-sm">
								<div class="input-group-prepend">
									<div class="input-group-text"><i class="fas fa-box"></i></div>
								</div>
								<input type="text" class="form-control form-control-sm" id="abono" value="${accounting.formatMoney(0)}">
							</div>
							<div class="text-left">
								<small id="msj_abono" class="msj"></small>
							</div>
						</div>
					</div>
					<div class="form-group row text-right">
							<label for="inputPassword" class="col-md-3 col-form-label">Forma de pago: </label>
							<div class="col-md-9">
								<div class="input-group input-group-sm">
									<div class="input-group-prepend">
										<div class="input-group-text"><i class="fas fa-box"></i></div>
									</div>
									<select class="form-control form-control-sm" id="forma_pago">
										<option value=0>Seleccione forma de pago</option>
										<option value="e">Efectivo</option>
										<option value="t">Transferencia</option>
									</select>
								</div>
								<div class="text-left">
									<small id="msj_forma" class="msj"></small>
								</div>
							</div>
						</div>
				</div>`;
			let confirmabono = $.confirm({
				title: '',
				content: contenido,
				type: 'green',
				columnClass: 'col-md-6 col-md-offset-2',
				containerFluid: true,
				typeAnimated: true,
				buttons: {
					Abonar: {
						btnClass: 'btn-success',
						action: () => {
							let avanza = 1;
							let abono = accounting.unformat($("#abono").val());
							let forma_pago = $("#forma_pago").val();

							if (abono == 0 || abono == "") {
								$("#msj_abono").show();
								$("#msj_abono").text("El abono debe ser mayor a 0");
								$("#msj_abono").fadeOut(3000);
								return 0;
							}
							if (forma_pago == 0) {
								$("#msj_forma").show();
								$("#msj_forma").text("Debe ingresar la forma de pago");
								$("#msj_forma").fadeOut(3000);
								return 0;
							}
							if (avanza == 1) {
								let conectividad = 1;
								$.ajax({
									url: "../../Abonos/insert",
									type: "POST",
									data: {
										idventa: idventa,
										monto: abono,
										forma_pago: forma_pago
									},
									success: (response) => {
										confirmabono.close();
										location.reload();
									},
									error: function (x, t, m) {
										mostrarmensajes();
										conectividad = errorcon(t, conectividad)
									},
									xhr: function () {
										mostrarmensajes();
										var xhr = $.ajaxSettings.xhr();
										xhr.upload.onprogress = function (e) {
											funcionesxhr(conectividad, "Cargando información");
										};
										xhr.onloadstart = function (e) {
											funcionesxhr(conectividad, "Iniciando proceso");
										};
										xhr.onloadend = function (e) {
											if (conectividad != 0) {
												$("#bloquear").hide();
												$("#msjsys").hide();
												$("#lblmsntop").hide();
											}

										};
										return xhr;
									}
								});
							}
						}
					},
					Liquidar: {
						btnClass: 'btn-success',
						action: () => {
							let abono = accounting.unformat($("#abono").val());
							let conectividad = 1;
							$.ajax({
								url: "../../Abonos/insert/1",
								type: "POST",
								data: {
									idventa: idventa,
									monto: faltante
								},
								success: (response) => {
									confirmabono.close();
									location.reload();
								},
								error: function (x, t, m) {
									mostrarmensajes();
									conectividad = errorcon(t, conectividad)
								},
								xhr: function () {
									mostrarmensajes();
									var xhr = $.ajaxSettings.xhr();
									xhr.upload.onprogress = function (e) {
										funcionesxhr(conectividad, "Cargando información");
									};
									xhr.onloadstart = function (e) {
										funcionesxhr(conectividad, "Iniciando proceso");
									};
									xhr.onloadend = function (e) {
										if (conectividad != 0) {
											$("#bloquear").hide();
											$("#msjsys").hide();
											$("#lblmsntop").hide();
										}

									};
									return xhr;
								}
							});
						}
					},
					Cerrar: {
						btnClass: 'btn-danger',
						action: () => {
							confirmabono.close();
						}
					}
				}
			});
		}
	})
});