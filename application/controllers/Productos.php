<?php
defined('BASEPATH') or exit('No direct script access allowed');

date_default_timezone_set("America/Mexico_City");
require('PDFProductos.php');
require('PDFProductosVxMes.php');


class Productos extends CI_Controller
{
	public function __construct()
	{

		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->Model('ProductosModel');
		$this->load->Model('MedidasModel');
		$this->load->Model('PresentacionesModel');


		$this->hoy = date("Y-m-d");
		$this->finicio = date("Y") . "-" . date("m") . "-" . "01";
		$this->ffinal = date("Y") . "-" . date("m") . "-" . date("t", strtotime("01-" . date("m") . "-" . date("Y")));

	}

	public function index()
	{
		if(is_null($this->session->userdata("edegollado"))){
			redirect(base_url());
		}else {
			$dataProductos = $this->ProductosModel->get_where(array("status"=>1));
			foreach ($dataProductos as $p) {
				$dataMedida = $this->MedidasModel->get_by_id($p->medida);
				$p->medidas = $dataMedida[0];
			}
			$data = array("productos" => $dataProductos);
			$this->load->view("lista_productos", $data);
		}
	}
	public function insert(){
		echo $this->ProductosModel->insert($this->input->post());
	}

	public function detalle($idProducto){
		if(is_null($this->session->userdata("edegollado"))){
			redirect(base_url());
		}else{
			$dataProductos = $this->ProductosModel->get_by_id($idProducto);
			foreach ($dataProductos as $p){
				$dataMedida = $this->MedidasModel->get_by_id($p->medida);
				$p->medida = $dataMedida[0];
			}
			$dataPresentacion = $this->PresentacionesModel->get_where(array("idproducto"=>$idProducto, "status"=>1));
			$data= array(
				"producto"=>$dataProductos,
				"presentaciones"=>$dataPresentacion
			);
			$this->load->view("detalle_producto",$data);
		}
	}
	public function nuevo(){
		if(is_null($this->session->userdata("edegollado"))){
			redirect(base_url());
		}else{
			$dataMedidas = $this->MedidasModel->get();
			$data = array(
				"medidas"=>$dataMedidas
			);
			$this->load->view("nuevo_producto", $data);
		}
	}

	public function editar($idproducto){
		if(is_null($this->session->userdata("edegollado"))){
			redirect(base_url());
		}else{
			$dataProducto = $this->ProductosModel->get_by_id($idproducto);
			$dataMedidas = $this->MedidasModel->get();

			$data = array(
				"producto"=>$dataProducto[0],
				"medidas"=>$dataMedidas);
			$this->load->view("editar_producto", $data);
		}
	}

	public function update($idproducto){
		$dataUpdate = $this->input->post();
		$this->ProductosModel->update($idproducto, $dataUpdate);
		echo 1;
	}


	public function get_like(){
		$json = [];
		$data = $this->input->get("q");
		$dataProductos = $this->ProductosModel->get_like($data);
		$json = $dataProductos;
		echo json_encode($json);
	}

	public function reporte(){
		$header = array('PRODUCTO', 'STOCK', 'MEDIDA');
		$dataClientes = $this->ProductosModel->select_get_where("producto, stock, medida", array("status"=>1));
		$pdf = new PDFProductos($header);


		$pdf->SetFont('Arial', '', 14);
		$pdf->AddPage();
		$pdf->BasicTable($dataClientes);

		$pdf->Output('paginaEnBlanco.pdf', 'I');
	}

	public function ventas_productos_mes($mes, $anio){
		$header = array('PRODUCTO', 'PRESENTACION', 'CANTIDAD', 'SUBTOTAL');
		$dataPdf = array();

		$dataProductos = $this->ProductosModel->ventas_productos_mes(array("ventas.status"=>1, "MONTH(fecha)" => $mes, "YEAR(fecha)" => $anio));
		foreach($dataProductos as $p){
			$subtotal =number_format($p->subtotal, 2, '.', ',');
			$dataPdf[] = array(
				0 => $p->producto,
				1 => $p->presentacion,
				2 => $p->canti,
				3 => "$ " . $subtotal,
			);
		}
		$pdf = new PDFProductosVxMes($header, $mes, $anio);
		$pdf->SetFont('Arial', '', 14);
		$pdf->AddPage();
		$pdf->BasicTable($dataPdf);

		$pdf->Output('paginaEnBlanco.pdf', 'I');
	}

}
