<?php

require('fpdf/fpdf.php');

class PDFVenta extends FPDF
{
    public function __construct($cliente, $direccion, $nota, $fecha, $credito, $vendedor, $total, $abonado, $liquidado)
    {
        parent::__construct();
        $this->cliente = $cliente;
        $this->direccion = $direccion;
        $this->nota = $nota;
        $this->fecha = $fecha;
        $this->credito = $credito;
        $this->vendedor = $vendedor;
        $this->total = $total;
        $this->abonado = $abonado;
        $this->liquidado = $liquidado;
    }

    public function Header()
    {
        $this->Rect(10, 25, 190, 240);
        $this->Image('assets/develop/images/logo.jpg', 50, 10, -300);
        $this->Ln();
        $this->Image('assets/develop/images/marcaagua.jpg', 45, 60, 125, 150);
        $this->SetXY(15, 40);
        $this->SetFont('Arial', 'B', 12);

        $this->Cell(140, 7, "FECHA:  " . $this->fecha, 0, 0, "L");
        $this->Cell(40, 7, "NOTA DE VENTA", 0, 0, "C");
        $this->Ln();
        $this->SetX(155);
        $this->Cell(40, 7, $this->nota, 0, 0, "C");
        $this->SetX(15);
        $this->Cell(140, 7, "CLIENTE:  " . $this->cliente, 0, 0, "L");
        $this->Ln();
        $this->SetX(15);
        $this->Cell(140, 7, "DIRECCION:  " . $this->direccion, 0, 0, "L");
        $this->Ln();
        $this->SetX(15);
        if ($this->credito == "SI" && $this->liquidado == 0) {
            $this->Cell(75, 7, "ABONADO:  $" . $this->abonado, 0, 0, "L");
        } else {
            $this->Cell(75, 7, "LIQUIDADO", 0, 0, "L");
        }
        $this->SetX(120);
        $this->Cell(75, 7, "CAJERO:  " . $this->vendedor, 0, 0, "R");
    }

    public function Footer()
    {
        $this->SetY(-50);
        $this->SetFont('Arial', 'B', 22);
        $this->Cell(190, 10, "TOTAL:  $" . $this->total, 0, 0, "R");


        $this->SetY(-34);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(0, 9, utf8_decode('KM 4.5 CARRETERA LA PIEDAD GUADALAJARA   (348) 1214996     AJLPGORA@HOTMAIL.COM'), 0, 0, 'C');
        $this->Cell(-15, 10, utf8_decode('Página ') . $this->PageNo(), 0, 0, 'C');
    }

    // Tabla simple
    public function BasicTable($header, $data, $header_a, $abonos)
    {
        // Cabecera
        $this->SetXY(12, 80);

        $this->SetFont('Arial', 'B', 9);

        foreach ($header as $col) {
            $this->Cell(38, 5, $col, 0, 0, "C");
        }

        // Datos
        $y = 85;
        foreach ($data as $row) {
            $this->SetXY(10, $y);
            foreach ($row as $col) {
                $this->Cell(38, 5, $col, 0, 0, "C");
            }
            $y = $y + 3;
        }


        if ($header_a != 0) {
            $y = $y + 15;
            $this->SetXY(10, $y);
            $this->Cell(190, 7, "ABONOS REALIZADOS", 0, 0, "C");

            $y = $y + 10;
            $this->SetXY(45, $y);


            foreach ($header_a as $c) {
                $this->Cell(40, 5, $c, 0, 0, "C");
            }
        }
        $y = $y + 10;
        $this->SetXY(45, $y);
        //var_dump($abonos);
        if ($abonos != 0) {
            foreach ($abonos as $row) {
                $this->SetXY(45, $y);
                foreach ($row as $col) {
                    $this->Cell(40, 5, $col, 0, 0, "C");
                }
                $y = $y + 4;
            }
        }
    }
}
