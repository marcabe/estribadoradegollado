<?php
defined('BASEPATH') or exit('No direct script access allowed');

date_default_timezone_set("America/Mexico_City");
require('PDFProveedores.php');


class Proveedores extends CI_Controller
{
	public function __construct()
	{

		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->Model('ProveedoresModel');
		$this->hoy = date("Y-m-d");
		$this->finicio = date("Y") . "-" . date("m") . "-" . "01";
		$this->ffinal = date("Y") . "-" . date("m") . "-" . date("t", strtotime("01-" . date("m") . "-" . date("Y")));

	}

	public function index()
	{
		if (is_null($this->session->userdata("edegollado"))) {
			redirect(base_url());
		} else {
			$dataProveedores = $this->ProveedoresModel->get_where(array("status"=>1));
			$data = array("proveedores" => $dataProveedores);
			$this->load->view("lista_proveedores", $data);
		}
	}

	public function nuevo(){
		if (is_null($this->session->userdata("edegollado"))) {
			redirect(base_url());
		} else {
			$this->load->view("nuevo_proveedor");
		}
	}

	public function insert(){
		if (is_null($this->session->userdata("edegollado"))) {
			redirect(base_url());
		} else {
			$dataInsert = $this->input->post();
			$dataInsert['create'] = $this->hoy;
			echo $this->ProveedoresModel->insert($dataInsert);
		}
	}

	public function update($idproveedor)
	{
		if (is_null($this->session->userdata("edegollado"))) {
			redirect(base_url());
		} else {
			$this->ProveedoresModel->update($idproveedor, $this->input->post());
			echo 1;
		}
	}

	public function editar($idproveedor){
		if (is_null($this->session->userdata("edegollado"))) {
			redirect(base_url());
		} else {
			$dataProveedor = $this->ProveedoresModel->get_by_id($idproveedor);
			$data = array(
				"proveedor"=>$dataProveedor[0]
			);
			$this->load->view("editar_proveedor", $data);
		}
	}

	public function get_like()
	{
		if (is_null($this->session->userdata("edegollado"))) {
			redirect(base_url());
		} else {
			$json = [];
			$data = $this->input->get("q");
			$dataProveedor = $this->ProveedoresModel->get_like($data);
			$json = $dataProveedor;
			echo json_encode($json);
		}
	}
	public function reporte(){
		$header = array('NOMBRE', 'TELEFONO', 'RAZON SOCIAL', 'RFC');
		$dataClientes = $this->ProveedoresModel->select_get_where("nombre, telefono, razonsocial, rfc", array("status"=>1));
		$pdf = new PDFProveedores($header);


		$pdf->SetFont('Arial', '', 14);
		$pdf->AddPage();
		$pdf->BasicTable($dataClientes);

		$pdf->Output('paginaEnBlanco.pdf', 'I');
	}

}
