<?php
defined('BASEPATH') or exit('No direct script access allowed');

date_default_timezone_set("America/Mexico_City");


class Ingresos extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->Model('IngresosModel');
    }



    public function index()
    {
        $dataIngresos = $this->IngresosModel->get();
        $data = array(
            "ingresos" => $dataIngresos
        );
        $this->load->view("lista_ingresos", $data);
    }

    public function insert()
    {
        $insert = $this->input->post();
        $this->IngresosModel->insert($insert);
        $dataIngresos = $this->IngresosModel->get();

        $dataResponse = array();

        foreach ($dataIngresos as $i) {
            $gasto = array(
                "no_ingreso" => $i->idIngreso,
                "motivo" => $i->motivo,
                "monto" => $i->ingreso,
                "fecha" => $i->fecha
            );
            $dataResponse[] = $gasto;
        }

        echo json_encode($dataResponse);
    }
}
