<?php


class PDFClientesNotas extends FPDF
{
	public function __construct($header, $fecha, $titulo, $x_f, $cliente)
	{

		parent::__construct();
		$this->header = $header;
		$this->fecha = $fecha;
		$this->titulo = $titulo;
		$this->x_f = $x_f;
		$this->cliente = $cliente;
	}

	function Header()
	{
		$this->Rect(10, 25, 190, 260);

		$this->Image('assets/develop/images/logo.jpg', 50, 10, -300);
		$this->Ln();
		$this->Image('assets/develop/images/marcaagua.jpg', 45, 60, 125, 150);
		$this->SetXY(12, 45);
		$this->SetFont('Arial', 'B', 9);


		$this->Cell(90, 5, "REPORTE DE: " . $this->cliente, 0, 0, "L");
		$this->Ln();
		$this->Ln();
	}

	function Footer()
	{
		$this->SetY(-10);
		$this->SetFont('Arial', 'B', 8);
		$this->Cell(0, 9, utf8_decode('KM 4.5 CARRETERA LA PIEDAD GUADALAJARA   (348) 1214996     AJLPGORA@HOTMAIL.COM'), 0, 0, 'C');
		$this->Cell(-15, 10, utf8_decode('Página ') . $this->PageNo(), 0, 0, 'C');
	}

	// Tabla simple
	function BasicTable($ventas)
	{

		#ABONOS


		$this->Ln();
		$this->SetFont('Arial', 'B', 15);
		$this->Cell(190, 5, $this->titulo, 0, 0, "C");
		$this->Ln();
		$this->Ln();
		$this->SetFont('Arial', 'B', 9);
		$this->SetX($this->x_f);

		foreach ($this->header as $col) {
			$this->Cell(35, 5, $col, 1, 0, "C");
		}
		$this->Ln();
		$totalab = 0;
		foreach ($ventas as $row) {
			$i = 0;
			$this->SetX($this->x_f);

			foreach ($row as $col) {
				if ($i == 0) {
					$this->Cell(35, 5, $col, 1, 0, "C");
				} elseif ($i == 2) {
					$this->Cell(35, 5, "$" . number_format($col), 1, 0, "C");
					$totalab += $col;
				} else {
					$this->Cell(35, 5, $col, 1, 0, "L");
				}
				$i++;
			}
			$this->Ln();
		}


		$this->Ln();
	}
}
