<?php
defined('BASEPATH') or exit('No direct script access allowed');

date_default_timezone_set("America/Mexico_City");
require('fpdf/fpdf.php');


class Vendedores extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->Model('VendedoresModel');
        $this->load->Model('VentasModel');
        $this->load->Model('ListaventasModel');
        $this->load->Model('ProductosModel');
        $this->load->Model('PresentacionesModel');
    }


    public function index()
    {
        $dataVendedores = $this->VendedoresModel->get();
        foreach ($dataVendedores as $v) {
            $v->btn = '
                <div class="dropdown">
                    <button
                        class="btn btn-success btn-sm dropdown-toggle configprospectos"
                        data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        <i class="fas fa-cogs"></i> Config
                    </button>
                    <div class="dropdown-menu listaconfig"
                        aria-labelledby="dropdownMenuButton">
                        <span class="dropdown-item editar"
                            data-vendedor="' . $v->idvendedor . '">
                            <i class="fas fa-eye"></i> Editar
                        </span>
                        <span class="dropdown-item imprimir_co"
                            data-vendedor="' . $v->idvendedor . '">
                            <i class="fas fa-eye"></i> Imprimir Comisiones
                        </span>
                    </div>
                </div>';
        }
        $data = array(
            "vendedores" => $dataVendedores
        );
        $this->load->view("lista_vendedores", $data);
    }

    public function nuevo()
    {
        $this->load->view("nuevo_vendedor");
    }

    public function editar($vendedor)
    {
        $dataVendedor = $this->VendedoresModel->get_by_id($vendedor);
        $data = array(
            "vendedor" => $dataVendedor[0]
        );
        $this->load->view("editar_vendedor", $data);
    }



    public function vendedores_comisiones()
    {
        $dataVendedores = $this->VendedoresModel->get();

        $data = array(
            "vendedores" => $dataVendedores
        );

        $this->load->view("vendedores_comisiones", $data);
    }

    public function insert($final = 0)
    {
        $dataInsert = $this->input->post();
        $this->VendedoresModel->insert($dataInsert);
    }

    public function update($vendedor)
    {
        $dataUpdate = $this->input->post();
        $this->VendedoresModel->update($vendedor, $dataUpdate);
        echo 1;
    }

    public function filtro_comision()
    {
        $dataFiltro = $this->input->post();
        $vendedor = $dataFiltro['idvendedor'];
        $fecha_inicial = $dataFiltro['fecha_inicial'] . " 00:00:00";
        $fecha_final = $dataFiltro['fecha_final'] . " 23:59:99";

        $dataVentas = $this->VentasModel->get_where(array("vendedor" => $vendedor, "fecha>=" => $fecha_inicial, "fecha<=" => $fecha_final));
        foreach ($dataVentas as $v) {
            $lista_ventas = $this->ListaventasModel->get_where(array("idventa" => $v->idventa));
            $lista_p = array();
            $suma_ventas_t = 0;
            foreach ($lista_ventas as $lv) {
                $dataPresentacion = $this->PresentacionesModel->get_where(array("idpresentacion" => $lv->presentacion));
                foreach ($dataPresentacion as $pre) {
                    $dataProducto = $this->ProductosModel->get_where(array("idproducto" => $pre->idproducto));
                    $pre->productos = $dataProducto[0];
                }
                $lv->presentacion = $dataPresentacion[0];
                $lista_p[] = $lv;
                $suma_ventas_t += $lv->subtotal;
            }
            $v->lista_p = $lista_p;
            $v->total_venta_c = $suma_ventas_t;
        }
        echo json_encode($dataVentas);
    }


    public function comisiones($vendedor)
    {
        $dataVentas = $this->VentasModel->get_where(array("vendedor" => $vendedor));
        $t_comision = 0;
        foreach ($dataVentas as $a) {
            $t_comision += $a->comision;
        }
        $data = array(
            "ventas" => $dataVentas,
            "vendedor" => $vendedor,
            "total" => $t_comision,
            "anio" => date("Y"),
            "mes" => date("m")
        );

        $this->load->view("comisiones_vendedores", $data);
    }

    public function filtro_ajax($anio, $mes, $vendedor)
    {
        $dataResult = array();
        $dataVentas = $this->VentasModel->get_where(array("vendedor" => $vendedor, "MONTH(date(fecha))" => $mes, "YEAR(date(fecha))" => $anio));
        foreach ($dataVentas as $v) {
            $data = array(
                "idventa" => $v->idventa,
                "fecha" => $v->fecha,
                "porcentaje" => "%" . $v->comision_porcentaje,
                "comision" => $v->comision
            );
            $dataResult[] = $data;
        }
        echo json_encode($dataResult);
    }

    public function get_like()
    {
        $json = [];
        $data = $this->input->get("q");
        $dataClientes = $this->VendedoresModel->get_like($data);
        $json = $dataClientes;
        echo json_encode($json);
    }

    public function impresion_comisiones($vendedor)
    {
        $dataVentas = $this->VentasModel->get_where(array("vendedor" => $vendedor));


        $pdf = new FPDF();
        $pdf->AddPage();
        $pdf->Rect(10, 25, 190, 260);
        $pdf->Image('assets/develop/images/logo.jpg', 50, 10, -300);
        $pdf->Ln();
        $pdf->Image('assets/develop/images/marcaagua.jpg', 45, 60, 125, 150);
        $pdf->SetXY(10, 45);
        $pdf->SetFont('Arial', 'B', 12);

        $pdf->Cell(190, 10, 'HISTORIAL DE COMISIONES', 0, 0, "C");
        $pdf->Ln();
        $pdf->Ln();
        $pdf->SetFont('Arial', 'B', 10);

        $pdf->SetX(65);
        $pdf->Cell(20, 6, 'ID VENTA', 1, 0, "C");
        $pdf->Cell(30, 6, 'FECHA', 1, 0, "C");
        $pdf->Cell(30, 6, 'MONTO', 1, 0, "C");
        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Ln();
        foreach ($dataVentas as $v) {
            $pdf->SetX(65);
            $pdf->Cell(20, 6, $v->idventa, 1, 0, "C");
            $pdf->Cell(30, 6, $v->fecha, 1, 0, "C");
            $pdf->Cell(30, 6, "$" . number_format($v->total), 1, 0, "C");
            $pdf->Ln();
        }
        $pdf->Output();
    }
}
