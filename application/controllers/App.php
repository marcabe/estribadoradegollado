<?php
defined('BASEPATH') or exit('No direct script access allowed');

date_default_timezone_set("America/Mexico_City");

require('fpdf/fpdf.php');



class App extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('session');

        $this->hoy = date("Y-m-d");
        $this->finicio = date("Y") . "-" . date("m") . "-" . "01";
        $this->ffinal = date("Y") . "-" . date("m") . "-" . date("t", strtotime("01-" . date("m") . "-" . date("Y")));
    }

    public function calculadora_efectivo()
    {
        $this->load->view("calculadora_efectivo");
    }

    public function impresion_calculadora()
    {

        $datos = $this->input->post();

        $t = 0;

        $pdf = new FPDF();
        $pdf->AddPage();
        $pdf->Rect(10, 25, 190, 260);
        $pdf->Image('assets/develop/images/logo.jpg', 50, 10, -300);
        $pdf->Ln();
        $pdf->Image('assets/develop/images/marcaagua.jpg', 45, 60, 125, 150);
        $pdf->SetXY(10, 45);
        $pdf->SetFont('Arial', 'B', 12);

        $pdf->Cell(190, 10, 'TOTAL GENERAL: ' . $datos['t_gral'], 1, 0, "C");
        $pdf->Ln();
        $pdf->Ln();
        $pdf->SetFont('Arial', 'B', 10);

        $pdf->Cell(95, 5, 'Lista de Billetes', 1, 0, "C");
        $pdf->Cell(95, 5, 'Lista de Monedas', 1, 0, "C");

        $pdf->SetX(10);
        $pdf->SetY(70);

        $yb = 70;
        $ym = 70;
        $pdf->Cell(30, 5, '$ 1,000 x', 1, 0, "C");
        $pdf->Cell(30, 5, $datos['b_1000'], 1, 0, "C");
        $pdf->Cell(35, 5, $datos['t_1000'], 1, 0, "C");
        $ym = $ym + 5;
        $pdf->SetX(10);
        $pdf->SetY($ym);
        $pdf->Cell(30, 5, '$ 500 x', 1, 0, "C");
        $pdf->Cell(30, 5, $datos['b_500'], 1, 0, "C");
        $pdf->Cell(35, 5, $datos['t_500'], 1, 0, "C");

        $ym = $ym + 5;
        $pdf->SetX(10);
        $pdf->SetY($ym);
        $pdf->Cell(30, 5, '$ 200 x', 1, 0, "C");
        $pdf->Cell(30, 5, $datos['b_200'], 1, 0, "C");
        $pdf->Cell(35, 5, $datos['t_200'], 1, 0, "C");
        $ym = $ym + 5;
        $pdf->SetX(10);
        $pdf->SetY($ym);
        $pdf->Cell(30, 5, '$ 100 x', 1, 0, "C");
        $pdf->Cell(30, 5, $datos['b_100'], 1, 0, "C");
        $pdf->Cell(35, 5, $datos['t_100'], 1, 0, "C");
        $ym = $ym + 5;
        $pdf->SetX(10);
        $pdf->SetY($ym);
        $pdf->Cell(30, 5, '$ 50 x', 1, 0, "C");
        $pdf->Cell(30, 5, $datos['b_50'], 1, 0, "C");
        $pdf->Cell(35, 5, $datos['t_50'], 1, 0, "C");
        $ym = $ym + 5;
        $pdf->SetX(10);
        $pdf->SetY($ym);
        $pdf->Cell(30, 5, '$ 20 x', 1, 0, "C");
        $pdf->Cell(30, 5, $datos['b_20'], 1, 0, "C");
        $pdf->Cell(35, 5, $datos['t_20'], 1, 0, "C");
        $ym = $ym + 5;
        $pdf->SetX(10);
        $pdf->SetY($ym);
        $pdf->Cell(60, 5, 'TOTAL: ', 1, 0, "R");
        $pdf->Cell(35, 5, $datos['tb_gral'], 1, 0, "C");


        $pdf->SetY($yb);
        $pdf->SetX(105);
        $pdf->Cell(30, 5, '$ 10 x', 1, 0, "C");
        $pdf->Cell(30, 5, $datos['m_10'], 1, 0, "C");
        $pdf->Cell(35, 5, $datos['tm_10'], 1, 0, "C");
        $yb = $yb + 5;
        $pdf->SetY($yb);
        $pdf->SetX(105);
        $pdf->Cell(30, 5, '$ 5 x', 1, 0, "C");
        $pdf->Cell(30, 5, $datos['m_5'], 1, 0, "C");
        $pdf->Cell(35, 5, $datos['tm_5'], 1, 0, "C");
        $yb = $yb + 5;
        $pdf->SetY($yb);
        $pdf->SetX(105);
        $pdf->Cell(30, 5, '$ 2 x', 1, 0, "C");
        $pdf->Cell(30, 5, $datos['m_2'], 1, 0, "C");
        $pdf->Cell(35, 5, $datos['tm_2'], 1, 0, "C");
        $yb = $yb + 5;
        $pdf->SetY($yb);
        $pdf->SetX(105);
        $pdf->Cell(30, 5, '$ 1 x', 1, 0, "C");
        $pdf->Cell(30, 5, $datos['m_1'], 1, 0, "C");
        $pdf->Cell(35, 5, $datos['tm_1'], 1, 0, "C");
        $yb = $yb + 5;
        $pdf->SetY($yb);
        $pdf->SetX(105);
        $pdf->Cell(30, 5, '$ .50 x', 1, 0, "C");
        $pdf->Cell(30, 5, $datos['m_050'], 1, 0, "C");
        $pdf->Cell(35, 5, $datos['tm_050'], 1, 0, "C");
        $yb = $yb + 5;
        $pdf->SetY($yb);
        $pdf->SetX(105);
        $pdf->Cell(60, 5, 'TOTAL: ', 1, 0, "R");
        $pdf->Cell(35, 5, $datos['tm_gral'], 1, 0, "C");




        $pdf->Output();
    }
}
