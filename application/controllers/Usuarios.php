<?php
defined('BASEPATH') or exit('No direct script access allowed');

date_default_timezone_set("America/Mexico_City");


class Usuarios extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->Model('UsuariosModel');
        $this->load->Model('VentasModel');
        $this->load->Model('EntradasModel');
        $this->load->Model('ProductosModel');
        $this->load->Model('ListaventasModel');
        $this->load->Model('PresentacionesModel');
        $this->load->Model('ClientesModel');

        $this->hoy = date("Y-m-d");
        $this->mes = date("m");
        $this->anio = date("Y");



        $this->finicio = date("Y") . "-" . date("m") . "-" . "01";
        $this->ffinal = date("Y") . "-" . date("m") . "-" . date("t", strtotime("01-" . date("m") . "-" . date("Y")));
    }

    public function index($mes = '', $anio = '')
    {
        $mes = ($mes == '') ? $this->mes : $mes;
        $anio = ($anio == '') ? $this->anio : $anio;

        $totalventas = 0;
        $totalentradas = 0;
        $contadorminimos = 0;

        #total de las ventas
        $data = $this->VentasModel->get_where(array("MONTH(fecha)" => $mes, "YEAR(fecha)" => $anio, "status"=>1));
        foreach ($data as $v) {
            $totalventas += $v->total;
        }

        #Total de las entradas
        $dataEntradas = $this->EntradasModel->get_where(array("MONTH(fecha)" => $mes, "YEAR(fecha)" => $anio));
        foreach ($dataEntradas as $v) {
            $totalentradas += $v->total;
        }

        #Total de ventas
        $dataVentas = $this->VentasModel->get_where(array("MONTH(fecha)" => $mes, "YEAR(fecha)" => $anio));
        $ventascredito = count($dataVentas);


        #Total entradas
        $dataEntradas = $this->EntradasModel->get_where(array("MONTH(fecha)" => $mes, "YEAR(fecha)" => $anio));
        $entradascredito = count($dataEntradas);


        #Grafica de pie
        $ventasentradas = array(
            array(
                "titulo" => "Ventas",
                "tot" => $totalventas
            ),
            array(
                "titulo" => "Entradas",
                "tot" => $totalentradas
            ),
        );


        #Grafica de barras
        if ($mes != '') {
            $diafinal = date("t", strtotime("01-" . $mes . "-" . $anio));
        } else {
            $diafinal = date("d");
        }
        for ($i = 1; $i <= $diafinal; $i++) {
            $dataVentas = $this->VentasModel->get_where(array("DATE(fecha) " => $anio . "-" . $mes . "-" . $i));
            $monto = 0;
            if (count($dataVentas) <= 0) {
                $monto = 0;
            } else {
                foreach ($dataVentas as $v) {
                    $monto += $v->total;
                }
            }
            $gdias[] = array(
                "dia" => date("Y") . "-" . date("m") . "-" . $i,
                "monto" => $monto
            );
        }

        $dataProductos = $this->ProductosModel->get_where(array("status" => 1));
        foreach ($dataProductos as $p) {
            $stock = $p->stock;
            $minimo = $p->minimo;
            if ($stock <= $minimo) {
                $contadorminimos++;
            }
        }


        $datalista = $this->ListaventasModel->producto_masvendido("DESC", array("MONTH(fecha)" => $mes, "YEAR(fecha)" => $anio));
        if (count($datalista) > 0) {
            $si=1;
            foreach ($datalista as $p) {
                $datapresentacion = $this->PresentacionesModel->get_by_id($p->presentacion);
                $dataproductos = $this->ProductosModel->get_by_id($datapresentacion[0]->idproducto);
                $p->nproducto = $dataproductos[0]->producto;
            }
        } else {
            $datalista[0]=0;
            $si=0;
        }


        $datalistamenos = $this->ListaventasModel->producto_masvendido("ASC", array("MONTH(fecha)" => $mes, "YEAR(fecha)" => $anio));
        if (count($datalistamenos) > 0) {
            $yes = 1;
            foreach ($datalistamenos as $p) {
                $datapresentacion = $this->PresentacionesModel->get_by_id($p->presentacion);
                $dataproductos = $this->ProductosModel->get_by_id($datapresentacion[0]->idproducto);
                $p->nproducto = $dataproductos[0]->producto;
            }
        } else {
            $datalistamenos[0]=0;
            $yes=0;
        }


        #Producto por terminar
        $stockproductos = $this->ProductosModel->get_where(array("status" => 1));
        $prodmin = array();
        foreach ($stockproductos as $k) {
            //echo "id=".$k->idproducto." stock=".$k->stock."minimo=".$k->minimo."<br>";
            $dataminimos = $this->ProductosModel->get_where(array("stock<=" => $k->minimo, "idproducto" => $k->idproducto, "stock>" => 0));
            if (count($dataminimos) > 0) {
                //var_dump($dataminimos[0]);
                $prodmin[] = $dataminimos[0];
            }
            //echo "<br><br>";
        }

        $totaldeuda = 0;
        $madeudor = $this->VentasModel->debe_mas(array("credito"=>1, "MONTH(fecha)" => $mes, "YEAR(fecha)" => $anio));
        foreach ($madeudor as $k) {
            $totaldeuda+=$k->debe;
            $dataCliente = $this->ClientesModel->get_by_id($k->cliente);
            $k->ncliente = $dataCliente[0]->nombre;
        }

        //die;
        $data = array(
            "totalventas" => number_format($totalventas, 2, '.', ','),
            "totalentradas" => number_format($totalentradas, 2, '.', ','),
            "ventascredito" => $ventascredito,
            "entradascredito" => $entradascredito,
            "productosminimo" => $contadorminimos,
            "ventasentradas" => json_encode($ventasentradas),
            "gdias" => json_encode($gdias),
            "masvendido" => $datalista[0],
            "si"=> $si,
            "yes"=>$yes,
            "menosvendido" => $datalistamenos[0],
            "stockproductos" => $prodmin,
            "deudor" => $madeudor,
            "mes"=>$mes,
            "anio"=>$anio,
            "totaldeuda"=>$totaldeuda

        );

        $this->load->view("home", $data);
    }

    public function login()
    {
        $this->session->sess_destroy();
        $this->load->view('login');
    }



    public function insert()
    {
        $dataResponse = $this->input->post();
        $dataUsuario = $this->UsuariosModel->get_by_id($dataResponse['usuario']);
        if (count($dataUsuario)>0) {
            echo 0;
        } else {
            $dataResponse['clave'] = md5($dataResponse['clave']);
            $this->UsuariosModel->insert($dataResponse);
            echo 1;
        }
    }

    public function valida_login()
    {
        $data = $this->input->post();
        $where = array(
            "usuario" => $data['usuario']
        );
        $datosUsua = $this->UsuariosModel->get_where($where);
        if (count($datosUsua) > 0 && $datosUsua[0]->status == 1) {
            $data['clave'] = md5($data['clave']);
            $response = $this->UsuariosModel->get_where($data);
            if (count($response) > 0) {
                $arraydata = array(
                    'nombre' => $response[0]->nombre,
                    'usuario' => $response[0]->usuario,
                    'rol' => $response[0]->rol,
                    'log_in' => true
                );
                $this->session->set_userdata("edegollado", $arraydata);
                if ($response[0]->rol!=3) {
                    echo "ventas/nueva_venta";
                } else {
                    echo 'home/'.$this->mes.'/'.$this->anio;
                }
            } else {
                #La clave no esta bien
                echo 1;
            }
        } else {
            #El usuario no existe
            echo 0;
        }
    }

    public function nuevo()
    {
        if (is_null($this->session->userdata("edegollado"))) {
            redirect(base_url());
        } else {
            $ses = $this->session->userdata("edegollado");
            if ($ses['rol']==3) {
                $this->load->view("nuevo_usuario");
            }
        }
    }

    public function lista()
    {
        if (is_null($this->session->userdata("edegollado"))) {
            redirect(base_url());
        } else {
            $ses = $this->session->userdata("edegollado");
            if ($ses['rol']==3) {
                $dataUsuarios = $this->UsuariosModel->get_where(array("rol!="=>3));
                $data = array(
                    "usuarios"=>$dataUsuarios
                );
                $this->load->view("lista_usuarios", $data);
            }
        }
    }


    public function update($usuario)
    {
        $dataUpdate = $this->input->post();
        if (isset($dataUpdate['clave'])) {
            $dataUpdate['clave'] = md5($dataUpdate['clave']);
            $this->UsuariosModel->update($usuario, $dataUpdate);
            echo 1;
        } else {
            $this->UsuariosModel->update($usuario, $dataUpdate);
            echo 1;
        }
    }

    public function editar($usuario)
    {
        $dataUsuario = $this->UsuariosModel->get_where(array("usuario"=>$usuario));
        $data = array(
            "usuario"=>$dataUsuario[0]
        );

        $this->load->view("editar_usuario", $data);
    }
}
