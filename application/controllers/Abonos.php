<?php
defined('BASEPATH') or exit('No direct script access allowed');

date_default_timezone_set("America/Mexico_City");


class Abonos extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->Model('AbonosModel');
        $this->load->Model('VentasModel');
        $this->load->Model('ClientesModel');
    }



    public function prueba()
    {
        $dataventas = $this->VentasModel->get_by_id(7359);
        echo "Informacion de ventas 1:<br><br>";
        var_dump($dataVentas);
        die;
    }

    public function insert($final=0)
    {
        if (is_null($this->session->userdata("edegollado"))) {
            redirect(base_url());
            die;
        } else {
            if ($final==0) {
                $dataInsert = $this->input->post();
                $this->AbonosModel->insert($dataInsert);
                $dataVentas = $this->VentasModel->get_by_id($dataInsert['idventa']);
                $creditoabonado = $dataVentas[0]->creditoabonado+$dataInsert['monto'];
                $this->VentasModel->update($dataInsert['idventa'], array("creditoabonado"=>$creditoabonado));
                $dataCliente  = $this->ClientesModel->get_by_id($dataVentas[0]->cliente);
                $credito_disponible  = $dataCliente[0]->creditousado- $dataInsert['monto'];
                $this->ClientesModel->update($dataVentas[0]->cliente, array("creditousado"=>$credito_disponible));
            } else {
                $dataInsert = $this->input->post();
                $this->AbonosModel->insert($dataInsert);
                $dataVentas = $this->VentasModel->get_by_id($dataInsert['idventa']);
                $this->VentasModel->update($dataInsert['idventa'], array("liquidado"=>1, "creditoabonado"=>$dataVentas[0]->total, "credito"=>0));
                $dataCliente  = $this->ClientesModel->get_by_id($dataVentas[0]->cliente);
                $creditousado = $dataCliente[0]->creditousado;
                $creditousado= $creditousado-$dataVentas[0]->total;
                $this->ClientesModel->update($dataVentas[0]->cliente, array("creditousado"=>$creditousado));
            }
        }
    }
}
