<?php
defined('BASEPATH') or exit('No direct script access allowed');

date_default_timezone_set("America/Mexico_City");


class Presentaciones extends CI_Controller
{
	public function __construct()
	{

		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->Model('PresentacionesModel');
		$this->hoy = date("Y-m-d");
	}

	public function insert(){
		$dataInsert = $this->input->post();
		echo $this->PresentacionesModel->insert($dataInsert);
	}

	public function get_by_id($idpresentacion){
		$dataPresentacion=$this->PresentacionesModel->get_by_id($idpresentacion);
		echo json_encode($dataPresentacion[0]);
	}


	public function update($idpresentacion){
		$data = $this->input->post();
		$this->PresentacionesModel->update($idpresentacion, $data);
	}

	public function get_where(){
		$dataWhere = $this->input->post();
		$dataPresentaciones = $this->PresentacionesModel->get_where($dataWhere);
		echo json_encode($dataPresentaciones);
	}

}
