<?php
defined('BASEPATH') or exit('No direct script access allowed');

date_default_timezone_set("America/Mexico_City");

require('PDFClientes.php');
require('PDFClientesNotas.php');
class Clientes extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->Model('ClientesModel');
        $this->load->Model('VentasModel');
        $this->load->Model('ListaventasModel');
        $this->load->Model('PresentacionesModel');
        $this->load->Model('ProductosModel');
        $this->load->Model('AbonosModel');

        $this->hoy = date("Y-m-d");
        $this->finicio = date("Y") . "-" . date("m") . "-" . "01";
        $this->ffinal = date("Y") . "-" . date("m") . "-" . date("t", strtotime("01-" . date("m") . "-" . date("Y")));
    }


    public function imprimir_notas()
    {

        $titulo = ($this->input->post("tipo_nota") == 1) ? "NOTAS NO PAGADAS" : "NOTAS PAGADAS";

        $credito = $this->input->post("tipo_nota");



        $fecha = $this->input->post("fecha");

        if ($credito == 0) {
            $header = array('NO. VENTA', 'FECHA', 'TOTAL');
            $x = 50;
        } else if ($credito == 1) {
            $header = array('NO. VENTA', 'FECHA', 'ABONADO', 'FECHA LIM.', 'TOTAL');
            $x = 10;
        }


        $dataCliente = $this->ClientesModel->get_by_id($this->input->post('cliente'));

        $ventas = array();
        $dataVentas = $this->VentasModel->get_where(array("cliente" => $this->input->post('cliente'), "credito" => $credito));
        foreach ($dataVentas as $i) {
            if ($credito == 0) {
                $data = array(
                    0 => $i->idventa,
                    1 => $i->fecha,
                    4 => $i->total
                );
            } else if ($credito == 1) {
                $data = array(
                    0 => $i->idventa,
                    1 => $i->fecha,
                    2 => $i->creditoabonado,
                    3 => $i->fecha_limite,
                    4 => $i->total
                );
            }


            $ventas[] = $data;
        }

        $cliente = $dataCliente[0]->nombre;

        $pdf = new PDFClientesNotas($header, $fecha, $titulo, $x, $cliente);
        $pdf->SetFont('Arial', '', 14);
        $pdf->AddPage();
        $pdf->BasicTable($ventas);

        $pdf->Output('paginaEnBlanco.pdf', 'I');
    }

    public function index()
    {
        if (is_null($this->session->userdata("edegollado"))) {
            redirect(base_url());
        } else {
            #Obtenemos todos los clientes
            $dataClientes = $this->ClientesModel->get_where(array("status" => 1));
            foreach ($dataClientes as $c) {
                $c->btn = '
                    <div class="dropdown">
                        <button
                            class="btn btn-success btn-sm dropdown-toggle configprospectos"
                            data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                            <i class="fas fa-cogs"></i> Config
                        </button>
                        <div class="dropdown-menu listaconfig"
                            aria-labelledby="dropdownMenuButton">
                            <span class="dropdown-item eliminar"
                                id="' . $c->idcliente . '">
                                <i class="fa fa-trash"></i> Eliminar
                            </span>
                            <span class="dropdown-item editar"
                                id="' . $c->idcliente . '">
                                <i class="fas fa-eye"></i> Editar
                            </span>
                            <span class="dropdown-item compras"
                                data-idcliente="' . $c->idcliente . '">
                                <i class="fas fa-eye"></i> Ver compras
                            </span>
                        </div>
                    </div>';
            }

            #Obtenemos todos los clientes que tienen plazo de 15 dias y tienen ventas activas y ya casi vencen;
            $dataClientes_15 = $this->ClientesModel->get_where(array("status" => 1, "diascredito" => 15));
            $t_15 = 0;
            $sum_15 = 0;
            foreach ($dataClientes_15 as $c15) {
                $dataVentas = $this->VentasModel->get_where(array("cliente" => $c15->idcliente, "credito" => 1, "liquidado" => 0, "dias_restantes" => 1));
                if (count($dataVentas) > 0) {
                    $t_15++;
                }
                foreach ($dataVentas as $v) {
                    $sum_15 = $sum_15 + ($v->total - $v->creditoabonado);
                }
            }

            $dataClientes_30 = $this->ClientesModel->get_where(array("status" => 1, "diascredito" => 30));
            $t_30 = 0;
            $sum_30 = 0;
            foreach ($dataClientes_30 as $c30) {
                $dataVentas = $this->VentasModel->get_where(array("cliente" => $c30->idcliente, "credito" => 1, "liquidado" => 0, "dias_restantes" => 1));
                if (count($dataVentas) > 0) {
                    $t_30++;
                }
                foreach ($dataVentas as $v) {
                    $sum_30 = $sum_30 + ($v->total - $v->creditoabonado);
                }
            }

            $data = array(
                "clientes" => $dataClientes,
                "t_15" => $t_15,
                "t_30" => $t_30,
                "sum_15" => "$" . number_format($sum_15),
                "sum_30" => "$" . number_format($sum_30),
                "sum_t" => "$" . number_format($sum_30 + $sum_15),
                "sum_t15_t30" => $t_15 + $t_30
            );
            $this->load->view("lista_clientes", $data);
        }
    }

    public function nuevo()
    {
        if (is_null($this->session->userdata("edegollado"))) {
            redirect(base_url());
        } else {
            $this->load->view("nuevo_cliente");
        }
    }

    public function insert()
    {
        if (is_null($this->session->userdata("edegollado"))) {
            redirect(base_url());
        } else {
            $dataInsert = $this->input->post();
            $dataInsert['create'] = $this->hoy;
            echo $this->ClientesModel->insert($dataInsert);
        }
    }

    public function get_like()
    {
        if (is_null($this->session->userdata("edegollado"))) {
            redirect(base_url());
        } else {
            $json = [];
            $data = $this->input->get("q");
            $dataClientes = $this->ClientesModel->get_like($data);
            $json = $dataClientes;
            echo json_encode($json);
        }
    }

    public function get_by_id($idcliente)
    {
        if (is_null($this->session->userdata("edegollado"))) {
            redirect(base_url());
        } else {
            $dataCliente = $this->ClientesModel->get_by_id($idcliente);
            echo json_encode($dataCliente[0]);
        }
    }

    public function update($idcliente)
    {
        if (is_null($this->session->userdata("edegollado"))) {
            redirect(base_url());
        } else {
            $this->ClientesModel->update($idcliente, $this->input->post());
            echo 1;
        }
    }

    public function editar($idCliente)
    {
        $dataClientes = $this->ClientesModel->get_by_id($idCliente);
        $data = array(
            "cliente" => $dataClientes[0]
        );
        $this->load->view("editar_cliente", $data);
    }

    public function compras($idCliente)
    {
        $dataCliente = $this->ClientesModel->get_by_id($idCliente);
        $dataVentas  = $this->VentasModel->get_where(array("cliente" => $dataCliente[0]->idcliente));
        foreach ($dataVentas as $v) {
            $dataLista = $this->ListaventasModel->get_where(array("idventa" => $v->idventa));
            if ($v->credito == 1 && $v->liquidado == 0) {
                $v->btn_al = "<button class='btn btn-success btn-sm abonar' data-venta='" . $v->idventa . "'>Abonar / Liquidar</button>";
            } else {
                $v->btn_al = '';
            }

            foreach ($dataLista as $l) {
                $dataPresentacion = $this->PresentacionesModel->get_where(array("idpresentacion" => $l->presentacion));
                foreach ($dataPresentacion as $p) {
                    $dataProducto = $this->ProductosModel->get_where(array("idproducto" => $p->idproducto));
                    $p->productos = $dataProducto[0];
                }
                $l->presentaciones = $dataPresentacion[0];
            }
            $v->listav = $dataLista;

            $dataAbonos = $this->AbonosModel->get_where(array("idventa" => $v->idventa));
            $v->abonos = $dataAbonos;
        }
        $data = array(
            "cliente" => $dataCliente[0],
            "ventas" => $dataVentas
        );
        $this->load->view("compras_cliente", $data);
    }


    public function reporte()
    {
        $header = array('NOMBRE', 'TELEFONO', 'RAZON SOCIAL', 'RFC');
        $dataClientes = $this->ClientesModel->select_get_where("nombre, telefono, razonsocial, rfc", array("status" => 1));
        $pdf = new PDFClientes($header);


        $pdf->SetFont('Arial', '', 14);
        $pdf->AddPage();
        $pdf->BasicTable($dataClientes);

        $pdf->Output('paginaEnBlanco.pdf', 'I');
    }

    public function filtro_plazo($plazo)
    {
        $dataResult = array();
        if ($plazo == 0) {
            $dataClientes = $this->ClientesModel->get_where(array("status" => 1, "status_credito" => 1));
        } else {
            $dataClientes = $this->ClientesModel->get_where(array("status" => 1, "status_credito" => 1, "diascredito" => $plazo));
        }
        foreach ($dataClientes as $c) {
            $dataVentas = $this->VentasModel->get_where(array("cliente" => $c->idcliente, "credito" => 1, "liquidado" => 0, "dias_restantes" => 1));
            if (count($dataVentas) > 0) {
                $c->btn = '
                    <div class="dropdown">
                        <button
                            class="btn btn-success btn-sm dropdown-toggle configprospectos"
                            data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                            <i class="fas fa-cogs"></i> Config
                        </button>
                        <div class="dropdown-menu listaconfig"
                            aria-labelledby="dropdownMenuButton">
                            <span class="dropdown-item eliminar"
                                id="' . $c->idcliente . '">
                                <i class="fa fa-trash"></i> Eliminar
                            </span>
                            <span class="dropdown-item editar"
                                id="' . $c->idcliente . '">
                                <i class="fas fa-eye"></i> Editar
                            </span>
                            <span class="dropdown-item compras"
                                data-idcliente="' . $c->idcliente . '">
                                <i class="fas fa-eye"></i> Ver compras
                            </span>
                        </div>
                    </div>';
                $data = array(
                    "nombre" => $c->nombre,
                    "telefono" => $c->telefono,
                    "credito" => "$" . number_format($c->credito),
                    "credito_usado" => "$" . number_format($c->creditousado),
                    "correo" => $c->correo,
                    "btn" => $c->btn
                );
                $dataResult[] = $data;
            }
        }
        echo json_encode($dataResult);
    }
}
