<?php

require('fpdf/fpdf.php');

class PDFProveedores extends FPDF
{
	public function __construct($header)
	{

		parent::__construct();
		$this->header = $header;
	}

	function Header()
	{
		$this->Rect(10, 25, 190, 260);

		$this->Image('assets/develop/images/logo.jpg', 50, 10, -300);
		$this->Ln();
		$this->Image('assets/develop/images/marcaagua.jpg', 45, 60, 125, 150);
		$this->SetXY(12, 45);
		$this->SetFont('Arial', 'B', 9);

		foreach ($this->header as $col) {
			$this->Cell(45, 5, $col,1, 0, "C");
		}
		$this->Ln();

	}

	function Footer()
	{
		$this->SetY(-10);
		$this->SetFont('Arial', 'B', 8);
		$this->Cell(0, 9, utf8_decode('KM 4.5 CARRETERA LA PIEDAD GUADALAJARA   (348) 1214996     AJLPGORA@HOTMAIL.COM'), 0, 0, 'C');
		$this->Cell(-15, 10, utf8_decode('Página ') . $this->PageNo(), 0, 0, 'C');
	}

// Tabla simple
	function BasicTable($data)
	{
		// Cabecera

		// Datos
		$this->SetFont('Arial', 'B', 6);

		foreach ($data as $row) {
			$this->SetX(12);
			foreach ($row as $col) {
				$this->Cell(45, 8, $col, 0, 0, "C");
			}
			$this->Ln();
		}
	}

}
