<?php

require('fpdf/fpdf.php');

class PDFEntrada extends FPDF
{
	public function __construct($cliente, $direccion, $nota, $fecha, $credito, $vendedor, $total)
	{

		parent::__construct();
		$this->cliente = $cliente;
		$this->direccion = $direccion;
		$this->nota = $nota;
		$this->fecha = $fecha;
		$this->credito = $credito;
		$this->vendedor = $vendedor;
		$this->total = $total;


	}

	function Header()
	{
		$this->Rect(10, 25, 190, 240);

		$this->Image('assets/develop/images/logo.jpg', 50, 10, -300);
		$this->Ln();
		$this->Image('assets/develop/images/marcaagua.jpg', 45, 60, 125, 150);
		$this->Ln();
		$this->SetXY(15, 40);
		$this->SetFont('Arial', 'B', 12);

		$this->Cell(140, 7, "FECHA:  " . $this->fecha, 0, 0, "L");
		$this->Cell(40, 7, "NOTA DE ENTRADA", 0, 0, "C");
		$this->Ln();
		$this->SetX(155);
		$this->Cell(40, 7, $this->nota, 0, 0, "C");
		$this->SetX(15);
		$this->Cell(140, 7, "PROVEEDOR:  " . $this->cliente, 0, 0, "L");
		$this->Ln();
		$this->SetX(15);
		$this->Cell(140, 7, "DIRECCION:  " . $this->direccion, 0, 0, "L");
		$this->Ln();
		$this->SetX(15);
		$this->Cell(30, 7, "", 0, 0, "L");
		$this->Cell(150, 7, "USUARIO:  " . $this->vendedor, 0, 0, "R");

	}

	function Footer()
	{
		$this->SetY(-50);
		$this->SetFont('Arial', 'B', 22);
		$this->Cell(190, 10, "TOTAL:  $" . $this->total, 0, 0, "R");


		$this->SetY(-34);
		$this->SetFont('Arial', 'B', 8);
		$this->Cell(0, 9, utf8_decode('KM 4.5 CARRETERA LA PIEDAD GUADALAJARA   (348) 1214996     AJLPGORA@HOTMAIL.COM'), 0, 0, 'C');
		$this->Cell(-15, 10, utf8_decode('Página ') . $this->PageNo(), 0, 0, 'C');
	}

// Tabla simple
	function BasicTable($header, $data)
	{
		// Cabecera
		$this->SetXY(12, 80);
		$this->SetFont('Arial', 'B', 9);

		foreach ($header as $col) {
			$this->Cell(48, 5, $col, 0, 0, "C");
		}
		$this->Ln();
		$this->Ln();
		// Datos

		foreach ($data as $row) {
			$this->SetX(12);

			foreach ($row as $col) {
				$this->Cell(48, 10, $col, 0, 0, "C");

			}
			$this->Ln();
		}
	}

}
