<?php
defined('BASEPATH') or exit('No direct script access allowed');

date_default_timezone_set("America/Mexico_City");


class Abonosentradas extends CI_Controller
{
	public function __construct()
	{

		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->Model('AbonosentradasModel');
		$this->load->Model('EntradasModel');

	}




	public function insert($final=0){
		if (is_null($this->session->userdata("edegollado"))) {
			redirect(base_url());
		} else {
			$dataInsert = $this->input->post();
			if($final==0){
				$this->AbonosentradasModel->insert($dataInsert);
				$dataentradas = $this->EntradasModel->get_by_id($dataInsert['identrada']);
				$creditoabonado = $dataentradas[0]->creditoabonado+$dataInsert['monto'];
				$this->EntradasModel->update($dataInsert['identrada'], array("creditoabonado"=>$creditoabonado));

			}else{
				$this->AbonosentradasModel->insert($dataInsert);
				$dataentradas = $this->EntradasModel->get_by_id($dataInsert['identrada']);

				$this->EntradasModel->update($dataInsert['identrada'], array("liquidado"=>1,"creditoabonado"=>$dataentradas[0]->total, "credito"=>0));
			}
		}
	}



}
