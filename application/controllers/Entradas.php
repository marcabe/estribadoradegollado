<?php
defined('BASEPATH') or exit('No direct script access allowed');

date_default_timezone_set("America/Mexico_City");

require('PDFEntrada.php');
require('PDFEntradareporte.php');

class Entradas extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->Model('ProveedoresModel');
		$this->load->Model('EntradasModel');
		$this->load->Model('ProductosModel');
		$this->load->Model('ProveedoresModel');
		$this->load->Model('PresentacionesModel');
		$this->load->Model('ListaentradasModel');
		$this->load->Model('UsuariosModel');

		$this->hoy = date("Y-m-d");
		$this->finicio = date("Y") . "-" . date("m") . "-" . "01";
		$this->ffinal = date("Y") . "-" . date("m") . "-" . date("t", strtotime("01-" . date("m") . "-" . date("Y")));
	}


	public function index()
	{
		$dataEntradas = $this->EntradasModel->get_where(array("status"=>1));

		foreach ($dataEntradas as $e) {
			$dataProveedor = $this->ProveedoresModel->get_by_id($e->idproveedor);
			$e->proveedor = $dataProveedor[0];
			$dataUsuario = $this->UsuariosModel->get_by_id($e->usuario);
			$e->usuario = $dataUsuario[0];
		}
		$data = array(
			"entradas" => $dataEntradas);
		$this->load->view("lista_entradas", $data);
	}

	public function get_where()
	{
		$dataWhere = $this->input->post();
		$dataVentas = $this->EntradasModel->get_where($dataWhere);
		echo json_encode($dataVentas[0]);
	}

	public function nueva()
	{
		if (is_null($this->session->userdata("edegollado"))) {
			redirect(base_url());
		} else {
			$dataEntrada = $this->EntradasModel->last_id();
			if (count($dataEntrada) > 0) {
				$noentrada = $dataEntrada[0]->identrada + 1;
			} else {
				$noentrada = 1;
			}
			$data = array(
				"hoy" => $this->hoy,
				"noventa" => $noentrada
			);
			$this->load->view("entradas", $data);
		}
	}


	public function insert()
	{

		$sesion = $this->session->userdata("edegollado");
		$datos = $this->input->post();
		$infoentrada = json_decode($datos['datosentrada']);
		$entradas = $infoentrada->entrada;
		$entradas->usuario = $sesion['usuario'];
		$listaentradas = $infoentrada->elementoslista;
		$identrada = $this->EntradasModel->insert($entradas);

		foreach ($listaentradas as $e) {
			$dataPresentacion = $this->PresentacionesModel->get_by_id($e->presentacion);
			$sumar = $e->cantidad * $dataPresentacion[0]->cantidadminima;
			$dataProducto = $this->ProductosModel->get_by_id($dataPresentacion[0]->idproducto);
			$stocknuevo = $dataProducto[0]->stock + $sumar;
			$this->ProductosModel->update($dataProducto[0]->idproducto, array("stock" => $stocknuevo));
			$e->identrada = $identrada;
			$this->ListaentradasModel->insert($e);
		}
		echo $identrada;

	}


	public function impresion($identrada)
	{

		$dataPdf = array();
		$dataListaEntradas = $this->ListaentradasModel->get_where(array("identrada" => $identrada));
		foreach ($dataListaEntradas as $v) {
			$dataPdf[] = array();
			$dataPresentacion = $this->PresentacionesModel->get_by_id($v->presentacion);
			$presentacion = $dataPresentacion[0]->presentacion;
			$dataProducto = $this->ProductosModel->get_by_id($dataPresentacion[0]->idproducto);
			$producto = $rest = substr($dataProducto[0]->producto, 0, 15)."...";  // devuelve "abcde"
			$cantidad = $v->cantidad;
			$subtotal = number_format($v->subtotal, 2, '.', ',');
			$dataPdf[] = array(
				0 => $producto,
				1 => $presentacion,
				2 => $cantidad,
				3 => "$ " . $subtotal
			);
		}
		$dataEntrada = $this->EntradasModel->get_by_id($identrada);
		foreach ($dataEntrada as $v) {
			$dataProveedor = $this->ProveedoresModel->get_by_id($v->idproveedor);
			$cliente = $dataProveedor[0]->nombre;
			$direccion = $dataProveedor[0]->direccion;
			$nota = $v->identrada;
			$fecha = $v->fecha;
			$dataUsuario = $this->UsuariosModel->get_by_id($v->usuario);
			$usuario = $dataUsuario[0]->nombre;
			$total = number_format($v->total, 2, '.', ',');
		}
		$header = array('PRODUCTO', 'PRESENTACION', 'CANTIDAD', 'PRECIO UNITARIO');


		$pdf = new PDFEntrada($cliente, $direccion, $nota, $fecha, "", $usuario, $total);


		$pdf->SetFont('Arial', '', 14);
		$pdf->AddPage();
		$pdf->BasicTable($header, $dataPdf);

		$pdf->Output('paginaEnBlanco.pdf', 'I');

	}

	public function eliminar($identrada)
	{
		$this->EntradasModel->update($identrada, array("status" => 0));
		$datalistaentradas = $this->ListaentradasModel->get_where(array("identrada" => $identrada));
		foreach ($datalistaentradas as $e) {
			$this->ListaentradasModel->update($e->idlistaentrada, array("status" => 0));

			$dataPresentacion = $this->PresentacionesModel->get_by_id($e->presentacion);
			$totalp = $dataPresentacion[0]->cantidadminima * $e->cantidad;

			$dataProducto = $this->ProductosModel->get_by_id($dataPresentacion[0]->idproducto);
			$stock = $dataProducto[0]->stock - $totalp;
			$this->ProductosModel->update($dataProducto[0]->idproducto, array("stock"=>$stock));
		}
	}

	public function reporte(){
		$header = array('NOMBRE','RAZON SOIAL', 'TOTAL', 'USUARIO', 'FECHA');
		$dataClientes = $this->EntradasModel->select_get_where("proveedores.nombre,
			proveedores.razonsocial,
			entradas.total,
			usuarios.nombre AS u,
			entradas.fecha", array("entradas.status"=>1));
		$pdf = new PDFEntradareporte($header);


		$pdf->SetFont('Arial', '', 14);
		$pdf->AddPage();
		$pdf->BasicTable($dataClientes);

		$pdf->Output('paginaEnBlanco.pdf', 'I');
	}

}
