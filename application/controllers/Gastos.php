<?php
defined('BASEPATH') or exit('No direct script access allowed');

date_default_timezone_set("America/Mexico_City");

require('fpdf/fpdf.php');

class Gastos extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->Model('GastosModel');
    }



    public function index()
    {
        $dataGastos = $this->GastosModel->get();

        foreach ($dataGastos as $g) {
            $g->button = "<button class='btn btn-success btn-sm impresion_gasto' data-id=" . $g->idgasto . ">Imprimir</button>";
        }
        $data = array(
            "gastos" => $dataGastos
        );
        $this->load->view("lista_gastos", $data);
    }


    public function impresion_gasto($idgasto)
    {
        $dataGastos = $this->GastosModel->get_where(array("idgasto" => $idgasto));

        $pdf = new FPDF();
        $pdf->AddPage();
        $pdf->Rect(10, 25, 190, 260);
        $pdf->Image('assets/develop/images/logo.jpg', 50, 10, -300);
        $pdf->Ln();
        $pdf->Image('assets/develop/images/marcaagua.jpg', 45, 60, 125, 150);
        $pdf->SetXY(10, 45);
        $pdf->SetFont('Arial', 'B', 12);

        $pdf->Cell(190, 10, 'GASTO INGRESADO: ', 0, 0, "C");
        $pdf->Ln();
        $pdf->Ln();
        $pdf->SetFont('Arial', 'B', 10);

        $pdf->SetX(10);
        $pdf->Cell(20, 10, 'ID GASTO', 1, 0, "C");
        $pdf->Cell(120, 10, 'MOTIVO', 1, 0, "C");
        $pdf->Cell(30, 10, 'FECHA', 1, 0, "C");
        $pdf->Cell(20, 10, 'MONTO', 1, 0, "C");
        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Ln();
        $pdf->Cell(20, 10, $dataGastos[0]->idgasto, 1, 0, "C");
        $pdf->Cell(120, 10, $dataGastos[0]->motivo, 1, 0, "C");
        $pdf->Cell(30, 10, $dataGastos[0]->fecha, 1, 0, "C");
        $pdf->Cell(20, 10, "$" . number_format($dataGastos[0]->monto), 1, 0, "C");
        $pdf->Output();
    }

    public function insert()
    {
        $insert = $this->input->post();
        $this->GastosModel->insert($insert);
        $dataGastos = $this->GastosModel->get();

        $dataResponse = array();

        foreach ($dataGastos as $g) {
            $gasto = array(
                "no_gasto" => $g->idgasto,
                "motivo" => $g->motivo,
                "monto" => $g->monto,
                "fecha" => $g->fecha,
                "btn" => "<button class='btn btn-success btn-sm impresion_gasto' data-id=" . $g->idgasto . ">Imprimir</button>"
            );
            $dataResponse[] = $gasto;
        }

        echo json_encode($dataResponse);
    }
}
