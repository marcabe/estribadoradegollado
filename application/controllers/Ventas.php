<?php
defined('BASEPATH') or exit('No direct script access allowed');

date_default_timezone_set("America/Mexico_City");
setlocale(LC_MONETARY, 'en_US');

require('PDFVenta.php');
require('PDFVentareporte.php');
require('PDFVentareporteCierre.php');


class Ventas extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->Model('VentasModel');
        $this->load->Model('ListaventasModel');
        $this->load->Model('PresentacionesModel');
        $this->load->Model('ProductosModel');
        $this->load->Model('ClientesModel');
        $this->load->Model('UsuariosModel');
        $this->load->Model('AbonosModel');
        $this->load->Model('GastosModel');
        $this->load->Model('IngresosModel');

        $this->hoy = date("Y-m-d");
    }

    public function filtro_ajax($anio, $mes)
    {
        $dataResult = array();
        $cre = '';
        $dataVentas = $this->VentasModel->get_where(array("status" => 1, "MONTH(date(fecha))" => $mes, "YEAR(date(fecha))" => $anio));
        foreach ($dataVentas as $v) {
            $dataClientes = $this->ClientesModel->get_by_id($v->cliente);
            $v->clie = $dataClientes[0];
            $dataUsuarios = $this->UsuariosModel->get_by_id($v->usuario);
            $v->usu = $dataUsuarios[0];

            $dataClientes = $this->ClientesModel->get_by_id($v->cliente);
            $v->clie = $dataClientes[0];
            $dataUsuarios = $this->UsuariosModel->get_by_id($v->usuario);
            $v->usu = $dataUsuarios[0];
            if ($v->credito == 1) {
                $v->btn = '
                <div class="dropdown">
                    <button
                        class="btn btn-success btn-sm dropdown-toggle configprospectos"
                        data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        <i class="fas fa-cogs"></i> Config
                    </button>
                    <div class="dropdown-menu listaconfig"
                        aria-labelledby="dropdownMenuButton">
                        <span class="dropdown-item abonar"
                            id="' . $v->idventa . '">
                            <i class="fas fa-eye"></i> Abonar/Liquidar
                        </span>
                        <span class="dropdown-item imprimir"
                            id="' . $v->idventa . '">
                            <i class="fas fa-eye"></i> Imprimir
                        </span>
                        <span class="dropdown-item eliminar"
                            id="' . $v->idventa . '">
                            <i class="fa fa-trash"></i> Eliminar
                        </span>
                    </div>';
            } else {
                $v->btn = '
                <div class="dropdown">
                    <button
                        class="btn btn-success btn-sm dropdown-toggle configprospectos"
                        data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        <i class="fas fa-cogs"></i> Config
                    </button>
                    <div class="dropdown-menu listaconfig"
                        aria-labelledby="dropdownMenuButton">
                        <span class="dropdown-item imprimir"
                            id="' . $v->idventa . '">
                            <i class="fas fa-eye"></i> Imprimir
                        </span>
                        <span class="dropdown-item eliminar"
                            id="' . $v->idventa . '">
                            <i class="fa fa-trash"></i> Eliminar
                        </span>
                    </div>';
            }

            $data = array(
                "no_venta" => $v->idventa,
                "cliente" => $v->clie->nombre,
                "direccion" => $v->direccion,
                "credito" => $v->credito,
                "total" => "$" . number_format($v->total),
                "vendedor" => $v->usu->nombre,
                "fecha" => $v->fecha,
                "btn" => $v->btn
            );
            $dataResult[] = $data;
        }
        echo json_encode($dataResult);
    }

    public function index()
    {
        if (is_null($this->session->userdata("edegollado"))) {
            redirect(base_url());
        } else {
            $dataVentas = $this->VentasModel->get_where(array("status" => 1, "MONTH(date(fecha))" => date("m"), "YEAR(date(fecha))" => date("Y")));
            foreach ($dataVentas as $v) {
                $dataClientes = $this->ClientesModel->get_by_id($v->cliente);
                $v->clie = $dataClientes[0];
                $dataUsuarios = $this->UsuariosModel->get_by_id($v->usuario);
                $v->usu = $dataUsuarios[0];
            }
            $data = array(
                "ventas" => $dataVentas,
                "mes" => date("m"),
                "anio" => date("Y")
            );
            $this->load->view("lista_ventas", $data);
        }
    }


    public function filtro($anio, $mes)
    {
        if (is_null($this->session->userdata("edegollado"))) {
            redirect(base_url());
        } else {
            $dataVentas = $this->VentasModel->get_where(array("status" => 1, "MONTH(fecha)" => $mes, "YEAR(fecha)" => $anio));
            foreach ($dataVentas as $v) {
                $dataClientes = $this->ClientesModel->get_by_id($v->cliente);
                $v->clie = $dataClientes[0];
                $dataUsuarios = $this->UsuariosModel->get_by_id($v->usuario);
                $v->usu = $dataUsuarios[0];
            }
            $data = array(
                "ventas" => $dataVentas
            );
            $this->load->view("lista_ventas_filtro", $data);
        }
    }

    public function nueva()
    {
        if (is_null($this->session->userdata("edegollado"))) {
            redirect(base_url());
        } else {
            $dataVentas = $this->VentasModel->last_id();
            if (count($dataVentas) > 0) {
                $noventa = $dataVentas[0]->idventa + 1;
            } else {
                $noventa = 1;
            }
            $data = array(
                "hoy" => $this->hoy,
                "noventa" => $noventa
            );
            $this->load->view("ventas", $data);
        }
    }

    public function insert()
    {
        $sesion = $this->session->userdata("edegollado");
        $datos = $this->input->post();
        $infoventas = json_decode($datos['datosventa']);

        $venta = $infoventas->ventas;
        $venta->usuario = $sesion['usuario'];
        $listaventas = $infoventas->elementoslista;
        //die;
        if ($venta->credito == 1) {
            $dataCliente = $this->ClientesModel->get_by_id($venta->cliente);
            $creditousado = $dataCliente[0]->creditousado + $venta->total;
            $this->ClientesModel->update($venta->cliente, array("creditousado" => $creditousado));
            $fecha_limite = date("Y-m-d", strtotime(date("Y-m-d") . "+ " . $dataCliente[0]->diascredito . " days"));
            $venta->dias_restantes = $dataCliente[0]->diascredito;
            $venta->fecha_limite = $fecha_limite;
        }
        $idventa = $this->VentasModel->insert($venta);

        foreach ($listaventas as $v) {
            $dataPresentacion = $this->PresentacionesModel->get_by_id($v->presentacion);
            $descontar = $v->cantidad * $dataPresentacion[0]->cantidadminima;
            $dataProducto = $this->ProductosModel->get_by_id($dataPresentacion[0]->idproducto);
            $stocknuevo = $dataProducto[0]->stock - $descontar;
            $this->ProductosModel->update($dataProducto[0]->idproducto, array("stock" => $stocknuevo));
            $v->idventa = $idventa;
            $id_lv = $this->ListaventasModel->insert($v);
        }

        echo $idventa;
    }


    public function impresion_venta($idVenta)
    {
        $dataPdf = array();
        $dataListaVentas = $this->ListaventasModel->get_where(array("idventa" => $idVenta));
        foreach ($dataListaVentas as $v) {
            $dataPdf[] = array();
            $dataPresentacion = $this->PresentacionesModel->get_by_id($v->presentacion);
            $presentacion = $dataPresentacion[0]->presentacion;
            $dataProducto = $this->ProductosModel->get_by_id($dataPresentacion[0]->idproducto);
            $producto = $rest = substr($dataProducto[0]->producto, 0, 15) . "...";  // devuelve "abcde"
            $tipoVenta = $v->tipoventa;
            $cantidad = $v->cantidad;
            if ($v->pespecial == 1) {
                $preciounitario = $v->unitarioespecial;
            } else {
                if ($tipoVenta == "Menudeo") {
                    $preciounitario = number_format($dataPresentacion[0]->menudeo, 2, '.', ',');
                } else {
                    $preciounitario = number_format($dataPresentacion[0]->mayoreo, 2, '.', ',');
                }
            }

            $subtotal = number_format($v->subtotal, 2, '.', ',');
            $dataPdf[] = array(
                0 => $producto,
                1 => $presentacion,
                2 => $cantidad,
                3 => "$ " . $preciounitario,
                4 => "$ " . $subtotal
            );
        }
        $dataVenta = $this->VentasModel->get_by_id($idVenta);
        foreach ($dataVenta as $v) {
            $direccion = $v->direccion;
            $dataCliente = $this->ClientesModel->get_by_id($v->cliente);
            $cliente = $dataCliente[0]->nombre;
            $nota = $v->idventa;
            $fecha = $v->fecha;
            $credito = ($v->credito == 1) ? "SI" : "NO";
            $dataUsuario = $this->UsuariosModel->get_by_id($v->usuario);
            $usuario = $dataUsuario[0]->nombre;
            $total = number_format($v->total, 2, '.', ',');
            $abonado = number_format($v->creditoabonado, 2, '.', ',');
            $liquidado = $v->liquidado;
        }
        $dataAbonos = $this->AbonosModel->get_where(array("idventa" => $nota));
        if (count($dataAbonos) > 0) {
            $header_abono = array('NO.', 'MONTO', 'FECHA');
            $dataAbonos = $this->AbonosModel->get_where(array("idventa" => $nota));
            $i = 1;
            foreach ($dataAbonos as $a) {
                $data_abono[] = array(
                    0 => $i,
                    1 => number_format($a->monto, 2, '.', ','),
                    2 => $a->fecha
                );
                $i++;
            }
        } else {
            $header_abono = 0;
            $data_abono = 0;
        }





        $header = array('PRODUCTO', 'PRESENTACION', 'CANTIDAD', 'PRECIO UNITARIO', 'SUBTOTAL');


        $pdf = new PDFVenta($cliente, $direccion, $nota, $fecha, $credito, $usuario, $total, $abonado, $liquidado);


        $pdf->SetFont('Arial', '', 14);
        $pdf->AddPage();
        $pdf->BasicTable($header, $dataPdf, $header_abono, $data_abono);

        $pdf->Output('paginaEnBlanco.pdf', 'I');
    }

    public function get_where()
    {
        $dataWhere = $this->input->post();
        $dataVentas = $this->VentasModel->get_where($dataWhere);
        echo json_encode($dataVentas[0]);
    }

    public function ventas_deudas()
    {
        $data = $this->input->post();
        unset($data[0]);
        $dataVentas = $this->VentasModel->get_where($data);
        echo json_encode($dataVentas);
    }

    public function eliminar($idVenta)
    {
        $this->VentasModel->update($idVenta, array("status" => 0));
        $datalistaventas = $this->ListaventasModel->get_where(array("idventa" => $idVenta));
        foreach ($datalistaventas as $v) {
            $this->ListaventasModel->update($v->idlistaventas, array("status" => 0));

            $dataPresentacion = $this->PresentacionesModel->get_by_id($v->presentacion);
            $totalp = $dataPresentacion[0]->cantidadminima * $v->cantidad;

            $dataProducto = $this->ProductosModel->get_by_id($dataPresentacion[0]->idproducto);
            $stock = $dataProducto[0]->stock + $totalp;
            $this->ProductosModel->update($dataProducto[0]->idproducto, array("stock" => $stock));
        }
        $dataVentas = $this->VentasModel->get_by_id($idVenta);
        //Obtenemos si es a credito o no
        $credito = $dataVentas[0]->credito;
        if ($credito == 1) {
            //Obtenemos el credito abonado
            $totalventa  = $dataVentas[0]->total;
            //obtenemos la informacion del cliente para saber su credito
            $idCliente = $dataVentas[0]->cliente;
            $dataCliente = $this->ClientesModel->get_by_id($idCliente);
            //Obtenemos el credito usado y le sumamoos el credito abonado de la venta
            $creditousado = $dataCliente[0]->creditousado - $totalventa;
            echo $creditousado;
            $this->ClientesModel->update($idCliente, array("creditousado" => $creditousado));
        }
    }

    public function reporte()
    {
        $header = array('CLIENTE', 'CREDITO', 'TOTAL', 'VENDEDOR', 'FECHA');
        $dataClientes = $this->VentasModel->select_get_where("clientes.nombre AS nc,
			if(ventas.credito = 1, 'Si', 'No') AS credito,
			ventas.total,
			usuarios.nombre AS nu,
			ventas.fecha", array("ventas.status" => 1));
        $pdf = new PDFVentareporte($header);


        $pdf->SetFont('Arial', '', 14);
        $pdf->AddPage();
        $pdf->BasicTable($dataClientes);

        $pdf->Output('paginaEnBlanco.pdf', 'I');
    }


    public function chrone_job_ventas()
    {
        $dataVentas = $this->VentasModel->get_where(array("credito" => 1, "liquidado" => 0, "dias_restantes>=" => 2));
        foreach ($dataVentas as $v) {
            $dias = $v->dias_restantes;
            $dias--;
            $this->VentasModel->update($v->idventa, array("dias_restantes" => $dias));
        }
    }

    public function arregla()
    {
        $where = array(
            "STATUS" => 1,
            "MONTH(date(fecha))" => 10,
            "YEAR(date(fecha))" => 2021,
            "credito" => 1,
            "liquidado" => 0,
        );

        $dataVentas = $this->VentasModel->get_where($where);
        $i = 1;
        foreach ($dataVentas as $v) {
            $dataCliente = $this->ClientesModel->get_by_id($v->cliente);
            if ($dataCliente[0]->status != 0) {
                $fecha_limite = date("Y-m-d", strtotime($v->fecha . "+ " . $dataCliente[0]->diascredito . " days"));
                echo "$i CLIENTE: " . $dataCliente[0]->idcliente . " tiene " . $dataCliente[0]->diascredito . " dias de redito<br>";
                echo "La fecha es " . $v->fecha . " y la fecha limite es " . $fecha_limite . "<br>";
                $date1 = new DateTime("2021-11-15");
                $date2 = new DateTime($fecha_limite);
                $diff = $date2->diff($date1);
                echo "Los dias restantes que le quedan son " . $diff->invert . " " . $diff->days . ' dias<br><br>';
                if ($diff->invert == 0) {
                    $this->VentasModel->update($v->idventa, array("dias_restantes" => 1, "fecha_limite" => $fecha_limite));
                } else {
                    $this->VentasModel->update($v->idventa, array("dias_restantes" => $diff->days, "fecha_limite" => $fecha_limite));
                }
                $i++;
            }
        }
    }

    public function reporte_cierre()
    {
        $fecha = $this->input->post("fecha");
        $header = array('NO. VENTA', 'NOMBRE', 'CREDITO', 'TOTAL');
        $header_gasto = array('NO.', 'MOTIVO', 'GASTO');
        $header_abonos = array('NO.', 'ID VENTA', 'MONTO', 'FECHA');
        $header_ingresos = array('NO.', 'MOTIVO', 'INGRESO');

        $dataVentasCredito = $this->VentasModel->select_get_where_cierre("ventas.idventa,
        clientes.nombre,
        if(ventas.credito = 1, 'Si', 'No') AS credito,
        ventas.total", array("DATE(`ventas`.`fecha`)" => $fecha, "ventas.credito" => 1, "ventas.liquidado" => 0, "ventas.fecha_limite!=" => "0000-00-00"));


        $dataVentasCredito2 = $this->VentasModel->select_get_where_cierre("ventas.idventa,
        clientes.nombre,
        if(ventas.credito = 0, 'Si', 'Si/Liquidado') AS credito,
        ventas.total", array("DATE(`ventas`.`fecha`)" => $fecha, "ventas.credito" => 0, "ventas.liquidado" => 1, "ventas.fecha_limite!=" => "0000-00-00"));
        foreach ($dataVentasCredito2 as $vc) {
            $dataVentasCredito[] = $vc;
        }

        $dataVentas = $this->VentasModel->select_get_where_cierre("ventas.idventa,
        clientes.nombre,
        if(ventas.credito = 1, 'Si', 'No') AS credito,
        ventas.total", array("DATE(`ventas`.`fecha`)" => $fecha, "ventas.credito" => 0, "ventas.fecha_limite" => "0000-00-00"));

        $dataAbonos = $this->AbonosModel->get_where(array("DATE(`fecha`)" => $fecha, "forma_pago" => "e"));
        $dataAbonos_trans = $this->AbonosModel->get_where(array("DATE(`fecha`)" => $fecha, "forma_pago" => "t"));

        $ingresos = array();
        $dataIngresos = $this->IngresosModel->get_where(array("status" => 1, "DATE(`fecha`)" => $fecha));
        foreach ($dataIngresos as $i) {
            $data = array(
                0 => $i->idIngreso,
                1 => $i->motivo,
                2 => $i->ingreso
            );
            $ingresos[] = $data;
        }

        $gastos = array();
        $dataGasto = $this->GastosModel->get_where(array("status" => 1, "DATE(`fecha`)" => $fecha));
        foreach ($dataGasto as $g) {
            $data = array(
                0 => $g->idgasto,
                1 => $g->motivo,
                2 => $g->monto
            );
            $gastos[] = $data;
        }

        $pdf = new PDFVentareporteCierre($header, $header_gasto, $header_abonos, $fecha, $header_ingresos);
        $pdf->SetFont('Arial', '', 14);
        $pdf->AddPage();
        $pdf->BasicTable($dataVentas, $dataVentasCredito, $gastos, $dataAbonos, $ingresos, $dataAbonos_trans);

        $pdf->Output('paginaEnBlanco.pdf', 'I');
    }
}
