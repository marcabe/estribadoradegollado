<?php


class PDFVentareporteCierre extends FPDF
{
	public function __construct($header, $header_gasto, $header_abono, $fecha, $header_ingresos)
	{

		parent::__construct();
		$this->header = $header;
		$this->header_gasto = $header_gasto;
		$this->header_ingresos = $header_ingresos;
		$this->header_abono = $header_abono;
		$this->fecha = $fecha;
	}

	function Header()
	{
		$this->Rect(10, 25, 190, 260);

		$this->Image('assets/develop/images/logo.jpg', 50, 10, -300);
		$this->Ln();
		$this->Image('assets/develop/images/marcaagua.jpg', 45, 60, 125, 150);
		$this->SetXY(12, 45);
		$this->SetFont('Arial', 'B', 9);


		$this->Cell(90, 5, "REPORTE DEL DiA: " . $this->fecha, 0, 0, "L");
		$this->Ln();
		$this->Ln();
	}

	function Footer()
	{
		$this->SetY(-10);
		$this->SetFont('Arial', 'B', 8);
		$this->Cell(0, 9, utf8_decode('KM 4.5 CARRETERA LA PIEDAD GUADALAJARA   (348) 1214996     AJLPGORA@HOTMAIL.COM'), 0, 0, 'C');
		$this->Cell(-15, 10, utf8_decode('Página ') . $this->PageNo(), 0, 0, 'C');
	}

	// Tabla simple
	function BasicTable($ventas, $credito, $pagos, $abonos, $ingresos, $abonos_trans)
	{
		// Cabecera
		$totalv = 0;
		foreach ($ventas as $row) {
			$i = 1;
			foreach ($row as $col) {
				if ($i == 4) {
					$totalv += $col;
					$i = -1;
				}
				$i++;
			}
		}

		$totalc = 0;
		foreach ($credito as $row) {
			$i = 1;
			foreach ($row as $col) {
				if ($i == 4) {
					$totalc += $col;
					$i = -1;
				}
				$i++;
			}
		}
		$totalp = 0;
		foreach ($pagos as $row) {
			$i = 0;
			foreach ($row as $col) {
				if ($i == 2) {
					$totalp += $col;
				}
				$i++;
			}
		}


		$totalab = 0;
		foreach ($abonos as $row) {
			$i = 0;
			foreach ($row as $col) {
				if ($i == 2) {
					$totalab += $col;
				}
				$i++;
			}
		}

		$totalabt = 0;
		foreach ($abonos_trans as $row) {
			$i = 0;
			foreach ($row as $col) {
				if ($i == 2) {
					$totalabt += $col;
				}
				$i++;
			}
		}

		$totali = 0;
		foreach ($ingresos as $row) {
			$i = 0;
			foreach ($row as $col) {
				if ($i == 2) {
					$totali += $col;
				}
				$i++;
			}
		}

		$this->SetFont('Arial', 'B', 8);

		$this->SetX(10);
		$this->Cell(40, 5, "ABONOS PAGADOS (" . count($abonos) . ")", 1, 0, "C");
		$this->Cell(40, 5, "VENTAS EN EFECTIVO (" . count($ventas) . ")", 1, 0, "C");
		$this->Cell(40, 5, "VENTAS A CREDITO (" . count($credito) . ")", 1, 0, "C");
		$this->Cell(35, 5, "TOTAL GASTOS (" . count($pagos) . ")", 1, 0, "C");
		$this->Cell(35, 5, "TOTAL EN CAJA", 1, 0, "C");
		$this->Ln();
		$this->Cell(40, 5, "$" . number_format($totalab), 1, 0, "R");
		$this->Cell(40, 5, "$" . number_format($totalv), 1, 0, "R");
		$this->Cell(40, 5, "$" . number_format($totalc), 1, 0, "R");
		$this->Cell(35, 5, "$" . number_format($totalp), 1, 0, "R");
		$this->Cell(35, 5, "$" . number_format($totalv - $totalp + $totalab + $totali), 1, 0, "R");

		$this->Ln();

		$this->SetX(10);
		$this->Cell(40, 5, "ABONOS TRANSF. (" . count($abonos_trans) . ")", 1, 0, "C");
		$this->Cell(40, 5, "OTROS INGRESOS (" . count($ingresos) . ")", 1, 0, "C");
		$this->Ln();
		$this->Cell(40, 5, "$" . number_format($totalabt), 1, 0, "R");
		$this->Cell(40, 5, "$" . number_format($totali), 1, 0, "R");


		$this->Ln();



		#ABONOS
		$this->Ln();
		$this->SetFont('Arial', 'B', 15);
		$this->Cell(190, 5, "ABONOS", 0, 0, "C");
		$this->Ln();
		$this->Ln();
		$this->SetFont('Arial', 'B', 10);
		foreach ($this->header_abono as $col) {
			$this->Cell(45, 5, $col, 1, 0, "C");
		}
		$this->Ln();
		$totalab = 0;
		foreach ($abonos as $row) {
			$i = 0;
			foreach ($row as $col) {
				if ($i == 0) {
					$this->Cell(45, 5, $col, 1, 0, "C");
				} elseif ($i == 2) {
					$this->Cell(45, 5, "$" . number_format($col), 1, 0, "C");
					$totalab += $col;
				} elseif ($i == 4) {
				} else {
					$this->Cell(45, 5, $col, 1, 0, "L");
				}
				$i++;
			}
			$this->Ln();
		}
		$this->SetFont('Arial', 'B', 15);
		$this->Cell(190, 8, "ABONOS PAGADOS: $" . number_format($totalab), 1, 0, "R");
		$this->Ln();
		$this->Ln();


		$this->Ln();
		$this->SetFont('Arial', 'B', 15);
		$this->Cell(190, 5, "ABONOS POR TRANSFERENCIA", 0, 0, "C");
		$this->Ln();
		$this->Ln();
		$this->SetFont('Arial', 'B', 10);
		foreach ($this->header_abono as $col) {
			$this->Cell(45, 5, $col, 1, 0, "C");
		}
		$this->Ln();

		$totalabt = 0;
		foreach ($abonos_trans as $row) {
			$i = 0;
			foreach ($row as $col) {
				if ($i == 0) {
					$this->Cell(45, 5, $col, 1, 0, "C");
				} elseif ($i == 2) {
					$this->Cell(45, 5, "$" . number_format($col), 1, 0, "C");
					$totalabt += $col;
				} elseif ($i == 4) {
				} else {
					$this->Cell(45, 5, $col, 1, 0, "L");
				}

				$i++;
			}
			$this->Ln();
		}
		$this->SetFont('Arial', 'B', 15);
		$this->Cell(190, 8, "ABONOS POR TRANSF: $" . number_format($totalabt), 1, 0, "R");
		$this->Ln();
		$this->Ln();


		// Ventas
		$this->Ln();
		$this->SetFont('Arial', 'B', 15);
		$this->Cell(190, 5, "VENTAS", 0, 0, "C");
		$this->Ln();
		$this->Ln();
		$this->SetFont('Arial', 'B', 10);
		foreach ($this->header as $col) {
			$this->Cell(45, 5, $col, 1, 0, "C");
		}
		$this->Ln();
		$total = 0;
		foreach ($ventas as $row) {
			$this->SetX(10);
			$i = 1;
			foreach ($row as $col) {
				if ($i == 4) {
					$this->Cell(45, 5, "$" . number_format($col), 1, 0, "C");
					$total += $col;
					$i = -1;
				} else {
					$this->Cell(45, 5, $col, 1, 0, "C");
				}
				$i++;
			}
			$this->Ln();
		}
		$this->SetFont('Arial', 'B', 15);
		$this->Cell(190, 8, "VENTAS EN EFECTIVO: $" . number_format($total), 1, 0, "R");
		$this->Ln();
		$this->Ln();


		$this->Ln();
		$this->SetFont('Arial', 'B', 15);
		$this->Cell(190, 5, "VENTAS A CREDITO", 0, 0, "C");
		$this->Ln();
		$this->Ln();

		$this->SetFont('Arial', 'B', 10);
		$total = 0;

		foreach ($this->header as $col) {
			$this->Cell(47, 5, $col, 1, 0, "C");
		}
		$this->Ln();

		$this->SetFont('Arial', 'B', 7);

		foreach ($credito as $row) {
			$this->SetX(10);
			$i = 1;
			foreach ($row as $col) {
				if ($i == 4) {
					$this->Cell(47, 5, "$" . number_format($col), 1, 0, "C");
					$total += $col;
					$i = -1;
				} else {
					$this->Cell(47, 5, $col, 1, 0, "C");
				}
				$i++;
			}
			$this->Ln();
		}
		$this->SetFont('Arial', 'B', 15);
		$this->Cell(190, 8, "VENTAS A CREDITO: $" . number_format($total), 1, 0, "R");
		$this->Ln();
		$this->Ln();



		$this->Ln();
		$this->SetFont('Arial', 'B', 15);
		$this->Cell(190, 5, "GASTOS", 0, 0, "C");
		$this->Ln();
		$this->Ln();
		$i = 0;
		$this->SetFont('Arial', 'B', 10);

		foreach ($this->header_gasto as $col) {
			if ($i == 0) {
				$this->Cell(15, 5, $col, 1, 0, "C");
			} elseif ($i == 2) {
				$this->Cell(35, 5, $col, 1, 0, "C");
			} else {
				$this->Cell(140, 5, $col, 1, 0, "C");
			}
			$i++;
		}
		$this->Ln();
		$total = 0;
		foreach ($pagos as $row) {
			$this->SetX(10);
			$i = 0;
			foreach ($row as $col) {
				if ($i == 0) {
					$this->Cell(15, 5, $col, 1, 0, "C");
				} elseif ($i == 2) {
					$this->Cell(35, 5, "$" . number_format($col), 1, 0, "C");
					$total += $col;
				} else {
					$this->Cell(140, 5, $col, 1, 0, "L");
				}
				$i++;
			}
			$this->Ln();
		}
		$this->SetFont('Arial', 'B', 15);
		$this->Cell(190, 8, "TOTAL GATOS: $" . number_format($total), 1, 0, "R");
		$this->Ln();
		$this->Ln();




		$this->Ln();
		$this->SetFont('Arial', 'B', 15);
		$this->Cell(190, 5, "OTROS INGRESOS", 0, 0, "C");
		$this->Ln();
		$this->Ln();
		$i = 0;
		$this->SetFont('Arial', 'B', 10);

		foreach ($this->header_ingresos as $col) {
			if ($i == 0) {
				$this->Cell(15, 5, $col, 1, 0, "C");
			} elseif ($i == 2) {
				$this->Cell(35, 5, $col, 1, 0, "C");
			} else {
				$this->Cell(140, 5, $col, 1, 0, "C");
			}
			$i++;
		}
		$this->Ln();
		$total = 0;
		foreach ($ingresos as $row) {
			$this->SetX(10);
			$i = 0;
			foreach ($row as $col) {
				if ($i == 0) {
					$this->Cell(15, 5, $col, 1, 0, "C");
				} elseif ($i == 2) {
					$this->Cell(35, 5, "$" . number_format($col), 1, 0, "C");
					$total += $col;
				} else {
					$this->Cell(140, 5, $col, 1, 0, "L");
				}
				$i++;
			}
			$this->Ln();
		}
		$this->SetFont('Arial', 'B', 15);
		$this->Cell(190, 8, "TOTAL OTROS INGRESOS: $" . number_format($total), 1, 0, "R");
		$this->Ln();
		$this->Ln();
	}
}
