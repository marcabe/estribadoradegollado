<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Usuarios/login';
$route['valida_login'] = 'Usuarios/valida_login';
$route['home/(:any)/(:any)'] = 'Usuarios/index/$1/$2';


#Rutas para el controlador prospecto
$route['usuarios/lista'] = 'Usuarios/lista';
$route['usuarios/nuevo'] = 'Usuarios/nuevo';
$route['usuarios/insert'] = 'Usuarios/insert';
$route['usuarios/editar/(:any)'] = 'Usuarios/editar/$1';


#Rutas para el controlador prospecto
$route['vendedores/lista'] = 'Vendedores';
$route['vendedores/nuevo'] = 'Vendedores/nuevo';
$route['vendedores/insert'] = 'Vendedores/insert';
$route['vendedores/editar/(:any)'] = 'Vendedores/editar/$1';
$route['vendedores/comisiones'] = 'Vendedores/vendedores_comisiones';

#Rutas para el controlador prospecto
$route['clientes/lista'] = 'Clientes/index';
$route['clientes/nuevo'] = 'Clientes/nuevo';
$route['clientes/insert'] = 'Clientes/insert';
$route['clientes/editar/(:any)'] = 'Clientes/editar/$1';
$route['clientes/compras/(:any)'] = 'Clientes/compras/$1';


#Ruta proveedoresx|
$route['proveedores/lista'] = 'Proveedores/index';
$route['proveedores/nuevo'] = 'Proveedores/nuevo';
$route['proveedores/insert'] = 'Proveedores/insert';
$route['proveedores/editar/(:any)'] = 'Proveedores/editar/$1';
$route['betitiux'] = 'Presentaciones/get_by_id/175';


#Ruta productos
$route['productos/lista'] = 'Productos/index';
$route['productos/nuevo'] = 'Productos/nuevo';
$route['productos/insert'] = 'Productos/insert';
$route['productos/update/(:any)'] = 'Productos/update/$1';
$route['productos/editar/(:any)'] = 'Productos/editar/$1';
$route['productos/ventas_productos_mes/(:any)/(:any)'] = 'Productos/ventas_productos_mes/$1/$2';

#Ruta de entradas
$route['entradas/lista'] = 'Entradas/index';
$route['entradas/nueva_entrada'] = 'Entradas/nueva';


#uta de ventas
$route['ventas/nueva_venta'] = 'Ventas/nueva';
$route['ventas/lista_ventas'] = 'Ventas/index';
$route['ventas/lista_ventas/filtro/(:any)/(:any)'] = 'Ventas/filtro/$1/$2';


#Ingresos
$route['ingresos/lista_ingresos'] = 'Ingresos/index';



#Rura para gastos
$route['gastos/lista'] = 'Gastos/index';

#APP
$route['app/calculadora_efectivo'] = 'App/calculadora_efectivo';





$route['404_override'] = '';
$route['translate_uri_dashes'] = false;
