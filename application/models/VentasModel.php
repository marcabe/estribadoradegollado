<?php

class VentasModel extends CI_Model
{
    public $tabla;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->tabla = "ventas";
    }

    public function get()
    {
        $this->db->select('*');
        $this->db->from($this->tabla);
        $consulta = $this->db->get();
        return $consulta->result();
    }

    public function get_where($where)
    {
        $this->db->select('*');
        $this->db->from($this->tabla);
        $this->db->where($where);
        $this->db->order_by("fecha", "DESC");

        $consulta = $this->db->get();
        return $consulta->result();
    }

    public function update($id, $dataUpdate)
    {
        $this->db->set($dataUpdate);
        $this->db->where('idventa', $id);
        $this->db->update($this->tabla);
        return 1;
    }

    public function last_id()
    {
        $this->db->select('*');
        $this->db->from($this->tabla);
        $this->db->order_by("idventa", "DESC");
        $this->db->limit(1);
        $consulta = $this->db->get();
        return $consulta->result();
    }

    public function get_by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->tabla);
        $this->db->where("idventa", $id);
        $consulta = $this->db->get();
        return $consulta->result();
    }

    public function insert($data)
    {
        if ($this->db->insert($this->tabla, $data)) {
            return $this->db->insert_id();
        } else {
            return null;
        }
    }
    public function delete_where($data)
    {
        $this->db->delete($this->tabla, $data);  // Produces: // DELETE FROM mytable  // WHERE id = $id
        return true;
    }

    public function select_get_where($select, $where)
    {
        $this->db->select($select);
        $this->db->from($this->tabla);
        $this->db->join("clientes", $this->tabla . ".cliente = clientes.idcliente");
        $this->db->join("usuarios", $this->tabla . ".usuario = usuarios.usuario");
        $this->db->where($where);

        $consulta = $this->db->get();
        return $consulta->result();
    }


    public function debe_mas($where)
    {
        $this->db->select("SUM(total) as debe, cliente");
        $this->db->from($this->tabla);
        $this->db->where($where);
        $this->db->group_by("cliente");
        $this->db->order_by("debe", "DESC");

        $consulta = $this->db->get();
        return $consulta->result();
    }
    public function select_get_where_cierre($select, $where)
    {
        $this->db->select($select);
        $this->db->from("clientes");
        $this->db->join("ventas", $this->tabla . ".cliente = clientes.idcliente");
        $this->db->where($where);

        $consulta = $this->db->get();
        return $consulta->result();
    }
}
