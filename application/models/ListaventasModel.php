<?php

class ListaventasModel extends CI_Model
{
    public $tabla;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->tabla = "listaventas";
    }

    public function get()
    {
        $this->db->select('*');
        $this->db->from($this->tabla);
        $consulta = $this->db->get();
        return $consulta->result();
    }

    public function get_where($where)
    {
        $this->db->select('*');
        $this->db->from($this->tabla);
        $this->db->where($where);

        $consulta = $this->db->get();
        return $consulta->result();
    }

    public function update($id, $dataUpdate)
    {
        $this->db->set($dataUpdate);
        $this->db->where('idlistaventas', $id);
        $this->db->update($this->tabla);
        return 1;
    }

    public function last_id()
    {
        return $this->db->insert_id();
    }

    public function get_by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->tabla);
        $this->db->where("idlistaventas", $id);
        $consulta = $this->db->get();
        return $consulta->result();
    }

    public function insert($data)
    {
        if ($this->db->insert($this->tabla, $data))
            return $this->db->insert_id();
        else
            return null;
    }
    public function delete_where($data)
    {
        $this->db->delete($this->tabla, $data);  // Produces: // DELETE FROM mytable  // WHERE id = $id
        return true;
    }



    public function producto_masvendido($orden, $where)
    {
        $this->db->select("presentacion, COUNT(presentacion) AS tpresentacion");
        $this->db->from($this->tabla);
        $this->db->join("ventas", "listaventas.idventa = ventas.idventa");
        $this->db->where($where);
        $this->db->group_by("presentacion");
        $this->db->order_by("tpresentacion", $orden);
        $consulta = $this->db->get();
        return $consulta->result();
    }
}
