<?php

class ProductosModel extends CI_Model
{
    public $tabla;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->tabla = "productos";
    }

    public function get()
    {
        $this->db->select('*');
        $this->db->from($this->tabla);
        $consulta = $this->db->get();
        return $consulta->result();
    }

    public function get_where($where)
    {
        $this->db->select('*');
        $this->db->from($this->tabla);
        $this->db->where($where);
        $consulta = $this->db->get();
        return $consulta->result();
    }

    public function update($id, $dataUpdate)
    {
        $this->db->set($dataUpdate);
        $this->db->where('idproducto', $id);
        $this->db->update($this->tabla);
        return 1;
    }

    public function get_by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->tabla);
        $this->db->where("idproducto", $id);
        $consulta = $this->db->get();
        return $consulta->result();
    }

    public function get_like($like)
    {
        $this->db->select('productos.idproducto as id, productos.producto, productos.stock, medidas.medida');
        $this->db->from($this->tabla);
        $this->db->join("medidas", $this->tabla . ".medida = medidas.idmedida");

        $this->db->where("status", 1);

        $this->db->like('producto', $like, 'both');

        $consulta = $this->db->get();
        $prod = $consulta->result_array();

        // Initialize Array with fetched data
        $data = array();
        foreach ($prod as $user) {
            $data[] = array("id" => $user['id'], "text" => $user['producto'] . "(" . $user['stock'] . "  " . $user['medida'] .  ")");
        }
        return $data;
    }

    public function insert($data)
    {
        if ($this->db->insert($this->tabla, $data))
            return $this->db->insert_id();
        else
            return null;
    }
    public function delete_where($data)
    {
        $this->db->delete($this->tabla, $data);  // Produces: // DELETE FROM mytable  // WHERE id = $id
        return true;
    }

    public function select_get_where($select, $where)
    {
        $this->db->select("producto, stock, medidas.medida");
        $this->db->from($this->tabla);
        $this->db->join("medidas", $this->tabla . ".medida = medidas.idmedida");
        $this->db->where($where);

        $consulta = $this->db->get();
        return $consulta->result();
    }


    public function ventas_productos_mes($where)
    {
        $this->db->select("productos.producto,presentaciones.presentacion,listaventas.cantidad,ventas.idventa,ventas.fecha,ventas.`status`,listaventas.subtotal, SUM(listaventas.cantidad) AS canti");
        $this->db->from($this->tabla);
        $this->db->join("presentaciones", "presentaciones.idproducto = productos.idproducto");
        $this->db->join("listaventas", "listaventas.presentacion = presentaciones.idpresentacion");
        $this->db->join("ventas", "listaventas.idventa = ventas.idventa");
        $this->db->where($where);
        $this->db->group_by("listaventas.presentacion");
        $this->db->order_by("idventa");

        $consulta = $this->db->get();
        return $consulta->result();
    }
}
