<?php require 'layaout/head.php'; ?>
<!--  menu -->
<?php require 'layaout/menu.php'; ?>
<!-- /menu -->

<!-- top navigation -->
<?php require 'layaout/cabecera.php'; ?>
<!-- /top navigation -->

<!-- page content -->
<style>

</style>
<div class="right_col" role="main">
	<div class="">

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12  ">
				<div class="x_panel">
					<div class="x_title">
						<h2><i class="fas fa-users fa-2x"></i></i> Modulo Clientes / <label>Lista cliente</label></h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content tituloSistema">
						<div class="row">
							<div class="col-sm-12 col-md-2">
								<div class="tile-stats text-center">
									<div class="count text-left">
										<i class="fas fa-credit-card"></i> <label id="perdidos">1/15</label>
									</div>
									<h6 class="mt-1"><?php echo $sum_15?>
									</h6>
									<p class="text-center">
										<button class="btn btn-primary btn-sm btn_clientes_pagos" data-plazo=15><i
												class="fas fa-info-circle"></i>
											Filtrar<sup><?php echo $t_15;?></sup></button>
									</p>
								</div>
							</div>

							<div class="col-sm-12 col-md-2">
								<div class="tile-stats text-center">
									<div class="count text-left">
										<i class="fas fa-credit-card"></i> <label id="perdidos">1/30</label>
									</div>
									<h6 class="mt-1"><?php echo $sum_30;?>
									</h6>
									<p class="text-center">
										<button class="btn btn-primary btn-sm btn_clientes_pagos" data-plazo=30><i
												class="fas fa-info-circle"></i>
											Filtrar<sup><?php echo $t_30;?></sup></button>
									</p>
								</div>
							</div>

							<div class="col-sm-12 col-md-4">
								<div class="tile-stats text-center">
									<div class="count text-left">
										<i class="fas fa-credit-card"></i> <label id="perdidos">Total Crédito</label>
									</div>
									<h6 class="mt-1"><?php echo $sum_t;?>
									</h6>
									<p class="text-center">
										<button class="btn btn-primary btn-sm btn_clientes_pagos" data-plazo=0><i
												class="fas fa-info-circle"></i>
											Filtrar<sup><?php echo $sum_t15_t30;?></sup></button>
									</p>
								</div>
							</div>


							<div class="col-md-4 text-right">
								<a href="../Clientes/reporte" target="_blank">
									<button class="btn btn-success btn-sm"><i class="fas fa-print"></i> Imprimir
										reporte</button>
								</a>
								<div class="alert alert-success" role="alert" id="msj_hecho" style="display: none;">
								</div>
							</div>
						</div>
						Lista de clientes
						<div class="row mt-5">
							<div class="col-md-12">
								<table id="tblgeneral" class="table table-bordered">
									<thead>
										<tr>
											<td><i class="far fa-id-card"></i> NOMBRE</td>
											<td><i class="fa fa-phone"></i> TELÉFONO</td>
											<td><i class="far fa-envelope"></i> RAZON SOCIAL</td>
											<td><i class="far fa-calendar-alt"></i> CORREO</td>
											<td><i class="far fa-calendar-alt"></i> DIRECCIÓN</td>
											<td></td>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($clientes as $c) { ?>
										<tr>
											<td><?php echo $c->nombre; ?>
											</td>
											<td><?php echo $c->telefono; ?>
											</td>
											<td><?php echo $c->razonsocial; ?>
											</td>
											<td><?php echo $c->correo; ?>
											</td>
											<td><?php echo $c->direccion; ?>
											</td>
											<td><?php echo $c->btn; ?>
											</td>
										</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<script src="<?php echo base_url(); ?>assets/develop/js/lista_clientes.js"
	type="module"></script>

<?php require 'layaout/footer.php'; ?>
<script>
	$(document).ready(function() {

	});
</script>