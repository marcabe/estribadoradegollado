<?php require 'layaout/head.php'; ?>
<!--  menu -->
<?php require 'layaout/menu.php'; ?>
<!-- /menu -->

<!-- top navigation -->
<?php require 'layaout/cabecera.php'; ?>
<!-- /top navigation -->

<!-- page content -->
<style>

</style>
<div class="right_col" role="main">
	<div class="">

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12  ">
				<div class="x_panel">
					<div class="x_title">
						<h2><i class="fas fa-boxes fa-2x"></i> Modulo Productos / <label>Lista productos</label></h2>
						</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content tituloSistema">
						<div class="row">
							<div class="col-12 text-right">
								<div class="row">
									<div class="col-md-12 text-left">
										<label>Rerpote de productos vendidos por mes</label>
									</div>
									<div class="col-md-2 text-left">
										<div class="input-group input-group-sm">
											<div class="input-group-prepend">
												<div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
											</div>
											<select class="form-control form-control-sm" id="mes">
												<option value="01">Enero</option>
												<option value="02">Febrero</option>
												<option value="03">Marzo</option>
												<option value="04">Abril</option>
												<option value="05">Mayo</option>
												<option value="06">Junio</option>
												<option value="07">Julio</option>
												<option value="08">Agosto</option>
												<option value="09">Septiembre</option>
												<option value="10">Octubre</option>
												<option value="11">Noviembre</option>
												<option value="12">Diciembre</option>
											</select>
										</div>
									</div>
									<div class="col-md-2 text-left">
										<div class="input-group input-group-sm">
											<div class="input-group-prepend">
												<div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
											</div>
											<select class="form-control form-control-sm" id="anio">
												<option value="2020">2020</option>
												<option value="2021">2021</option>
												<option value="2022">2022</option>
												<option value="2023">2023</option>
											</select>
										</div>
									</div>
									<div class="col-md-5 text-left">
										<button class="btn btn-success btn-sm" id="ventaspmes"><i class="fas fa-print"></i> Imprimir</button>
									</div>
									<div class="col-md-3">
										<a href="../Productos/reporte" target="_blank">
											<button class="btn btn-success btn-sm"><i class="fas fa-print"></i> Imprimir reporte</button>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="row mt-5">
							<div class="col-md-6" id="mensaje" style="display: none;">
								<div class="alert alert-success alert-dismissible " role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
									</button>
									Presentacion agregada correctamente
								</div>
							</div>
							<div class="col-md-12">
								<table id="tblgeneral" class="table table-bordered">
									<thead>
										<tr>
											<td><i class="far fa-id-card"></i> Producto</td>
											<td><i class="fas fa-balance-scale"></i> Medida</td>
											<td><i class="fas fa-boxes"></i> Stock</td>
											<td></td>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($productos as $p) { ?>
											<tr>
												<td><?php echo $p->producto; ?></td>
												<td><?php echo $p->medidas->medida; ?></td>
												<td><?php echo $p->stock; ?></td>
												<td class="text-center">
													<div class="dropdown">
														<button class="btn btn-success btn-sm dropdown-toggle configprospectos" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
															<i class="fas fa-cogs"></i> Config
														</button>
														<div class="dropdown-menu listaconfig" aria-labelledby="dropdownMenuButton">
															<span class="dropdown-item detalle" id="<?php echo $p->idproducto; ?>">
																<i class="fas fa-eye"></i> Ver detalle
															</span>
															<span class="dropdown-item editar" id="<?php echo $p->idproducto; ?>">
																<i class="fas fa-edit"></i> Editar
															</span>
															<span class="dropdown-item presentacion" id="<?php echo $p->idproducto; ?>">
																<i class="fas fa-archive"></i> Agregar presentación
															</span>
															<span class="dropdown-item eliminar" id="<?php echo $p->idproducto; ?>">
																<i class="fa fa-trash"></i> Eliminar
															</span>
														</div>
													</div>
												</td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<script src="<?php echo base_url(); ?>assets/develop/js/lista_productos.js" type="module"></script>

<?php require 'layaout/footer.php'; ?>
<script>
	$(document).ready(function() {
		const mostrarmensajes = () => {
			$("#bloquear").show();
			$("#msjsys").show();
			$("#lblmsntop").show();
			return;
		}

		const funcionesxhr = (conectividad, mensaje) => {
			if (conectividad == 0) {
				$("#lblmsntop").text("No hay acceso a internet");
			} else {
				$("#lblmsntop").text(mensaje);
			}
			return;
		}
		const errorcon = (t, conectividad = 1) => {
			$("#lblmsntop").text("Error en la peticion al servidor");
			if (t === "timeout") {
				console.log("Error en la conexion a internet");
			} else {
				conectividad = 0;
			}
			return conectividad;
		}
		$("#ventaspmes").on("click", function() {
			let mes = $("#mes").val();
			let anio = $("#anio").val();
			window.open("../productos/ventas_productos_mes/" + mes + "/" + anio, "_blank");
		});


		$(".presentacion").on("click", function() {
			let conectividad =1;
			let producto = $(this).attr("id");
			let contenido = `
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12 text-center">
						Nueva presentación
					</div>
				</div>
				<div class="form-group row text-right">
					<label for="inputPassword" class="col-md-3 col-form-label">Presentación: </label>
					<div class="col-md-9">
						<div class="input-group input-group-sm">
							<div class="input-group-prepend">
								<div class="input-group-text"><i class="fas fa-box"></i></div>
							</div>
							<input type="text" class="form-control form-control-sm" id="presentacion">
						</div>
						<div class="text-left">
							<small id="msj_presentacion" class="msj_presentacion"></small>
						</div>
					</div>
				</div>
				<div class="form-group row text-right">
					<label for="inputPassword" class="col-md-3 col-form-label">Cant. min.: </label>
					<div class="col-md-9">
						<div class="input-group input-group-sm">
							<div class="input-group-prepend">
								<div class="input-group-text"><i class="fas fa-boxes"></i></div>
							</div>
							<input type="number" class="form-control form-control-sm" id="cantidadminima">
						</div>
						<div class="text-left">
							<small id="msj_cantidadminima" class="msj_cantidadminima"></small>
						</div>
					</div>
				</div>

				<div class="form-group row text-right">
					<label for="inputPassword" class="col-md-3 col-form-label">Menudeo: </label>
					<div class="col-md-9">
						<div class="input-group input-group-sm">
							<div class="input-group-prepend">
								<div class="input-group-text"><i class="fas fa-dollar-sign"></i></div>
							</div>
							<input type="text" class="form-control form-control-sm" id="menudeo">
						</div>
						<div class="text-left">
							<small id="msj_menudeo" class="msj_menudeo"></small>
						</div>
					</div>
				</div>

				<div class="form-group row text-right">
					<label for="inputPassword" class="col-md-3 col-form-label">Mayoreo: </label>
					<div class="col-md-9">
						<div class="input-group input-group-sm">
							<div class="input-group-prepend">
								<div class="input-group-text"><i class="fas fa-dollar-sign"></i></div>
							</div>
							<input type="text" class="form-control form-control-sm" id="mayoreo">
						</div>
						<div class="text-left">
							<small id="msj_mayoreo" class="msj_mayoreo"></small>
						</div>
					</div>
				</div>
			</div>`;
			$.confirm({
				title: '<i class="fas fa-robot"></i> Mensaje del sistema',
				content: contenido,
				draggable: true,
				buttons: {
					aceptar: {
						text: '<i class="far fa-thumbs-up"></i> Aceptar', // text for button
						btnClass: 'btn-success success-modal',
						action: function() {
							let avanza = 1;
							let presentacion = $("#presentacion").val();
							let cantidadminima = $("#cantidadminima").val();
							let mayoreo = $("#mayoreo").val();
							let menudeo = $("#menudeo").val();

							if (presentacion == '') {
								$("#msj_presentacion").html("Debe ingresar el nombre del prospecto");
								$("#presentacion").css({
									"background": "#EFD3D2"
								});
								$("#msj_presentacion").show();
								avanza = 0;
							}

							if (cantidadminima == 0) {
								$("#msj_cantidadminima").html("Debe ingresar el nombre del prospecto");
								$("#cantidadminima").css({
									"background": "#EFD3D2"
								});
								$("#msj_cantidadminima").show();
								avanza = 0;
							}

							if (mayoreo == '') {
								$("#msj_mayoreo").html("Debe ingresar el nombre del prospecto");
								$("#mayoreo").css({
									"background": "#EFD3D2"
								});
								$("#msj_mayoreo").show();
								avanza = 0;
							}

							if (menudeo == '') {
								$("#msj_menudeo").html("Debe ingresar el nombre del prospecto");
								$("#menudeo").css({
									"background": "#EFD3D2"
								});
								$("#msj_menudeo").show();
								avanza = 0;
							}
							if (avanza == 1) {
								$.ajax({
									url: '../Presentaciones/insert',
									data: {
										presentacion: presentacion,
										cantidadminima: cantidadminima,
										menudeo: accounting.unformat(menudeo),
										mayoreo: accounting.unformat(mayoreo),
										status: 1,
										idproducto: producto
									},
									type: 'POST',
									success: function(response) {
										$("#mensaje").show();
										$("#mensaje").fadeOut(5000);
									},
									error: function(x, t, m) {
										mostrarmensajes();
										conectividad = errorcon(t, conectividad)
									},
									xhr: function() {
										mostrarmensajes();
										var xhr = $.ajaxSettings.xhr();
										xhr.upload.onprogress = function(e) {
											funcionesxhr(conectividad, "Cargando información");
										};
										xhr.onloadstart = function(e) {
											funcionesxhr(conectividad, "Iniciando proceso");
										};
										xhr.onloadend = function(e) {
											if (conectividad != 0) {
												$("#bloquear").hide();
												$("#msjsys").hide();
												$("#lblmsntop").hide();
											}

										};
										return xhr;
									}
								});
							} else {
								return 0;
							}
						}
					},
					cerrar: {
						text: '<i class="fas fa-times"></i> Cerrar', // text for button
						btnClass: 'btn-danger', // multiple classes.
						action: function() {}
					}
				}
			});
		});

	});
</script>