<?php require 'layaout/head.php'; ?>
<!--  menu -->
<?php require 'layaout/menu.php'; ?>
<!-- /menu -->

<!-- top navigation -->
<?php require 'layaout/cabecera.php'; ?>
<!-- /top navigation -->

<div class="right_col" role="main">
	<div class="">

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12  ">
				<div class="x_panel">
					<div class="x_title">
						<h2><i class="fas fa-truck-moving fa-2x"></i></i> Modulo Proveedores / <label>Editar proveedor</label></h2>
						<div class="clearfix"></div>
					</div>
					<div class="container mt-3">
						<div class="row">
							<div class="col-md-12 text-left">
								<button class="btn btn-success btn-sm text-center" id="editar"><i
											class="far fa-save"></i> Editar
								</button>
							</div>
						</div>
						<input type="text" id="idproveedor" value="<?php echo $proveedor->idproveedor;?>" hidden>
						<div class="row mt-4">
							<div class="col-md-4">
								<div class="form-group">
									<label>Nombre(s) del proveedor: </label>
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="far fa-user"></i></div>
										</div>
										<input type="text" class="form-control form-control-sm" id="nombre" value="<?php echo $proveedor->nombre;?>">
									</div>
									<div class="text-left">
										<small id="msj_nombre" class="msj_formulario"></small>
									</div>
								</div>
							</div>

							<div class="col-md-2">
								<div class="form-group">
									<label>Teléfono / Celular: </label>
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="fas fa-phone-alt"></i></div>
										</div>
										<input type="text" class="form-control form-control-sm" id="telefono"
											   data-inputmask="'mask' : '(999) 999-99-99'" value="<?php echo $proveedor->telefono;?>"></div>
									<div class="text-left">
										<small id="msj_telefono" class="msj_formulario"></small>
									</div>
								</div>
							</div>

							<div class="col-md-3">
								<div class="form-group">
									<label>Razon Social: </label>
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="fas fa-building"></i></div>
										</div>
										<input type="text" class="form-control form-control-sm" id="razonsocial" value="<?php echo $proveedor->razonsocial;?>">
									</div>
									<div class="text-left">
										<small id="msj_razonsocial" class="msj_formulario"></small>
									</div>
								</div>
							</div>

							<div class="col-md-3">
								<div class="form-group">
									<label>RFC: </label>
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="far fa-id-card"></i></div>
										</div>
										<input type="text" class="form-control form-control-sm" id="rfc" value="<?php echo $proveedor->rfc;?>">
									</div>
									<div class="text-left">
										<small id="msj_rfc" class="msj_formulario"></small>
									</div>
								</div>
							</div>

							<div class="col-md-3">
								<div class="form-group">
									<label>Correo: </label>
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="far fa-envelope"></i></div>
										</div>
										<input type="text" class="form-control form-control-sm" id="correo" value="<?php echo $proveedor->correo;?>">
									</div>
									<div class="text-left">
										<small id="msj_correo" class="msj_formulario"></small>
									</div>
								</div>
							</div>


							<div class="col-md-3">
								<div class="form-group">
									<label>Dirección: </label>
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="fas fa-house-user"></i></div>
										</div>
										<input type="text" class="form-control form-control-sm" id="direccion" value="<?php echo $proveedor->direccion;?>">
									</div>
									<div class="text-left">
										<small id="msj_direccion" class="msj_formulario"></small>
									</div>
								</div>
							</div>

							<div class="col-md-3">
								<div class="form-group">
									<label>CP: </label>
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="fas fa-house-user"></i></div>
										</div>
										<input type="text" class="form-control form-control-sm" id="cp" value="<?php echo $proveedor->cp;?>">
									</div>
									<div class="text-left">
										<small id="msj_cp" class="msj_cp"></small>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<script src="<?php echo base_url(); ?>assets/develop/js/editar_proveedor.js"></script>

<?php require 'layaout/footer.php'; ?>
