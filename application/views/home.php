<?php require 'layaout/head.php'; ?>
<!--  menu -->
<?php require 'layaout/menu.php'; ?>
<!-- /menu -->

<!-- top navigation -->
<?php require 'layaout/cabecera.php'; ?>
<!-- /top navigation -->

<!-- page content -->
<style>

</style>
<div class="right_col" role="main">
	<div class="">

		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-8">
				<div class="page-title">
					<div class="title_left">
						<h3>Dashboard Principal</h3>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="col-md-4 text-left">
				<div class="input-group input-group-sm">
					<div class="input-group-prepend">
						<div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
					</div>
					<select class="form-control form-control-sm" id="mes">
						<option value="01">Enero</option>
						<option value="02">Febrero</option>
						<option value="03">Marzo</option>
						<option value="04">Abril</option>
						<option value="05">Mayo</option>
						<option value="06">Junio</option>
						<option value="07">Julio</option>
						<option value="08">Agosto</option>
						<option value="09">Septiembre</option>
						<option value="10">Octubre</option>
						<option value="11">Noviembre</option>
						<option value="12">Diciembre</option>
					</select>
				</div>
			</div>
			<div class="col-md-4 text-left">
				<div class="input-group input-group-sm">
					<div class="input-group-prepend">
						<div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
					</div>
					<select class="form-control form-control-sm" id="anio">
						<option value="2020">2020</option>
						<option value="2021">2021</option>
						<option value="2022">2022</option>
						<option value="2023">2023</option>
					</select>
				</div>
			</div>
			<input id="mesel" value="<?php echo $mes; ?>" hidden>
			<input id="aniosel" value="<?php echo $anio; ?>" hidden>

			<div class="col-md-4 text-left">
				<button class="btn btn-success btn-sm" id="consultar">Consultar</button>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-sm-12">
				<div class="tile-stats text-center">
					<div class="count text-left">
						<i class="far fa-money-bill-alt"></i> <label id="totalprospectos"><?php echo "$ ".$totalventas;?></label>
					</div>
					<h6 class="mt-1">Ventas del mes</h6>

				</div>
			</div>

			<div class="col-md-4 col-sm-12">
				<div class="tile-stats text-center">
					<div class="count text-left">
						<i class="far fa-money-bill-alt"></i> <label id="totalprospectos"><?php echo "$ ".$totalentradas;?></label>
					</div>
					<h6 class="mt-1">Entradas del mes</h6>
				</div>
			</div>
			<div class="col-sm-12 col-md-2">
				<div class="tile-stats text-center">
					<div class="count text-left">
						<i class="fas fa-credit-card"></i> <label id="perdidos"><?php echo $ventascredito;?></label>
					</div>
					<h6 class="mt-1">Ventas totales del mes</h6>
					<p class="text-center">
						<a href="../../ventas/lista_ventas/filtro/<?php echo $anio;?>/<?php echo $mes;?>">
							<button class="btn btn-primary btn-sm" ><i class="fas fa-info-circle"></i> Ver detalle</button>
						</a>
					</p>
				</div>
			</div>
			<div class="col-sm-12 col-md-2">
				<div class="tile-stats text-center">
					<div class="count text-left">
						<i class="fas fa-credit-card"></i> <label id="perdidos"><?php echo $entradascredito;?></label>
					</div>
					<h6 class="mt-1">Entradas totales</h6>
					<p class="text-center">
						<!--<a href="entradas/lista">
							<button class="btn btn-primary btn-sm" ><i class="fas fa-info-circle"></i> Ver detalle
							</button>
						</a>-->
					</p>
				</div>
			</div>
			
		</div>

		<div class="row">
			<div class="col-md-4 col-sm-12">
				<div class="x_panel">
					<div class="x_title">
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="row">
							<div class="col-md-12">
								<div id="ventasentradas"></div>
							</div>
						</div>
						<div class="divider"></div>
					</div>
				</div>
			</div>

			<div class="col-md-6 col-sm-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Productos vendidos del mes</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="row">
							<div class="col-md-12">
								<div id="gdias"></div>
							</div>
						</div>
						<div class="divider"></div>
					</div>
				</div>
			</div>

			<div class="col-sm-12 col-md-2">
				<div class="row">
					<div class="col-md-12">
						<div class="tile-stats text-center">
							<div class="count text-left">
								<i class="fas fa-boxes"></i> <label id="perdidos"><?php echo $productosminimo;?></label>
							</div>
							<h6 class="mt-1">Productos por terminar</h6>
							<p class="text-center">
								<button class="btn btn-primary btn-sm" id="6"><i class="fas fa-info-circle"></i> Ver detalle
								</button>
							</p>
						</div>
					</div>
					<div class="col-md-12">
						<div class="tile-stats text-center">
							<div class="count text-left">
								<i class="fas fa-boxes"></i> <label id="perdidos"><?php echo ($si==0)?"0":$masvendido->tpresentacion;?></label>
							</div>
							<h6 class="mt-1">Producto mas vendido: <?php echo ($si==0)?"0":$masvendido->nproducto;?></h6>
						</div>
					</div>
					<div class="col-md-12">
						<div class="tile-stats text-center">
							<div class="count text-left">
								<i class="fas fa-boxes" style="color:red!important;"></i> <label id="perdidos"><?php echo ($yes==0)?"0":$menosvendido->tpresentacion;?></label>
							</div>
							<h6 class="mt-1">Producto menos vendido: <?php echo ($yes==0)?"0":$menosvendido->nproducto;?></h6>
						</div>
					</div>
				</div>
				
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-sm-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Productos con stock bajo</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="row">
							<div class="col-md-12">
								<table class="table table-bordered" id="stock">
									<thead>
										<tr>
											<td>PRODUCTO</td>
											<td>STOCK</td>
											<td>MINIMO</td>
										</tr>
									</thead>
									<tbody>
										<?php foreach($stockproductos as $p){ ?>
										<tr>
											<td><?php echo $p->producto ?></td>
											<td><?php echo $p->stock ?></td>
											<td><?php echo $p->minimo ?></td>
										</tr>
										<?php } ?>
									</tbody>
								</table>							
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-sm-12">
				<div class="x_panel">
					<div class="x_title">
						<div class="row">
							<h2 class="col-md-6 col-sm-12">Clientes con Mayor deuda</h2>
							<h4 class="text-right col-md-6 col-sm-12">$ <?php echo number_format($totaldeuda, 2, '.', ',');?></h4>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
					
						<div class="row">
							<div class="col-md-12">
								<table class="table table-bordered" id="clientes">
									<thead>
										<tr>
											<td>CLIENTE</td>
											<td>DEUDA</td>
										</tr>
									</thead>
									<tbody>
										<?php foreach($deudor as $p){ ?>
										<tr class="cliente" style="cursor:pointer;" id="<?php echo $p->cliente;?>">
											<td><?php echo $p->ncliente ?></td>
											<td><?php echo "$ ".number_format($p->debe, 2, '.', ','); ?></td>
										</tr>
										<?php } ?>
									</tbody>
								</table>							
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->

<?php require 'layaout/footer.php'; ?>
<script>
	$(document).ready(function () {

		$("#consultar").on("click", function(){
			window.location='../'+$("#mes").val()+"/"+$("#anio").val();
		});


		$("#mes").val(<?php echo $mes;?>);
		$("#anio").val(<?php echo $anio;?>);


		$(".cliente").on("click", function(){
			let idcliente = $(this).attr("id");
			$.ajax({
				url: "../../Ventas/ventas_deudas/",
				data: {
					'MONTH(fecha)':$("#mesel").val(),
					'YEAR(fecha)':$("#aniosel").val(),
					cliente:idcliente
				},
				dataType: 'json',
				type: "POST",
				success: (response) => {
					console.log(response);
					let cuerpo = '';
					$.each(response, function (i, d) {
						console.log("Entramos");
						let total = parseFloat(d.total);
						cuerpo +=`<tr><td>${d.idventa}</td><td>$ ${total}</td> </tr>`;
					});
					let contenido = `
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-6 offset-md-3 col-sm-12 text-center">
								<table class="table table-bordered">
									<thead>
									<tr>
										<td>Folio de Venta</td>
										<td>Total</td>
									</tr>
									</thead>
									<tbody id='cuerpotabla'>
									${cuerpo}
									</tbody>
								</table>
							</div>
						</div>
					</div>`;
					
					let credi = $.confirm({
						title: 'Credito en ventas',
						content: contenido,
						type: 'green',
						columnClass: 'col-md-6 col-md-offset-2',
						containerFluid: true,
						typeAnimated: true,
						buttons: {
							Aceptar: {
								btnClass: 'btn-success',
								action: () => {
									credi.close();
								}
							}
						}
					});
				}
			});
			
		});

		$('#stock').DataTable({
		"language": {
			"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
		}
	});

		$('#clientes').DataTable({
		"ordering": false,
		"language": {
			"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
		}
	});
		$("#ventasentradas").dxPieChart({
			palette: "bright",
			dataSource: <?php echo $ventasentradas;?>,
			title: {
				text: "Ventas vs. Entradas",
				subtitle: ""
			},
			legend: {
				orientation: "horizontal",
				itemTextPosition: "right",
				horizontalAlignment: "center",
				verticalAlignment: "bottom",
				columnCount: 4
			},
			onPointClick: function (e) {
				var point = e.target;
				toggleVisibility(point);
			},
			onLegendClick: function (e) {
				var arg = e.target;
				toggleVisibility(this.getAllSeries()[0].getPointsByArg(arg)[0]);
			},
			"export": {
				enabled: true
			},
			series: [{
				argumentField: "titulo",
				valueField: "tot",
				label: {
					visible: true,
					font: {
						size: 16
					},
					connector: {
						visible: true,
						width: 0.5
					},
					position: "columns",
					customizeText: function (arg) {
						return arg.valueText + " (" + arg.percentText + ")";
					}
				}
			}]
		});


		$("#gdias").dxChart({
			dataSource: <?php echo $gdias;?>,
			palette: "soft",
			title: {
				text: "Ventas por dia",
				subtitle: ""
			},
			commonSeriesSettings: {
				type: "bar",
				valueField: "monto",
				argumentField: "dia",
				ignoreEmptyPoints: true,
				label: {
					visible: true,
					format: {
						type: "fixedPoint",
						precision: 0
					}
				}
			},
			seriesTemplate: {
				nameField: "dia"
			},
			tooltip: {
				enabled: true,
				location: "edge",
				customizeTooltip: function (arg) {
					return {
						text: arg.seriesName
					};
				}
			},
			legend: {
				visible: false
			}
		});

	});
</script>
