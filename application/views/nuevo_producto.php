<?php require 'layaout/head.php'; ?>
<!--  menu -->
<?php require 'layaout/menu.php'; ?>
<!-- /menu -->

<!-- top navigation -->
<?php require 'layaout/cabecera.php'; ?>
<!-- /top navigation -->

<div class="right_col" role="main">
	<div class="">

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12  ">
				<div class="x_panel">
					<div class="x_title">
						<h2><i class="fas fa-boxes fa-2x"></i> Modulo Productos / <label>Nuevo producto</label></h2>
						<div class="clearfix"></div>
					</div>
					<div class="container mt-3">
						<div class="row">
							<div class="col-md-12 text-left">
								<button class="btn btn-success btn-sm text-center" id="guardar"><i
											class="far fa-save"></i> Guardar
								</button>
							</div>
						</div>
						<div class="col-md-6 offset-md-2 mt-4">
							<div class="form-group row text-right">
								<label for="inputPassword" class="col-md-3 col-form-label">Producto: </label>
								<div class="col-md-9">
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="far fa-user"></i></div>
										</div>
										<input type="text" class="form-control form-control-sm" id="producto"
											   placeholder="Ingresa el nombre del usuario">
									</div>
									<div class="text-left">
										<small id="msj_producto" class="msj_producto"></small>
									</div>
								</div>
							</div>


							<div class="form-group row text-right">
								<label for="inputPassword" class="col-md-3 col-form-label">Medida: </label>
								<div class="col-md-9">
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="fas fa-balance-scale"></i></div>
										</div>
										<select class="form-control form-control-sm" id="medida">
											<option value=0>Seleccione una medida</option>
											<?php foreach ($medidas as $m) { ?>
												<option value="<?php echo $m->idmedida; ?>"><?php echo $m->medida; ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="text-left">
										<small id="msj_medida" class="msj"></small>
									</div>
								</div>
							</div>


							<div class="form-group row text-right">
								<label for="inputPassword" class="col-md-3 col-form-label">Stock: </label>
								<div class="col-md-9">
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="fas fa-balance-scale"></i></div>
										</div>
										<input type="number" class="form-control form-control-sm" id="stock">
									</div>
									<div class="text-left">
										<small id="msj_stock" class="msj"></small>
									</div>
								</div>
							</div>

							<div class="form-group row text-right">
								<label for="inputPassword" class="col-md-3 col-form-label">Minimo: </label>
								<div class="col-md-9">
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="fas fa-balance-scale"></i></div>
										</div>
										<input type="number" class="form-control form-control-sm" id="minimo">
									</div>
									<div class="text-left">
										<small id="msj_minimo" class="msj"></small>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<script src="<?php echo base_url(); ?>assets/develop/js/nuevo_producto.js" type="module"></script>

<?php require 'layaout/footer.php'; ?>
