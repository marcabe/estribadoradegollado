<?php require 'layaout/head.php'; ?>
<!--  menu -->
<?php require 'layaout/menu.php'; ?>
<!-- /menu -->

<!-- top navigation -->
<?php require 'layaout/cabecera.php'; ?>
<!-- /top navigation -->

<div class="right_col" role="main">
	<div class="">

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12  ">
				<div class="x_panel">
					<div class="x_title">
						<h2><i class="fas fa-users fa-2x"></i></i> Modulo Clientes / <label>Nuevo cliente</label></h2>
						<div class="clearfix"></div>
					</div>
					<div class="container mt-3">
						<div class="row">
							<div class="col-md-12 text-left">
								<button class="btn btn-success btn-sm text-center" id="guardar"><i
										class="far fa-save"></i> Guardar
								</button>
							</div>
						</div>
						<div class="row mt-4">
							<div class="col-md-4">
								<div class="form-group">
									<label>Nombre(s) del cliente: </label>
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="far fa-user"></i></div>
										</div>
										<input type="text" class="form-control form-control-sm" id="nombre">
									</div>
									<div class="text-left">
										<small id="msj_nombre" class="msj_formulario"></small>
									</div>
								</div>
							</div>

							<div class="col-md-2">
								<div class="form-group">
									<label>Teléfono / Celular: </label>
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="fas fa-phone-alt"></i></div>
										</div>
										<input type="text" class="form-control form-control-sm" id="telefono"
											data-inputmask="'mask' : '(999) 999-99-99'">
									</div>
									<div class="text-left">
										<small id="msj_telefono" class="msj_formulario"></small>
									</div>
								</div>
							</div>

							<div class="col-md-3">
								<div class="form-group">
									<label>Razon Social: </label>
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="fas fa-building"></i></div>
										</div>
										<input type="text" class="form-control form-control-sm" id="razonsocial">
									</div>
									<div class="text-left">
										<small id="msj_razonsocial" class="msj_formulario"></small>
									</div>
								</div>
							</div>

							<div class="col-md-3">
								<div class="form-group">
									<label>RFC: </label>
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="far fa-id-card"></i></div>
										</div>
										<input type="text" class="form-control form-control-sm" id="rfc">
									</div>
									<div class="text-left">
										<small id="msj_rfc" class="msj_formulario"></small>
									</div>
								</div>
							</div>

							<div class="col-md-3">
								<div class="form-group">
									<label>Correo: </label>
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="far fa-envelope"></i></div>
										</div>
										<input type="text" class="form-control form-control-sm" id="correo">
									</div>
									<div class="text-left">
										<small id="msj_correo" class="msj_formulario"></small>
									</div>
								</div>
							</div>


							<div class="col-md-3">
								<div class="form-group">
									<label>Dirección: </label>
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="fas fa-house-user"></i></div>
										</div>
										<input type="text" class="form-control form-control-sm" id="direccion">
									</div>
									<div class="text-left">
										<small id="msj_direccion" class="msj_formulario"></small>
									</div>
								</div>
							</div>

							<div class="col-md-2">
								<div class="form-group">
									<label>C.P: </label>
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="fas fa-house-user"></i></div>
										</div>
										<input type="text" class="form-control form-control-sm" id="cp">
									</div>
									<div class="text-left">
										<small id="msj_direccion" class="msj_formulario"></small>
									</div>
								</div>
							</div>
							<div style="display:none;" id="info_credito">
								<div class="col-md-2">
									<div class="form-group">
										<label>Credito: </label>
										<div class="input-group input-group-sm">
											<div class="input-group-prepend">
												<div class="input-group-text"><i class="fas fa-dollar-sign"></i></div>
											</div>
											<input type="text" class="form-control form-control-sm" id="credito"
												value="0">
										</div>
										<div class="text-left">
											<small id="msj_credito" class="msj_formulario"></small>
										</div>
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label>Días de credito: </label>
										<div class="input-group input-group-sm">
											<div class="input-group-prepend">
												<div class="input-group-text"><i class="fas fa-dollar-sign"></i></div>
											</div>
											<select class="form-control form-control-sm" id="diascredito">
												<option value=0>Seleccione los dias</option>
												<option value=15>15 días</option>
												<option value=30>30 días</option>
											</select>
										</div>
										<div class="text-left">
											<small id="msj_diascredito" class="msj_formulario"></small>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label>Credito: </label>
								</div>
								<div class="row">
									<label class="col-md-6">
										<input type="radio" name="opcion_credito" value="Si"> Si
									</label>
									<label class="col-md-6">
										<input type="radio" name="opcion_credito" value="No" checked> No
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<script src="<?php echo base_url(); ?>assets/develop/js/nuevo_cliente.js"
	type="module"></script>

<?php require 'layaout/footer.php';
