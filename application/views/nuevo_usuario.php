<?php require 'layaout/head.php'; ?>
<!--  menu -->
<?php require 'layaout/menu.php'; ?>
<!-- /menu -->

<!-- top navigation -->
<?php require 'layaout/cabecera.php'; ?>
<!-- /top navigation -->

<div class="right_col" role="main">
	<div class="">

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12  ">
				<div class="x_panel">
					<div class="x_title">
						<h2><i class="fas fa-boxes fa-2x"></i> Modulo Usuarios / <label>Nuevo usuario</label></h2>
						<div class="clearfix"></div>
					</div>
					<div class="container mt-3">
						<div class="row">
							<div class="col-md-12 text-left">
								<button class="btn btn-success btn-sm text-center" id="guardar"><i
										class="far fa-save"></i> Guardar
								</button>
							</div>
						</div>
						<div class="col-md-6 offset-md-2 mt-4">
							<div class="form-group row text-right">
								<label for="inputPassword" class="col-md-3 col-form-label">Nombre: </label>
								<div class="col-md-9">
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="far fa-user"></i></div>
										</div>
										<input type="text" class="form-control form-control-sm" id="nombre"
											placeholder="Ingresa el nombre del usuario">
									</div>
									<div class="text-left">
										<small id="msj_producto" class="msj_nombre"></small>
									</div>
								</div>
							</div>

							<div class="form-group row text-right">
								<label for="inputPassword" class="col-md-3 col-form-label">Usuario: </label>
								<div class="col-md-9">
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="far fa-user"></i></div>
										</div>
										<input type="text" class="form-control form-control-sm" id="usuario"
											placeholder="Ingresa el usuario">
									</div>
									<div class="text-left">
										<small id="msj_usuario" class="msj_usuario"></small>
									</div>
								</div>
							</div>


							<div class="form-group row text-right">
								<label for="inputPassword" class="col-md-3 col-form-label">Rol: </label>
								<div class="col-md-9">
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="fas fa-balance-scale"></i></div>
										</div>
										<select class="form-control form-control-sm" id="roles">
											<option value=0>Seleccione el rol</option>
											<option value=1>Administrador</option>
											<option value=2>Personal</option>
										</select>
									</div>
									<div class="text-left">
										<small id="msj_rol" class="msj"></small>
									</div>
								</div>
							</div>


							<div class="form-group row text-right">
								<label for="inputPassword" class="col-md-3 col-form-label">Contraseña: </label>
								<div class="col-md-9">
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="fas fa-balance-scale"></i></div>
										</div>
										<input type="password" class="form-control form-control-sm" id="clave">
									</div>
									<div class="text-left">
										<small id="msj_clave" class="msj"></small>
									</div>
								</div>
							</div>

							<div class="form-group row text-right">
								<label for="inputPassword" class="col-md-3 col-form-label">Confirmar contraseña:
								</label>
								<div class="col-md-9">
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="fas fa-balance-scale"></i></div>
										</div>
										<input type="password" class="form-control form-control-sm" id="clave_confirm">
									</div>
									<div class="text-left">
										<small id="msj_clave_confirm" class="msj"></small>
									</div>
								</div>
							</div>


						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<script src="<?php echo base_url(); ?>assets/develop/js/nuevo_usuario.js"
	type="module"></script>

<?php require 'layaout/footer.php';
