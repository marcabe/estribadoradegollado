<?php require 'layaout/head.php'; ?>
<!--  menu -->
<?php require 'layaout/menu.php'; ?>
<!-- /menu -->

<!-- top navigation -->
<?php require 'layaout/cabecera.php'; ?>
<!-- /top navigation -->

<div class="right_col" role="main">
	<div class="">

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12  ">
				<div class="x_panel">
					<div class="x_title">
						<h2><i class="fas fa-users fa-2x"></i></i> Modulo Clientes / <label>Historial de compras</label>
						</h2>
						<div class="clearfix"></div>
					</div>
					<div class="container mt-3">
						<div class="row">
							<div class="col-md-6">
								<p>
									<label><strong>Nombre: </strong><?php echo $cliente->nombre; ?></label>
								</p>
								<p>
									<label><strong>Telefono: </strong><?php echo $cliente->telefono; ?></label>
								</p>
								<p>
									<label><strong>Credito: </strong><?php echo "$" . number_format($cliente->credito); ?></label>
								</p>
								<p>
									<label><strong>Credito Usado: </strong><?php echo "$" . number_format($cliente->creditousado); ?></label>
								</p>
								<p>
									<label><strong>Disponible: </strong><?php echo "$" . number_format($cliente->credito - $cliente->creditousado); ?></label>
								</p>
							</div>
							<div class="col-md-6">
								<form action="../imprimir_notas" method="POST" target="_blank">
									<div class="form-group">
										<label>Notas</label>
										<input value=<?php echo $cliente->idcliente; ?> hidden name="cliente">
										<select class="form-control form-control-sm" name="tipo_nota">
											<option>Seleccione una opcion</option>
											<option value=0>Pagadas</option>
											<option value=1>No pagadas</option>
										</select>
									</div>
									<div class="form-group">
										<button class="btn btn-sm btn-success">Imprimir</button>
									</div>
								</form>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12" id="lista_notas">
								<div class="accordion" id="accordionExample">
									<div class="card">

										<?php foreach ($ventas as $v) { ?>
											<div class="card-header">
												<div class="row" style="background:<?php echo ($v->liquidado == 1) ? '#B4E391' : ''; ?>">
													<div class="col-md-6">
														<h5 class="mb-0">
															<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#ventas_<?php echo $v->idventa; ?>" aria-expanded="true" aria-controls="collapseOne">
																No. Ventas <?php echo $v->idventa; ?>
																(<?php echo $v->fecha; ?>)
																<label>Total: <?php echo "$" . number_format($v->total); ?></label>

															</button>
														</h5>
													</div>
													<div class="col-md-6 text-right">
														<?php echo $v->btn_al; ?>
														<a target="_blank" href="../../ventas/impresion_venta/<?php echo $v->idventa; ?>">
															<button class="btn btn-sm btn-success">Imprimir factura</button>
														</a>
													</div>
												</div>

											</div>
											<div id="ventas_<?php echo $v->idventa; ?>" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
												<div class="card-body">
													<table class="table table-striped table-bordered">
														<thead>
															<tr>
																<td>PRODUCTO</td>
																<td>PRESENTACION</td>
																<td>TIPO DE VENTA</td>
																<td>CANTIDAD</td>
																<td>SUB TOTAL</td>
															</tr>
														</thead>
														<tbody>
															<?php foreach ($v->listav as $l) { ?>
																<tr>

																	<td><?php echo $l->presentaciones->productos->producto; ?>
																	</td>
																	<td><?php echo $l->presentaciones->presentacion; ?>
																	</td>
																	<td><?php echo $l->tipoventa; ?>
																	</td>
																	<td><?php echo $l->cantidad; ?>
																	</td>
																	<td><?php echo "$" . number_format($l->subtotal); ?>
																	</td>
																</tr>
															<?php } ?>
														</tbody>
													</table>
													<?php if (count($v->abonos) > 0) { ?>
														<div class="row">
															<div class="col-md-12">
																<h5>ABONOS</h5>
																<table class="table table-striped table-bordered">
																	<thead>
																		<tr>
																			<td>NO.</td>
																			<td>MONTO</td>
																			<td>FECHA</td>
																		</tr>
																	</thead>
																	<tbody>
																		<?php $i = 1;
																		foreach ($v->abonos as $a) { ?>
																			<tr>

																				<td><?php echo $i; ?>
																				</td>
																				<td><?php echo "$" . number_format($a->monto); ?>
																				</td>
																				<td><?php echo $a->fecha; ?>
																				</td>
																			</tr>
																		<?php $i++;
																		} ?>
																	</tbody>
																</table>
															</div>
														</div>
													<?php } ?>

												</div>
											</div>
										<?php } ?>


									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<script src="<?php echo base_url(); ?>assets/develop/js/compras_cliente.js" type="module">
</script>


<?php require 'layaout/footer.php';
