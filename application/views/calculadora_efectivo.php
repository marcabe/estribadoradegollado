<?php require 'layaout/head.php'; ?>
<!--  menu -->
<?php require 'layaout/menu.php'; ?>
<!-- /menu -->

<!-- top navigation -->
<?php require 'layaout/cabecera.php'; ?>
<!-- /top navigation -->

<!-- page content -->
<style>

</style>
<div class="right_col" role="main">
    <div class="">

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12  ">
                <form action="impresion_calculadora" target="_blank" method="POST">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><i class="fas fa-users fa-2x"></i></i> Modulo Calculadora / <label>Calculadora de Efectivo</label> <button class="btn btn-success btn-sm">Imprimir</button></h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content tituloSistema">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <h2>Total general: <label id="total_general">$0 </label><input id="total_input" name="t_gral" hidden></h2>
                                </div>
                                <div class="col-4 offset-1 text-center">
                                    <h5>Lista de Billetes</h5>
                                    <table class="table table_hover table_responsive">
                                        <thead>
                                            <tr>
                                                <td width="40%">Billetes</td>
                                                <td>Cant.</td>
                                                <td>Total</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>$1,000 x</td>
                                                <td><input type="number" step="1" min=0 class="form-control form-control-sm " name="b_1000" id=billete_1000 value=0></td>
                                                <td><input type="text" class="form-control form-control-sm billetes" id="t_1000" name="t_1000" readonly value=0></td>
                                            </tr>
                                            <tr>
                                                <td>$500 x</td>
                                                <td><input type="number" step="1" min=0 class="form-control form-control-sm " name="b_500" id=billete_500 value=0></td>
                                                <td><input type="text" class="form-control form-control-sm billetes" id="t_500" name="t_500" value=0 readonly></td>
                                            </tr>
                                            <tr>
                                                <td>$200 x</td>
                                                <td><input type="number" step="1" min=0 class="form-control form-control-sm " name="b_200" id=billete_200 value=0></td>
                                                <td><input class="form-control form-control-sm billetes" id="t_200" value=0 name="t_200" readonly></td>
                                            </tr>
                                            <tr>
                                                <td>$100 x</td>
                                                <td><input type="number" step="1" min=0 class="form-control form-control-sm " name="b_100" id=billete_100 value=0></td>
                                                <td><input class="form-control form-control-sm billetes" id="t_100" value=0 name="t_100" readonly></td>
                                            </tr>
                                            <tr>
                                                <td>$50 x</td>
                                                <td><input type="number" step="1" min=0 class="form-control form-control-sm " name="b_50" id=billete_50 value=0></td>
                                                <td><input class="form-control form-control-sm billetes" id="t_50" value=0 name="t_50" readonly></td>
                                            </tr>
                                            <tr>
                                                <td>$20 x</td>
                                                <td><input type="number" step="1" min=0 class="form-control form-control-sm " name="b_20" id=billete_20 value=0></td>
                                                <td><input class="form-control form-control-sm billetes" id="t_20" value=0 name="t_20" readonly></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">Total<input id="total_billete_input" name="tb_gral" hidden> </td>
                                                <td id="total_billetes" data-totalb=0>$0></td>
                                            </tr>
                                        </tbody>

                                    </table>
                                </div>
                                <div class="col-4 offset-2 text-center">
                                    <h5>Lista de Monedas</h5>
                                    <table class="table table_hover table_responsive">
                                        <thead>
                                            <tr>
                                                <td>Monedas</td>
                                                <td>Cant.</td>
                                                <td>Total</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>$10 x</td>
                                                <td><input type="number" step="1" min=0 class="form-control form-control-sm" name='m_10' id=moneda_10 value="0"></td>
                                                <td><input class="form-control form-control-sm monedas" id="t_10" name='tm_10' value=0 readonly></td>
                                            </tr>
                                            <tr>
                                                <td>$5 x</td>
                                                <td><input type="number" step="1" min=0 class="form-control form-control-sm" name='m_5' id=moneda_5 value="0"></td>
                                                <td><input class="form-control form-control-sm monedas" id="t_5" name='tm_5' value=0 readonly></td>
                                            </tr>
                                            <tr>
                                                <td>$2 x</td>
                                                <td><input type="number" step="1" min=0 class="form-control form-control-sm" name='m_2' id=moneda_2 value="0"></td>
                                                <td><input class="form-control form-control-sm monedas" id="t_2" name='tm_2' value=0 readonly></td>
                                            </tr>
                                            <tr>
                                                <td>$1 x</td>
                                                <td><input type="number" step="1" min=0 class="form-control form-control-sm" name='m_1' id=moneda_1 value="0"></td>
                                                <td><input class="form-control form-control-sm monedas" id="t_1" name='tm_1' value=0 readonly></td>
                                            </tr>
                                            <tr>
                                                <td>$.50 x</td>
                                                <td><input type="number" step="1" min=0 class="form-control form-control-sm" name='m_050' id=moneda_05 value="0"></td>
                                                <td><input class="form-control form-control-sm monedas" id="t_05" name='tm_050' value=0 readonly></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">Total <input id="total_moneda_input" name="tm_gral" hidden></td>
                                                <td id="total_monedas" data-totalm=0>$0 </td>
                                            </tr>
                                        </tbody>

                                    </table>
                                </div>

                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /page content -->

    <?php require 'layaout/footer.php'; ?>
    <script>
        $("#billete_1000").on("change", function() {
            let cant = $(this).val();
            let total_billetes = 0;
            $("#t_1000").val(accounting.formatMoney(cant * 1000));
            $(".billetes ").each(function(i, val) {
                total_billetes += accounting.unformat($(this).val());
            });
            $("#total_billetes").text(accounting.formatMoney(total_billetes));
            $("#total_billetes").attr("data-totalb", total_billetes);
            let total_general = parseFloat($("#total_billetes").attr("data-totalb")) + parseFloat($("#total_monedas").attr("data-totalm"));
            $("#total_general").text(accounting.formatMoney(total_general));

            $("#total_input").val(accounting.formatMoney(total_general));
            $("#total_billete_input").val(accounting.formatMoney(total_billetes));
        });

        $("#billete_500").on("change", function() {
            let cant = $(this).val();
            let total_billetes = 0;
            $("#t_500").val(accounting.formatMoney(cant * 500));
            $(".billetes ").each(function(i, val) {
                total_billetes += accounting.unformat($(this).val());
            });
            $("#total_billetes").text(accounting.formatMoney(total_billetes));
            $("#total_billetes").attr("data-totalb", total_billetes);
            let total_general = parseFloat($("#total_billetes").attr("data-totalb")) + parseFloat($("#total_monedas").attr("data-totalm"));
            $("#total_general").text(accounting.formatMoney(total_general));

            $("#total_input").val(accounting.formatMoney(total_general));
            $("#total_billete_input").val(accounting.formatMoney(total_billetes));
        });



        $("#billete_200").on("change", function() {
            let cant = $(this).val();
            let total_billetes = 0;
            $("#t_200").val(accounting.formatMoney(cant * 200));
            $(".billetes ").each(function(i, val) {
                total_billetes += accounting.unformat($(this).val());
            });
            $("#total_billetes").text(accounting.formatMoney(total_billetes));
            $("#total_billetes").attr("data-totalb", total_billetes);
            let total_general = parseFloat($("#total_billetes").attr("data-totalb")) + parseFloat($("#total_monedas").attr("data-totalm"));
            $("#total_general").text(accounting.formatMoney(total_general));

            $("#total_input").val(accounting.formatMoney(total_general));
            $("#total_billete_input").val(accounting.formatMoney(total_billetes));
        });


        $("#billete_100").on("change", function() {
            let cant = $(this).val();
            let total_billetes = 0;
            $("#t_100").val(accounting.formatMoney(cant * 100));
            $(".billetes ").each(function(i, val) {
                total_billetes += accounting.unformat($(this).val());
            });
            $("#total_billetes").text(accounting.formatMoney(total_billetes));
            $("#total_billetes").attr("data-totalb", total_billetes);
            let total_general = parseFloat($("#total_billetes").attr("data-totalb")) + parseFloat($("#total_monedas").attr("data-totalm"));
            $("#total_general").text(accounting.formatMoney(total_general));

            $("#total_input").val(accounting.formatMoney(total_general));
            $("#total_billete_input").val(accounting.formatMoney(total_billetes));
        });

        $("#billete_50").on("change", function() {
            let cant = $(this).val();
            let total_billetes = 0;
            $("#t_50").val(accounting.formatMoney(cant * 50));
            $(".billetes ").each(function(i, val) {
                total_billetes += accounting.unformat($(this).val());
            });
            $("#total_billetes").text(accounting.formatMoney(total_billetes));
            $("#total_billetes").attr("data-totalb", total_billetes);
            let total_general = parseFloat($("#total_billetes").attr("data-totalb")) + parseFloat($("#total_monedas").attr("data-totalm"));
            $("#total_general").text(accounting.formatMoney(total_general));

            $("#total_input").val(accounting.formatMoney(total_general));
            $("#total_billete_input").val(accounting.formatMoney(total_billetes));
        });


        $("#billete_20").on("change", function() {
            let cant = $(this).val();
            let total_billetes = 0;
            $("#t_20").val(accounting.formatMoney(cant * 20));
            $(".billetes ").each(function(i, val) {
                total_billetes += accounting.unformat($(this).val());
            });
            $("#total_billetes").text(accounting.formatMoney(total_billetes));
            $("#total_billetes").attr("data-totalb", total_billetes);
            let total_general = parseFloat($("#total_billetes").attr("data-totalb")) + parseFloat($("#total_monedas").attr("data-totalm"));
            $("#total_general").text(accounting.formatMoney(total_general));

            $("#total_input").val(accounting.formatMoney(total_general));
            $("#total_billete_input").val(accounting.formatMoney(total_billetes));
        });


        $("#moneda_10").on("change", function() {
            let cant = $(this).val();
            let total_billetes = 0;
            $("#t_10").val(accounting.formatMoney(cant * 10));
            $(".monedas ").each(function(i, val) {
                total_billetes += accounting.unformat($(this).val());
            });
            $("#total_monedas").text(accounting.formatMoney(total_billetes));
            $("#total_monedas").attr("data-totalm", total_billetes);
            let total_general = parseFloat($("#total_billetes").attr("data-totalb")) + parseFloat($("#total_monedas").attr("data-totalm"));
            $("#total_general").text(accounting.formatMoney(total_general));

            $("#total_input").val(accounting.formatMoney(total_general));
            $("#total_moneda_input").val(accounting.formatMoney(total_billetes));

        });


        $("#moneda_5").on("change", function() {
            let cant = $(this).val();
            let total_billetes = 0;
            $("#t_5").val(accounting.formatMoney(cant * 5));
            $(".monedas ").each(function(i, val) {
                total_billetes += accounting.unformat($(this).val());
            });
            $("#total_monedas").text(accounting.formatMoney(total_billetes));
            $("#total_monedas").attr("data-totalm", total_billetes);
            let total_general = parseFloat($("#total_billetes").attr("data-totalb")) + parseFloat($("#total_monedas").attr("data-totalm"));
            $("#total_general").text(accounting.formatMoney(total_general));

            $("#total_input").val(accounting.formatMoney(total_general));
            $("#total_moneda_input").val(accounting.formatMoney(total_billetes));
        });


        $("#moneda_2").on("change", function() {
            let cant = $(this).val();
            let total_billetes = 0;
            $("#t_2").val(accounting.formatMoney(cant * 2));
            $(".monedas ").each(function(i, val) {
                total_billetes += accounting.unformat($(this).val());
            });
            $("#total_monedas").text(accounting.formatMoney(total_billetes));
            $("#total_monedas").attr("data-totalm", total_billetes);
            let total_general = parseFloat($("#total_billetes").attr("data-totalb")) + parseFloat($("#total_monedas").attr("data-totalm"));
            $("#total_general").text(accounting.formatMoney(total_general));

            $("#total_input").val(accounting.formatMoney(total_general));
            $("#total_moneda_input").val(accounting.formatMoney(total_billetes));
        });


        $("#moneda_1").on("change", function() {
            let cant = $(this).val();
            let total_billetes = 0;
            $("#t_1").val(accounting.formatMoney(cant * 1));
            $(".monedas ").each(function(i, val) {
                total_billetes += accounting.unformat($(this).val());
            });
            $("#total_monedas").text(accounting.formatMoney(total_billetes));
            $("#total_monedas").attr("data-totalm", total_billetes);
            let total_general = parseFloat($("#total_billetes").attr("data-totalb")) + parseFloat($("#total_monedas").attr("data-totalm"));
            $("#total_general").text(accounting.formatMoney(total_general));

            $("#total_input").val(accounting.formatMoney(total_general));
            $("#total_moneda_input").val(accounting.formatMoney(total_billetes));
        });


        $("#moneda_05").on("change", function() {
            let cant = $(this).val();
            let total_billetes = 0;
            $("#t_05").val(accounting.formatMoney(cant * 0.5));
            $(".monedas ").each(function(i, val) {
                total_billetes += accounting.unformat($(this).val());
            });
            $("#total_monedas").text(accounting.formatMoney(total_billetes));
            $("#total_monedas").attr("data-totalm", total_billetes);
            let total_general = parseFloat($("#total_billetes").attr("data-totalb")) + parseFloat($("#total_monedas").attr("data-totalm"));
            $("#total_general").text(accounting.formatMoney(total_general));

            $("#total_input").val(accounting.formatMoney(total_general));
            $("#total_moneda_input").val(accounting.formatMoney(total_billetes));
        });
    </script>