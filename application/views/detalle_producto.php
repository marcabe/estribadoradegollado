<?php require 'layaout/head.php'; ?>
<!--  menu -->
<?php require 'layaout/menu.php'; ?>
<!-- /menu -->

<!-- top navigation -->
<?php require 'layaout/cabecera.php'; ?>
<!-- /top navigation -->

<div class="right_col" role="main">
	<div class="">

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12  ">
				<div class="x_panel">
					<div class="x_title">
						<h2><i class="fas fa-boxes fa-2x"></i> Modulo Productos / <label>Detalle</label></h2>
						<div class="clearfix"></div>
					</div>
					<div class="container mt-3">
						<div class="col-md-12 text-right">
							<button class="btn btn-success btn-sm" id="regresar"><i class="fas fa-arrow-circle-left"></i> Regresar</button>
						</div>
						<div class="col-md-6" id="mensaje" style="display: none;">
							<div class="alert alert-success alert-dismissible " role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
								</button>
								El registro fue editado correctamente
							</div>
						</div>
						<div class="col-md-8 offset-md-2 mt-4">
							<div class="row text-center">
								<div class="col-md-4">
									<p><i class="fas fa-box"></i> Producto: </p>
									<label><?php echo $producto[0]->producto;?></label>
								</div>
								<div class="col-md-4">
									<p><i class="fas fa-boxes"></i> Stock: </p>
									<label><?php echo $producto[0]->stock;?></label>
								</div>
								<div class="col-md-4">
									<p><i class="fas fa-balance-scale"></i> Medida:</p>
									<label id="med"><?php echo $producto[0]->medida->medida;?></label>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 text-center pt-5">
									<label>Lista de presentaciones</label>
								</div>
								<div class="col-md-12 pt-3">
									<table id="tblgeneral" class="table table-bordered">
										<thead>
										<tr>
											<td><i class="far fa-id-card"></i> Presentacion</td>
											<td><i class="fas fa-balance-scale"></i> Cantidad</td>
											<td><i class="fas fa-boxes"></i> Mayoreo</td>
											<td><i class="fas fa-boxes"></i> Menudeo</td>
											<td></td>
										</tr>
										</thead>
										<tbody id="lista">
										<?php foreach ($presentaciones as $p) { ?>
											<tr>
												<td id="presentacion<?php echo $p->idpresentacion; ?>"><?php echo $p->presentacion; ?></td>
												<td id="cantidad<?php echo $p->idpresentacion; ?>"><?php echo $p->cantidadminima." ".$producto[0]->medida->medida; ?></td>
												<td class="mayoreo" id="mayoreo<?php echo $p->idpresentacion; ?>"><?php echo $p->mayoreo; ?></td>
												<td class="menudeo" id="menudeo<?php echo $p->idpresentacion; ?>"><?php echo $p->menudeo; ?></td>
												<td class="text-center">
													<div class="dropdown">
														<button class="btn btn-success btn-sm dropdown-toggle configprospectos"
																data-toggle="dropdown" aria-haspopup="true"
																aria-expanded="false">
															<i class="fas fa-cogs"></i> Config
														</button>
														<div class="dropdown-menu listaconfig"
															 aria-labelledby="dropdownMenuButton">
														<span class="dropdown-item eliminar"
															  id="<?php echo $p->idpresentacion; ?>">
																  <i class="far fa-edit"></i> <?php echo ($p->status==1)? "Eliminar":"Activar";?>
															</span>
															<span class="dropdown-item editar"
																  id="<?php echo $p->idpresentacion; ?>">
																  <i class="far fa-edit"></i> Editar
															</span>
														</div>
													</div>
												</td>
											</tr>
										<?php } ?>
										</tbody>
									</table>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<script src="<?php echo base_url(); ?>assets/develop/js/detalle_producto.js"></script>

<?php require 'layaout/footer.php'; ?>
<script>
	$(document).ready(function () {
		$(".editar").on("click", function () {
			let idpresentacion = $(this).attr("id");
			$.ajax({
				url:"../../Presentaciones/get_by_id/"+idpresentacion,
				data:{},
				type:"POST",
				success: function (response){
					let presentacion = JSON.parse(response);
					let menudeo = accounting.formatMoney(presentacion.menudeo);
					let mayoreo = accounting.formatMoney(presentacion.mayoreo);
					let contenido = `
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12 text-center">
								Nueva presentación
							</div>
						</div>
						<div class="form-group row text-right">
							<label for="inputPassword" class="col-md-3 col-form-label">Presentación: </label>
							<div class="col-md-9">
								<div class="input-group input-group-sm">
									<div class="input-group-prepend">
										<div class="input-group-text"><i class="fas fa-box"></i></div>
									</div>
									<input type="text" class="form-control form-control-sm" id="presentacion" value="${presentacion.presentacion}">
								</div>
								<div class="text-left">
									<small id="msj_presentacion" class="msj_presentacion"></small>
								</div>
							</div>
						</div>
						<div class="form-group row text-right">
							<label for="inputPassword" class="col-md-3 col-form-label">Cant. min.: </label>
							<div class="col-md-9">
								<div class="input-group input-group-sm">
									<div class="input-group-prepend">
										<div class="input-group-text"><i class="fas fa-boxes"></i></div>
									</div>
									<input type="number" class="form-control form-control-sm" id="cantidadminima" value="${presentacion.cantidadminima}">
								</div>
								<div class="text-left">
									<small id="msj_cantidadminima" class="msj_cantidadminima"></small>
								</div>
							</div>
						</div>

						<div class="form-group row text-right">
							<label for="inputPassword" class="col-md-3 col-form-label">Menudeo: </label>
							<div class="col-md-9">
								<div class="input-group input-group-sm">
									<div class="input-group-prepend">
										<div class="input-group-text"><i class="fas fa-dollar-sign"></i></div>
									</div>
									<input type="text" class="form-control form-control-sm" id="menudeo" value="${menudeo}">
								</div>
								<div class="text-left">
									<small id="msj_menudeo" class="msj_menudeo"></small>
								</div>
							</div>
						</div>

						<div class="form-group row text-right">
							<label for="inputPassword" class="col-md-3 col-form-label">Mayoreo: </label>
							<div class="col-md-9">
								<div class="input-group input-group-sm">
									<div class="input-group-prepend">
										<div class="input-group-text"><i class="fas fa-dollar-sign"></i></div>
									</div>
									<input type="text" class="form-control form-control-sm" id="mayoreo" value="${mayoreo}">
								</div>
								<div class="text-left">
									<small id="msj_mayoreo" class="msj_mayoreo"></small>
								</div>
							</div>
						</div>
					</div>`;
					$.confirm({
						title: '<i class="fas fa-robot"></i> Mensaje del sistema',
						content: contenido,
						draggable: true,
						buttons: {
							aceptar: {
								text: '<i class="far fa-thumbs-up"></i> Aceptar', // text for button
								btnClass: 'btn-success success-modal',
								action: function () {
									let avanza = 1;
									let presentacion = $("#presentacion").val();
									let cantidadminima = $("#cantidadminima").val();
									let mayoreo = $("#mayoreo").val();
									let menudeo = $("#menudeo").val();

									if (presentacion == '') {
										$("#msj_presentacion").html("Debe ingresar el nombre del prospecto");
										$("#presentacion").css({"background": "#EFD3D2"});
										$("#msj_presentacion").show();
										avanza = 0;
									}

									if (cantidadminima == 0) {
										$("#msj_cantidadminima").html("Debe ingresar el nombre del prospecto");
										$("#cantidadminima").css({"background": "#EFD3D2"});
										$("#msj_cantidadminima").show();
										avanza = 0;
									}

									if (mayoreo == '') {
										$("#msj_mayoreo").html("Debe ingresar el nombre del prospecto");
										$("#mayoreo").css({"background": "#EFD3D2"});
										$("#msj_mayoreo").show();
										avanza = 0;
									}

									if (menudeo == '') {
										$("#msj_menudeo").html("Debe ingresar el nombre del prospecto");
										$("#menudeo").css({"background": "#EFD3D2"});
										$("#msj_menudeo").show();
										avanza = 0;
									}
									if (avanza == 1) {
										$.ajax({
											url: '../../Presentaciones/update/'+idpresentacion,
											data: {
												presentacion: presentacion,
												cantidadminima: cantidadminima,
												menudeo: accounting.unformat(menudeo),
												mayoreo: accounting.unformat(mayoreo),
											},
											type: 'POST',
											success: function (response) {
												$("#presentacion"+idpresentacion).text(presentacion);
												$("#cantidad"+idpresentacion).text(cantidadminima+" "+$("#med").text());
												$("#menudeo"+idpresentacion).text(menudeo);
												$("#mayoreo"+idpresentacion).text(mayoreo);
												$("#mensaje").show();
												$("#mensaje").fadeOut(5000);
											}
										});
									} else {
										return 0;
									}
								}
							},
							cerrar: {
								text: '<i class="fas fa-times"></i> Cerrar', // text for button
								btnClass: 'btn-danger', // multiple classes.
								action: function () {
								}
							}
						}
					});
				}
			});

		});

	});

</script>
