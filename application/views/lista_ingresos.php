<?php require 'layaout/head.php'; ?>
<!--  menu -->
<?php require 'layaout/menu.php'; ?>
<!-- /menu -->

<!-- top navigation -->
<?php require 'layaout/cabecera.php'; ?>
<!-- /top navigation -->

<!-- page content -->
<style>

</style>
<div class="right_col" role="main">
	<div class="">

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12  ">
				<div class="x_panel">
					<div class="x_title">
						<h2><i class="fas fa-cart-arrow-down fa-2x"></i> Modulo Ingresos / <label>Lista de Ingresos</label>
						</h2>
						</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content tituloSistema">
						<div class="row">
							<div class="col-12 text-left">
								<button class="btn btn-sm btn-success" id="nuevo_gasto">Nuevo Ingreso</button>
							</div>
						</div>
						<div class="row mt-5">
							<div class="col-md-12">
								<table id="tblgeneral" class="table table-bordered" id="tblgeneral">
									<thead>
										<tr>
											<td><i class="far fa-id-card"></i> No.</td>
											<td><i class="fa fa-phone"></i> Motivo</td>
											<td><i class="fa fa-phone"></i> Fecha</td>
											<td><i class="far fa-envelope"></i> Monto</td>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($ingresos as $i) { ?>
											<tr>
												<td><?php echo $i->idIngreso; ?></td>
												<td><?php echo $i->motivo; ?></td>
												<td><?php echo $i->fecha; ?></td>
												<td>$ <?php echo number_format($i->ingreso); ?></td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<!--<script src="<?php echo base_url(); ?>assets/develop/js/lista_ventas.js" type="module"></script>-->

<?php require 'layaout/footer.php'; ?>
<script>
	let ingresos = $('#tblgeneral').DataTable({
		"language": {
			"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
		},
		columns: [{
				data: "no_ingreso"
			},
			{
				data: "motivo"
			},
			{
				data: "monto"
			},
			{
				data: "fecha"
			}
		]

	});


	$(document).on("blur", "#monto", function() {
		$(this).val(accounting.formatMoney($("#monto").val()));
	});

	$("#nuevo_gasto").on("click", function() {

		let contenido = `
			<div class="container-fluid">
				<div class="form-group row text-center">
					<div class="col-md-12 text-center">
						<label for="inputPassword" class="col-form-label">Monto: </label>
						<div class="input-group input-group-sm">
							<input type="text" class="form-control form-control-sm" id="monto">
						</div>
					</div>
				</div>
				<div class="form-group row text-right">
					
				<div class="col-md-12 text-center">
					Motivo 
						<textarea class="form_control" id="motivo"></textarea>
					</div>
				</div>
				
			</div>`;
		$.confirm({
			title: '<i class="fas fa-robot"></i> Mensaje del sistema',
			content: contenido,
			draggable: true,
			buttons: {
				aceptar: {
					text: '<i class="far fa-thumbs-up"></i> Aceptar', // text for button
					btnClass: 'btn-success success-modal',
					action: function() {
						let motivo = $("#motivo").val();
						let monto = $("#monto").val();
						if (motivo == '' || monto == '') {
							alert("Debe ingresar todos los campos");
							return 0;
						}
						$.ajax({
							url: 'insert',
							data: {
								ingreso: accounting.unformat(monto),
								motivo: motivo,
								status: 1,
							},
							type: 'POST',
							dataType: "json",
							success: function(response) {
								ingresos.clear().draw();

								let ingreso = response;
								console.log(ingreso);
								$.each(ingreso, function(i, e) {
									var rowNode = ingresos
										.row.add(e)
										.draw()
										.node();
								});
							}
						});

					}
				},
				cerrar: {
					text: '<i class="fas fa-times"></i> Cerrar', // text for button
					btnClass: 'btn-danger', // multiple classes.
					action: function() {}
				}
			}
		});
	});
</script>