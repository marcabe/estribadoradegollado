<?php require 'layaout/head.php'; ?>
<!--  menu -->
<?php require 'layaout/menu.php'; ?>
<!-- /menu -->

<!-- top navigation -->
<?php require 'layaout/cabecera.php'; ?>
<!-- /top navigation -->

<!-- page content -->
<style>

</style>
<div class="right_col" role="main">
	<div class="">

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12  ">
				<div class="x_panel">
					<div class="x_title">
						<h2><i class="fas fa-cart-arrow-down fa-2x"></i> Modulo entradas / <label>Nueva entrada</label></h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content tituloSistema">
						<div class="row">
							<div class="col-12 text-right">
								<div class="alert alert-danger" role="alert" id="msj_alerta" style="display: none;">
								</div>
							</div>
							<div class="col-12 text-right">
								<div class="alert alert-success" role="alert" id="msj_hecho" style="display: none;">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-1">
								<div class="form-group">
									<label for="exampleFormControlInput1">Entrada</label>
									<input type="text" class=" form-control form-control-sm"
										   value="<?php echo $noventa; ?>" readonly id="noventa">
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<label for="exampleFormControlInput1">Proveedor</label>
									<select class=" form-control form-control-sm" id="proveedor"></select>
									<div class="text-left">
										<small id="msj_proveedor" class="msj"></small>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<div class="form-group">
									<label for="exampleFormControlInput1">Fecha de compra</label>
									<input type="date" class=" form-control form-control-sm" id="fecha" value="<?php echo $hoy; ?>">
									<div class="text-left">
										<small id="msj_fecha" class="msj"></small>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<div class="form-group">
									<label for="exampleFormControlInput1">Credito</label>
									<div class="row">
										<div class="col-6">
											<div class="form-check">
												<input class="form-check-input" name="credito" type="radio" value="1" id="si">
												<label class="form-check-label" for="defaultCheck1">
													Sí
												</label>
											</div>
										</div>
										<div class="col-6">
											<div class="form-check">
												<input class="form-check-input" name="credito" type="radio" value="0" id="no">
												<label class="form-check-label" for="defaultCheck1">
													No
												</label>
											</div>
										</div>
										<div class="text-left">
											<small id="msj_check" class="msj"></small>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-3" style="display: none" id="diascreditodiv">
								<div class="form-group">
									<label for="exampleFormControlInput1">Dias/credito</label>
									<input type="text" class=" form-control form-control-sm" id="diascredito">
									<div class="text-left">
										<small id="msj_diascredito" class="msj"></small>
									</div>
								</div>
							</div>


						</div>
						<div class="row">
							<div class="col-sm-3">
								<div class="form-group">
									<label for="exampleFormControlInput1">Producto</label>
									<select class=" form-control form-control-sm" id="productos"></select>
									<div class="text-left">
										<small id="msj_producto" class="msj"></small>
									</div>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<label for="exampleFormControlInput1">Presentacion:</label>
									<select class=" form-control form-control-sm" id="presentacion">
										<option value="0">--- Selecciona una presentacion ---</option>
									</select>
									<div class="text-left">
										<small id="msj_presentacion" class="msj"></small>
									</div>
								</div>
							</div>
							<div class="col-sm-1">
								<div class="form-group">
									<label for="exampleFormControlInput1">Cantidad:</label>
									<input type="number" min="1" class=" form-control form-control-sm" id="cantidad">
									<div class="text-left">
										<small id="msj_cantidad" class="msj"></small>
									</div>
								</div>
							</div>

							<div class="col-sm-2">
								<div class="form-group">
									<label for="exampleFormControlInput1">Subtotal:</label>
									<input class=" form-control form-control-sm" id="subtotal" >
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<button class="btn btn-success btn-sm" id="agregar"><i class="fas fa-plus-circle"></i>
									Agregar
								</button>
							</div>
							<div class="col-sm-6 text-right">
								<h1 id="total">0</h1>
							</div>
							<div class="col-12 text-right">
								<button class="btn btn-success btn-sm" id="finalizar"><i class="fas fa-arrow-circle-right"></i>
									Finalizar entrada
								</button>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12 text-center">
								<label>Lista de productos</label>
							</div>
							<div class="col-sm-12">
								<table id="tblgeneral" class="table table-active table-bordered">
									<thead>
									<tr>
										<td>Produccto</td>
										<td>Presentacion</td>
										<td>Cantidad</td>
										<td>Subtotal</td>
										<td></td>
									</tr>
									</thead>
									<tbody id="listap">
									</tbody>
								</table>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<script src="<?php echo base_url(); ?>assets/develop/js/entradas.js"></script>

<?php require 'layaout/footer.php'; ?>

