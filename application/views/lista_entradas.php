<?php require 'layaout/head.php'; ?>
<!--  menu -->
<?php require 'layaout/menu.php'; ?>
<!-- /menu -->

<!-- top navigation -->
<?php require 'layaout/cabecera.php'; ?>
<!-- /top navigation -->

<!-- page content -->
<style>

</style>
<div class="right_col" role="main">
	<div class="">

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12  ">
				<div class="x_panel">
					<div class="x_title">
						<h2><i class="fas fa-boxes fa-2x"></i> Modulo Entradas / <label>Lista entradas</label></h2>
						</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content tituloSistema">
						<div class="row">
							<div class="col-12 text-right">
								<a href="../Entradas/reporte" target="_blank">
									<button class="btn btn-success btn-sm"><i class="fas fa-print"></i> Imprimir reporte</button>
								</a>
							</div>
						</div>

						<div class="row mt-5">
							<div class="col-md-6" id="mensaje" style="display: none;">
								<div class="alert alert-success alert-dismissible " role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
												aria-hidden="true">×</span>
									</button>
									Entrada agregada correctamente
								</div>
							</div>
							<div class="col-md-12">
								<table id="tblgeneral" class="table table-bordered">
									<thead>
									<tr>
										<td><i class="far fa-id-card"></i> Proveedor</td>
										<td><i class="fas fa-boxes"></i> Direccion</td>
										<td><i class="fas fa-boxes"></i> Total</td>
										<td><i class="fas fa-boxes"></i> Usuario</td>
										<td><i class="fas fa-boxes"></i> Fecha</td>
										<td></td>
									</tr>
									</thead>
									<tbody>
									<?php foreach ($entradas as $e) { ?>
										<tr style='color: <?php echo ($e->credito==1)? "red":"#73879C"; ?>'>
											<td><?php echo $e->proveedor->nombre; ?></td>
											<td><?php echo $e->proveedor->direccion; ?></td>
											<td class="costotbl"><?php echo $e->total; ?></td>
											<td><?php echo $e->usuario->nombre; ?></td>
											<td><?php echo $e->fecha; ?></td>
											<td class="text-center">
												<div class="dropdown">
													<button class="btn btn-success btn-sm dropdown-toggle configprospectos"
															data-toggle="dropdown" aria-haspopup="true"
															aria-expanded="false">
														<i class="fas fa-cogs"></i> Config
													</button>
													<div class="dropdown-menu listaconfig"
														 aria-labelledby="dropdownMenuButton">
														<?php if($e->credito==1){?>
															<span class="dropdown-item abonar"
																  id="<?php echo $e->identrada; ?>">
																  <i class="fas fa-eye"></i> Abonar/Liquidar
															</span>
														<?php } ?>
														<span class="dropdown-item imprimir"
															  id="<?php echo $e->identrada; ?>">
																  <i class="fas fa-eye"></i> Imprimir
														</span>
														<span class="dropdown-item eliminar"
															  id="<?php echo $e->identrada ?>">
																  <i class="fas fa-edit"></i> Eliminar
														</span>
													</div>
												</div>
											</td>
										</tr>
									<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<script src="<?php echo base_url(); ?>assets/develop/js/lista_entradas.js" type="module"></script>

<?php require 'layaout/footer.php'; ?>

