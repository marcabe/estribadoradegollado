<?php require 'layaout/head.php'; ?>
<!--  menu -->
<?php require 'layaout/menu.php'; ?>
<!-- /menu -->

<!-- top navigation -->
<?php require 'layaout/cabecera.php'; ?>
<!-- /top navigation -->

<!-- page content -->
<style>

</style>
<div class="right_col" role="main">
	<div class="">

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12  ">
				<div class="x_panel">
					<div class="x_title">
						<h2><i class="fas fa-boxes fa-2x"></i> Modulo Entradas / <label>Lista entradas</label></h2>
						</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content tituloSistema">
						<div class="row">
							<div class="col-12 text-right">
								<a href="../Entradas/reporte" target="_blank">
									<button class="btn btn-success btn-sm"><i class="fas fa-print"></i> Imprimir
										reporte</button>
								</a>
							</div>
						</div>

						<div class="row mt-5">
							<div class="col-md-6" id="mensaje" style="display: none;">
								<div class="alert alert-success alert-dismissible " role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
											aria-hidden="true">×</span>
									</button>
									Entrada agregada correctamente
								</div>
							</div>
							<div class="col-md-12">
								<table class="table table-striped table-hover" id="tbl_usuarios">
									<thead>
										<tr>
											<td>Usuario</td>
											<td>Tipo</td>
											<td>Nombre</td>
											<td>Status</td>
											<td></td>
										</tr>
									</thead>
									<tbody id="lista_usuarios">
										<?php foreach ($usuarios as $u) { ?>
										<tr class="usuario_fila"
											data-content="<?php echo $u->usuario;?>">
											<td><?php echo $u->usuario;?>
											</td>
											<td><?php echo ($u->rol==1)?'Administrador':'Personal';?>
											</td>
											<td><?php echo $u->nombre;?>
											</td>
											<td><?php echo ($u->status==0)?'<i class="fas fa-user-times stat_m" data-content='.$u->usuario.'></i>':'<i class="fas fa-user-check stat_m" data-content='.$u->usuario.'></i>';?>
											</td>
											<td>
												<div class="dropdown">
													<button
														class="btn btn-success btn-sm dropdown-toggle configprospectos"
														data-toggle="dropdown" aria-haspopup="true"
														aria-expanded="false">
														<i class="fas fa-cogs"></i> Config
													</button>
													<div class="dropdown-menu listaconfig"
														aria-labelledby="dropdownMenuButton">
														<span class="dropdown-item editar"
															id="<?php echo $u->usuario; ?>">
															<i class="fas fa-eye"></i> Editar
														</span>

													</div>
												</div>
											</td>
										</tr>
										<?php } ?>
									</tbody>
									<tfoot id="final">
										<tr>
											<td>Usuario</td>
											<td>Tipo</td>
											<td>Nombre</td>
											<td>Status</td>
											<td></td>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<script src="<?php echo base_url(); ?>assets/develop/js/lista_usuarios.js"
	type="module"></script>

<?php require 'layaout/footer.php';
