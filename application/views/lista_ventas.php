<?php require 'layaout/head.php'; ?>
<!--  menu -->
<?php require 'layaout/menu.php'; ?>
<!-- /menu -->

<!-- top navigation -->
<?php require 'layaout/cabecera.php'; ?>
<!-- /top navigation -->

<!-- page content -->
<style>

</style>
<div class="right_col" role="main">
	<div class="">

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12  ">
				<div class="x_panel">
					<div class="x_title">
						<h2><i class="fas fa-cart-arrow-down fa-2x"></i> Modulo Ventas / <label>Lista de ventas</label>
						</h2>
						</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content tituloSistema">
						<div class="row">
							<div class="col-8 text-left">
								<div class="form-row">
									<div class="form-group col-md-4">
										<select class="form-control form-control-sm" id="mes_f">
											<option value=1>Enero</option>
											<option value=2>Febrero</option>
											<option value=3>Marzo</option>
											<option value=4>Abril</option>
											<option value=5>Mayo</option>
											<option value=6>Junio</option>
											<option value=7>Julio</option>
											<option value=8>Agosto</option>
											<option value=9>Septiembre</option>
											<option value=10>Octubre</option>
											<option value=11>Noviembre</option>
											<option value=12>Diciembre</option>
										</select>
									</div>
									<div class="form-group col-md-4">
										<select class="form-control form-control-sm" id="anio_f">
											<option value=2019>2019</option>
											<option value=2020>2020</option>
											<option value=2021>2021</option>
											<option value=2022>2022</option>
										</select>
									</div>
									<div class="form-group col-md-4">
										<button class="btn btn-success btn-sm" id="filtrar">Filtrar</button>
									</div>
									<div class="col-12">
										<form class="row" method="POST" action="../Ventas/reporte_cierre" target="_blank">
											<div class="form-group col-md-6">
												<input class="form-control form-control-sm" type="date" name="fecha">
											</div>
											<div class="form-group col-md-6">
												<button class="btn btn-success btn-sm"><i class="fas fa-print"></i> Imprimir Corte</button>
											</div>
										</form>
									</div>

								</div>
							</div>
							<div class="col-4 text-right">
								<a href="../Ventas/reporte" target="_blank">
									<button class="btn btn-success btn-sm"><i class="fas fa-print"></i> Imprimir
										reporte</button>
								</a>
							</div>
						</div>
						<div class="row mt-5">
							<div class="col-md-12">
								<table id="tblgeneral" class="table table-bordered">
									<thead>
										<tr>
											<td><i class="far fa-id-card"></i> No. Venta</td>
											<td><i class="fa fa-phone"></i> Cliente</td>
											<td><i class="far fa-envelope"></i> Direccion</td>
											<td><i class="fas fa-box-open"></i> Credito</td>
											<td><i class="far fa-calendar-alt"></i> Total</td>
											<td><i class="far fa-calendar-alt"></i> Vendedor</td>
											<td><i class="far fa-calendar-alt"></i> Fecha</td>
											<td></td>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($ventas as $v) { ?>
											<tr style='color: <?php echo ($v->credito == 1) ? "red" : "#73879C"; ?>'>
												<td><?php echo $v->idventa; ?>
												</td>
												<td><?php echo $v->clie->nombre; ?>
												</td>
												<td><?php echo $v->direccion; ?>
												</td>
												<td><?php echo ($v->credito == 1) ? "Si" : "No"; ?>
												</td>
												<td class="totalv"><?php echo $v->total; ?>
												</td>
												<td><?php echo $v->usu->nombre; ?>
												</td>
												<td><?php echo $v->fecha; ?>
												</td>
												<td>
													<div class="dropdown">
														<button class="btn btn-success btn-sm dropdown-toggle configprospectos" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
															<i class="fas fa-cogs"></i> Config
														</button>
														<div class="dropdown-menu listaconfig" aria-labelledby="dropdownMenuButton">
															<?php if ($v->credito == 1) { ?>
																<span class="dropdown-item abonar" id="<?php echo $v->idventa; ?>">
																	<i class="fas fa-eye"></i> Abonar/Liquidar
																</span>
															<?php } ?>
															<span class="dropdown-item imprimir" id="<?php echo $v->idventa; ?>">
																<i class="fas fa-eye"></i> Imprimir
															</span>
															<span class="dropdown-item eliminar" id="<?php echo $v->idventa; ?>">
																<i class="fa fa-trash"></i> Eliminar
															</span>
														</div>

													</div>
												</td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<script src="<?php echo base_url(); ?>assets/develop/js/lista_ventas.js" type="module"></script>

<?php require 'layaout/footer.php'; ?>
<script>
	$("#anio_f").val(<?php echo $anio; ?>);
	$("#mes_f").val(<?php echo $mes; ?>);
</script>