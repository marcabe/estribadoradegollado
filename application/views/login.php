<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Inventario E.sDegollado </title>

	<!-- Bootstrap -->
	<link href="<?php echo base_url() ?>assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="<?php echo base_url() ?>assets/vendors/fontawesome-f/css/all.css" rel="stylesheet">
	<!-- NProgress -->
	<link href="<?php echo base_url() ?>assets/vendors/nprogress/nprogress.css" rel="stylesheet">
	<!-- Custom Theme Style -->
	<link href="<?php echo base_url() ?>assets/build/css/custom.min.css" rel="stylesheet">
	<!--Css general -->
	<link href="<?php echo base_url() ?>assets/develop/css/general.css" rel="stylesheet">

	<script src="<?php echo base_url() ?>assets/build/js/jquery.min.js"></script>
	<script src="<?php echo base_url() ?>assets/build/js/bootstrap.min.js"></script>

</head>
<body class="login">
<div>
	<a class="hiddenanchor" id="signup"></a>
	<a class="hiddenanchor" id="signin"></a>
	<input id="mes" value="<?php echo date('m');?>" hidden>
	<input id="anio" value="<?php echo date('Y');?>" hidden>
	<div class="login_wrapper">
		<div class="animate form login_form">
			<section class="login_content pl-5 pr-5 pb-5">
				<div>
					<h1>Inicio de Sesión</h1>
					<div>
						<div class="form-group text-left">
							<label for="exampleFormControlInput1">Usuario</label>
							<div class="input-group input-group-sm">
								<div class="input-group-prepend">
									<div class="input-group-text"><i class="far fa-user"></i></div>
								</div>
								<input type="text" class="form-control form-control-sm" id="usuario"
									   placeholder="Ingresa su correo">
							</div>
							<div>
								<small id="msj_usuario"></small>
							</div>
						</div>
					</div>
					<div>
						<div class="form-group text-left">
							<label for="exampleFormControlInput1">Contraseña</label>
							<div class="input-group input-group-sm">
								<div class="input-group-prepend">
									<div class="input-group-text"><i class="fas fa-key"></i></div>
								</div>
								<input type="password" class="form-control form-control-sm" id="clave"
									   placeholder="Ingresa su contraseña">
							</div>
							<div>
								<small id="msj_clave"></small>
							</div>
						</div>
					</div>
					<div>
						<button id="ingresar" class="btn btn-success btn-sm"><i class="far fa-paper-plane"></i> Ingresar</button>
					</div>

					<div class="clearfix"></div>

					<div class="separator text-center">


						<div class="clearfix"></div>
						<br/>

						<div>
							<img src="<?php echo base_url();?>assets/develop/images/logo.jpg" width="100%">
							<p class="mt-3">©2020 Desarrollado por Ing. Marco Antonio Cardona</p>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
<script src="<?php echo base_url(); ?>assets/develop/js/login.js"></script>
</body>
</html>
