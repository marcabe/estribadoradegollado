<?php require 'layaout/head.php'; ?>
<!--  menu -->
<?php require 'layaout/menu.php'; ?>
<!-- /menu -->

<!-- top navigation -->
<?php require 'layaout/cabecera.php'; ?>
<!-- /top navigation -->

<!-- page content -->
<style>

</style>
<div class="right_col" role="main">
	<div class="">

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12  ">
				<div class="x_panel">
					<div class="x_title">
						<h2><i class="fas fa-users fa-2x"></i></i> Modulo Vendedores / <label>Comisiones</label></h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content tituloSistema">
						<div class="row">
							<div class="col-12 text-left">
								<div class="form-row">
									<div class="form-group col-md-3">
										<select class=" form-control form-control-sm" id="vendedor"></select>
									</div>
									<div class="form-group col-md-3">
										<input class="form-control form-control-sm" type="date" id="fecha_inicial">
									</div>
									<div class="form-group col-md-3">
										<input class="form-control form-control-sm" type="date" id="fecha_final">
									</div>
									<div class="form-group col-md-3">
										<button class="btn btn-success btn-sm" id="filtrar">Filtrar</button>
									</div>
								</div>
							</div>
						</div>

						Lista de Vendedores
						<div class="row mt-5" id="lista_ventas">
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<script
	src="<?php echo base_url(); ?>assets/develop/js/vendedores_comisiones.js"
	type="module"></script>

<?php require 'layaout/footer.php'; ?>
<script>
	$(document).ready(function() {

	});
</script>