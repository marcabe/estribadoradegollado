<?php require 'layaout/head.php'; ?>
<!--  menu -->
<?php require 'layaout/menu.php'; ?>
<!-- /menu -->

<!-- top navigation -->
<?php require 'layaout/cabecera.php'; ?>
<!-- /top navigation -->

<div class="right_col" role="main">
	<div class="">

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12  ">
				<div class="x_panel">
					<div class="x_title">
						<h2><i class="fas fa-users fa-2x"></i></i> Modulo Clientes / <label>Editar cliente</label></h2>
						<div class="clearfix"></div>
					</div>
					<div class="container mt-3">
						<div class="row">
							<div class="col-md-12 text-left">
								<button class="btn btn-success btn-sm text-center" id="editar"><i
										class="far fa-save"></i> Editar
								</button>
							</div>
						</div>
						<input id="idcliente"
							value="<?php echo $cliente->idcliente; ?>"
							hidden>
						<div class="row mt-4">
							<div class="col-md-4">
								<div class="form-group">
									<label>Nombre(s) del cliente: </label>
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="far fa-user"></i></div>
										</div>
										<input type="text" class="form-control form-control-sm" id="nombre"
											value="<?php echo $cliente->nombre; ?>">
									</div>
									<div class="text-left">
										<small id="msj_nombre" class="msj_formulario"></small>
									</div>
								</div>
							</div>

							<div class="col-md-2">
								<div class="form-group">
									<label>Teléfono / Celular: </label>
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="fas fa-phone-alt"></i></div>
										</div>
										<input type="text" class="form-control form-control-sm" id="telefono"
											data-inputmask="'mask' : '(999) 999-99-99'"
											value="<?php echo $cliente->telefono; ?>">
									</div>
									<div class="text-left">
										<small id="msj_telefono" class="msj_formulario"></small>
									</div>
								</div>
							</div>

							<div class="col-md-3">
								<div class="form-group">
									<label>Razon Social: </label>
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="fas fa-building"></i></div>
										</div>
										<input type="text" class="form-control form-control-sm" id="razonsocial"
											value="<?php echo $cliente->razonsocial; ?>">
									</div>
									<div class="text-left">
										<small id="msj_razonsocial" class="msj_formulario"></small>
									</div>
								</div>
							</div>

							<div class="col-md-3">
								<div class="form-group">
									<label>RFC: </label>
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="far fa-id-card"></i></div>
										</div>
										<input type="text" class="form-control form-control-sm" id="rfc"
											value="<?php echo $cliente->rfc; ?>">
									</div>
									<div class="text-left">
										<small id="msj_rfc" class="msj_formulario"></small>
									</div>
								</div>
							</div>

							<div class="col-md-3">
								<div class="form-group">
									<label>Correo: </label>
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="far fa-envelope"></i></div>
										</div>
										<input type="text" class="form-control form-control-sm" id="correo"
											value="<?php echo $cliente->correo; ?>">
									</div>
									<div class="text-left">
										<small id="msj_correo" class="msj_formulario"></small>
									</div>
								</div>
							</div>


							<div class="col-md-3">
								<div class="form-group">
									<label>Dirección: </label>
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="fas fa-house-user"></i></div>
										</div>
										<input type="text" class="form-control form-control-sm" id="direccion"
											value="<?php echo $cliente->direccion; ?>">
									</div>
									<div class="text-left">
										<small id="msj_direccion" class="msj_formulario"></small>
									</div>
								</div>
							</div>

							<div class="col-md-2">
								<div class="form-group">
									<label>C.P: </label>
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="fas fa-house-user"></i></div>
										</div>
										<input type="text" class="form-control form-control-sm" id="cp"
											value="<?php echo $cliente->cp; ?>">
									</div>
									<div class="text-left">
										<small id="msj_direccion" class="msj_formulario"></small>
									</div>
								</div>
							</div>
							<div id="info_credito">

								<div class="col-md-2">
									<div class="form-group">
										<label>Credito: </label>
										<div class="input-group input-group-sm">
											<div class="input-group-prepend">
												<div class="input-group-text"><i class="fas fa-dollar-sign"></i></div>
											</div>
											<input type="text" class="form-control form-control-sm" id="credito"
												value="<?php echo $cliente->credito; ?>">
										</div>
										<div class="text-left">
											<small id="msj_credito" class="msj_formulario"></small>
										</div>
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label>Días de credito: </label>
										<div class="input-group input-group-sm">
											<div class="input-group-prepend">
												<div class="input-group-text"><i class="fas fa-dollar-sign"></i></div>
											</div>
											<select class="form-control form-control-sm" id="diascredito">
												<option value=0>Seleccione los dias</option>
												<option value=15>15 días</option>
												<option value=30>30 días</option>
											</select>
										</div>
										<div class="text-left">
											<small id="msj_diascredito" class="msj_formulario"></small>
										</div>
									</div>
								</div>
							</div>


						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label>Credito: </label>
								</div>
								<div class="row">
									<label class="col-md-6">
										<input type="radio" name="opcion_credito" value="Si" id="checksi"> Si
									</label>
									<label class="col-md-6">
										<input type="radio" name="opcion_credito" value="No" id="checkno"> No
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<script src="<?php echo base_url(); ?>assets/develop/js/editar_cliente.js">
</script>

<script>
	let credito_global = '';

	$("input[name=opcion_credito]").on("click", function() {
		if ($(this).val() == "Si") {
			$("#info_credito").css({
				"display": "contents"
			});
			credito_global = 1;
		} else {
			$("#info_credito").hide();
			credito_global = 0;
		}
	});

	<?php if ($cliente->status_credito==1) { ?>
	$("#checksi").prop('checked', true);
	$("#checkno").prop('checked', false);

	$("#info_credito").css({
		"display": "contents"
	});

	$("#credito").val( <?php echo $cliente->credito; ?> );
	$("#diascredito").val( <?php echo $cliente->diascredito; ?> );
	credito_global = 1;

	<?php } else { ?>
	$("#checksi").prop('checked', false);
	$("#checkno").prop('checked', true);
	$("#info_credito").css({
		"display": "none"
	});
	$("#credito").val(0);
	$("#diascredito").val(0);
	credito_global = 0;
	<?php } ?>


	$("#editar").on("click", function() {

		let nombre = $("#nombre").val();
		let telefono = $("#telefono").val();
		let razonsocial = $("#razonsocial").val();
		let rfc = $("#rfc").val();
		let correo = $("#correo").val();
		let direccion = $("#direccion").val();
		let cp = $("#cp").val();
		let credito = $("#credito").val();
		let diascredito = $("#diascredito").val();

		if (nombre == '') {
			$("#msj_nombre").html("Debe ingresar el nombre");
			$("#nombre").css({
				"background": "#EFD3D2"
			});
			$("#msj_nombre").show();
			return 0;
		}
		if (nombre.length <= 2) {
			$("#msj_nombre").html("El nombre ingresado debe tener 3 caracteres o más");
			$("#nombre").css({
				"background": "#EFD3D2"
			});
			$("#msj_nombre").show();
			return 0;
		}

		if (telefono == '') {
			$("#msj_telefono").html("Debe ingresar el telefono");
			$("#telefono").css({
				"background": "#EFD3D2"
			});
			$("#msj_telefono").show();
			return 0;
		}
		if (razonsocial == '') {
			$("#msj_razonsocial").html("Debe ingresar el correo");
			$("#razonsocial").css({
				"background": "#EFD3D2"
			});
			$("#msj_razonsocial").show();
			return 0;
		}

		if (direccion == '') {
			$("#msj_direccion").html("Debe ingresar la direccion");
			$("#direccion").css({
				"background": "#EFD3D2"
			});
			$("#msj_direccion").show();
			return 0;
		}

		if (credito_global == 1) {
			if (credito == '') {
				$("#msj_credito").html("Debe ingresar credito");
				$("#credito").css({
					"background": "#EFD3D2"
				});
				$("#msj_credito").show();
				return 0;
			}

			if (diascredito == 0) {
				$("#msj_diascredito").html("Debe ingresar los dias de credito credito");
				$("#diascredito").css({
					"background": "#EFD3D2"
				});
				$("#msj_diascredito").show();
				return 0;
			}
		} else {
			credito = 0;
			diascredito = 0;
		}


		$.confirm({
			title: '<i class="fas fa-robot"></i> Mensaje del sistema',
			content: '¿Estas a punto de editar la informacion del cliente, deseas continuar?',
			draggable: true,
			type: 'green',
			typeAnimated: true,

			buttons: {
				guardar: {
					text: '<i class="far fa-thumbs-up"></i> Si', // text for button
					btnClass: 'btn-success success-modal',
					action: function() {
						$.ajax({
							url: '../../Clientes/update/' + $("#idcliente").val(),
							data: {
								nombre: nombre,
								telefono: telefono,
								razonsocial: razonsocial,
								rfc: rfc,
								correo: correo,
								direccion: direccion,
								cp: cp,
								credito: accounting.unformat(credito),
								creditousado: 0,
								diascredito: diascredito,
								status: 1,
								status_credito: credito_global

							},
							type: 'POST',
							success: function(response) {
								if (response != 0) {
									window.location = "../../clientes/lista";

								}
							}
						});
					}
				},
				cerrar: {
					text: '<i class="fas fa-times"></i> No', // text for button
					btnClass: 'btn-danger', // multiple classes.
				}
			}
		});
	});
</script>

<?php require 'layaout/footer.php';
