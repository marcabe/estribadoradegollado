<?php require 'layaout/head.php'; ?>
<!--  menu -->
<?php require 'layaout/menu.php'; ?>
<!-- /menu -->

<!-- top navigation -->
<?php require 'layaout/cabecera.php'; ?>
<!-- /top navigation -->

<!-- page content -->
<style>

</style>
<div class="right_col" role="main">
	<div class="">

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12  ">
				<div class="x_panel">
					<div class="x_title">
						<h2><i class="fas fa-truck-moving fa-2x"></i></i> Modulo Proveedores / <label>Lista proveedores</label></h2>
						</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content tituloSistema">
						<div class="row">
							<div class="col-12 text-right">
								<a href="../Proveedores/reporte" target="_blank">
									<button class="btn btn-success btn-sm"><i class="fas fa-print"></i> Imprimir reporte</button>
								</a>
							</div>
						</div>
						<div class="row mt-5">
							<div class="col-md-12">
								<table id="tblgeneral" class="table table-bordered">
									<thead>
									<tr>
										<td><i class="far fa-id-card"></i> Nombre</td>
										<td><i class="fa fa-phone"></i> Telefono</td>
										<td><i class="far fa-envelope"></i> Razon Social</td>
										<td><i class="fas fa-box-open"></i> RFC</td>
										<td><i class="far fa-calendar-alt"></i> Correo</td>
										<td><i class="far fa-calendar-alt"></i> Direccion</td>
										<td><i class="far fa-calendar-alt"></i> Status</td>
										<td></td>
									</tr>
									</thead>
									<tbody>
									<?php foreach ($proveedores as $c) { ?>
										<tr>
											<td><?php echo $c->nombre; ?></td>
											<td><?php echo $c->telefono; ?></td>
											<td><?php echo $c->razonsocial; ?></td>
											<td><?php echo $c->rfc; ?></td>
											<td><?php echo $c->correo; ?></td>
											<td><?php echo $c->direccion; ?></td>
											<td><?php if ($c->status == 1) {
													echo "Activo";
												} elseif ($c->status == 2) {
													echo "Inactivo";
												} ?></td>
											<td>
												<div class="dropdown">
													<button class="btn btn-success btn-sm dropdown-toggle configprospectos"
															data-toggle="dropdown" aria-haspopup="true"
															aria-expanded="false">
														<i class="fas fa-cogs"></i> Config
													</button>
													<div class="dropdown-menu listaconfig"
														 aria-labelledby="dropdownMenuButton">
														<span class="dropdown-item editar"
															  id="<?php echo $c->idproveedor; ?>">
																  <i class="fas fa-eye"></i> Editar
															</span>
														<span class="dropdown-item eliminar"
																		 id="<?php echo $c->idproveedor; ?>">
																  <i class="fa fa-trash"></i> Eliminar
															</span>

													</div>
												</div>
											</td>
										</tr>
									<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<script src="<?php echo base_url(); ?>assets/develop/js/lista_proveedores.js" type="module"></script>

<?php require 'layaout/footer.php'; ?>
<script>
	$(document).ready(function () {

	});
</script>
