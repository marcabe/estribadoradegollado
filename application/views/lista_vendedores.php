<?php require 'layaout/head.php'; ?>
<!--  menu -->
<?php require 'layaout/menu.php'; ?>
<!-- /menu -->

<!-- top navigation -->
<?php require 'layaout/cabecera.php'; ?>
<!-- /top navigation -->

<!-- page content -->
<style>

</style>
<div class="right_col" role="main">
	<div class="">

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12  ">
				<div class="x_panel">
					<div class="x_title">
						<h2><i class="fas fa-users fa-2x"></i></i> Modulo Vendedores / <label>Lista Vendedores</label></h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content tituloSistema">
						<div class="row">
							<div class="col-md-12 text-right">
								<a href="../Clientes/reporte" target="_blank">
									<button class="btn btn-success btn-sm"><i class="fas fa-print"></i> Imprimir
										reporte</button>
								</a>
								<div class="alert alert-success" role="alert" id="msj_hecho" style="display: none;">
								</div>
							</div>
						</div>

						Lista de Vendedores
						<div class="row mt-5">
							<div class="col-md-12">
								<table id="tblgeneral" class="table table-bordered">
									<thead>
										<tr>
											<td><i class="far fa-id-card"></i> NOMBRE</td>
											<td></td>
											<td></td>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($vendedores as $v) { ?>
											<tr>
												<td><?php echo $v->nombre; ?>
												</td>
												<td><?php echo ($v->status == 0) ? '<i class="fas fa-user-times stat_m" data-content=' . $v->idvendedor . '></i>' : '<i class="fas fa-user-check stat_m" data-content=' . $v->idvendedor . '></i>'; ?>

												<td><?php echo $v->btn; ?>
												</td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<script src="<?php echo base_url(); ?>assets/develop/js/lista_vendedores.js" type="module"></script>

<?php require 'layaout/footer.php'; ?>
<script>
	$(document).ready(function() {
		$(".imprimir_co").on("click", function() {
			window.open("impresion_comisiones/" + $(this).attr("data-vendedor"), '_blank');
		});
	});
</script>