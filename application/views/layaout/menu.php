<div class="col-md-3 left_col">
	<div class="left_col scroll-view">
		<div class="navbar nav_title" style="border: 0;">
			<a href="index.html" class="site_title"><i class="fas fa-toolbox"></i> Sistema<span></span></a>
		</div>
		<div class="clearfix"></div>
		<br />
		<?php $ses = $this->session->userdata("edegollado"); ?>
		<!-- sidebar menu -->
		<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
			<div class="menu_section">
				<h3>Menu</h3>
				<ul class="nav side-menu">
					<?php if ($ses['rol'] == 3) { ?>
						<li><a><i class="fas fa-home"></i> Home <span class="fas fa-chevron-down"></span></a>
							<ul class="nav child_menu">
								<li><a href="<?php echo base_url() . 'home/' . date('m') . '/' . date('Y'); ?>">Dashboard</a>
								</li>
							</ul>
						</li>

						<li><a><i class="fas fa-users"></i> Usuarios <span class="fas fa-chevron-down"></span></a>
							<ul class="nav child_menu">
								<li><a href="<?php echo base_url() ?>usuarios/lista">Lista
										de usuarios</a></li>
								<li><a href="<?php echo base_url() ?>usuarios/nuevo">Nuevo
										usuario</a></li>
							</ul>
						</li>

						<li><a><i class="fas fa-users"></i> Vendedores <span class="fas fa-chevron-down"></span></a>
							<ul class="nav child_menu">
								<li><a href="<?php echo base_url() ?>vendedores/lista">Lista
										de vendedores</a></li>
								<li><a href="<?php echo base_url() ?>vendedores/nuevo">Nuevo
										vendedor</a></li>
								<li><a href="<?php echo base_url() ?>vendedores/comisiones">Comisiones
										Vendedores</a></li>
							</ul>
						</li>
					<?php } ?>
					<li><a><i class="fas fa-users"></i> Clientes <span class="fas fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							<li><a href="<?php echo base_url() ?>clientes/lista">Lista
									de clietes</a></li>
							<li><a href="<?php echo base_url() ?>clientes/nuevo">Nuevo
									cliente</a></li>
						</ul>
					</li>
					<li><a><i class="fas fa-users"></i> Gastos <span class="fas fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							<li><a href="<?php echo base_url() ?>gastos/lista">Lista
									de gatos</a></li>
						</ul>
					</li>
					<li><a><i class="fas fa-truck-moving"></i> Proveedores <span class="fas fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							<li><a href="<?php echo base_url() ?>proveedores/lista">Lista
									de proveedores</a></li>
							<li><a href="<?php echo base_url() ?>proveedores/nuevo">Nuevo
									proveedor</a></li>
						</ul>
					</li>
					<li><a><i class="fas fa-boxes"></i> Productos <span class="fas fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							<li><a href="<?php echo base_url() ?>productos/lista">Lista
									de productos</a></li>
							<li><a href="<?php echo base_url() ?>productos/nuevo">Nuevo
									producto</a></li>
						</ul>
					</li>
					<li><a><i class="fas fa-boxes"></i> Entradas <span class="fas fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							<li><a href="<?php echo base_url() ?>entradas/lista">Lista
									de entradas</a></li>
							<li><a href="<?php echo base_url() ?>entradas/nueva">Nueva
									entrada</a></li>
						</ul>
					</li>
					<li><a><i class="fas fa-cart-arrow-down" style="color: #00993A !important;"></i> Ventas <span class="fas fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							<li><a href="<?php echo base_url() ?>ventas/nueva_venta">Nueva
									venta</a></li>
							<li><a href="<?php echo base_url() ?>ventas/lista_ventas">Lista
									de ventas</a></li>
							<li><a href="<?php echo base_url() ?>ingresos/lista_ingresos">Otros Ingresos</a></li>
						</ul>
					</li>
					<li><a><i class="fas fa-cart-arrow-down"></i> Calculadora <span class="fas fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							<li><a href="<?php echo base_url() ?>app/calculadora_efectivo">Calculadora de efectivo</a></li>
						</ul>
					</li>
					<li><a><i class="fas fa-cogs"></i> Configuracion <span class="fas fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							<li><a href="<?php echo base_url() ?>configuracion/lista_configuraciones">Lista
									de configuraciones</a></li>

						</ul>
					</li>


				</ul>
			</div>


		</div>
		<!-- /sidebar menu -->

		<!-- /menu footer buttons -->
		<!-- /menu footer buttons -->
	</div>
</div>