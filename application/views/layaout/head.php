<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Sistema de Inventario EDegollado</title>

	<!-- Bootstrap -->
	<link href="<?php echo base_url() ?>assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="<?php echo base_url() ?>assets/vendors/fontawesome-f/css/all.css" rel="stylesheet">
	<!-- NProgress -->
	<link href="<?php echo base_url() ?>assets/vendors/nprogress/nprogress.css" rel="stylesheet">
	<!-- Custom Theme Style -->
	<link href="<?php echo base_url() ?>assets/build/css/custom.min.css" rel="stylesheet">
	<!--Css general -->
	<link href="<?php echo base_url() ?>assets/develop/css/general.css" rel="stylesheet">

	<script src="<?php echo base_url() ?>assets/build/js/jquery.min.js"></script>
	<script src="<?php echo base_url() ?>assets/build/js/bootstrap.min.js"></script>

	<link rel="stylesheet" href="<?php echo base_url() ?>assets/build/css/jquery-confirm.min.css">

	<link href="<?php echo base_url() ?>assets/build/css/jquery.dataTables.min.css" rel="stylesheet">

	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet">

	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
	<script src="<?php echo base_url() ?>assets/develop/js/detectai.js"></script>


</head>
<script>
	$(window).on("load", function() {
		$(".loader").fadeOut("slow");
		$(".loader1").fadeOut("slow");
	});
</script>
<div class="loader"></div>
<div class="loader1"></div>

<div id="bloquear"></div>

<div class="container" id="msjsys">
	<div class="row">
		<div class="col-md-12 text-center">
			<label id="lblmsntop">CARGANDO...</label>
		</div>
	</div>
</div>

<input type="text" id="rol" style="display: none;" value="<?php echo $this->session->userdata('rol') ?>">

<body class="nav-md">
	<div class="container body">
		<div class="main_container">