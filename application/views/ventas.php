<?php require 'layaout/head.php'; ?>
<!--  menu -->
<?php require 'layaout/menu.php'; ?>
<!-- /menu -->

<!-- top navigation -->
<?php require 'layaout/cabecera.php'; ?>
<!-- /top navigation -->

<!-- page content -->
<style>

</style>
<div class="right_col" role="main">
	<div class="">

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12  ">
				<div class="x_panel">
					<div class="x_title">
						<h2><i class="fas fa-cart-arrow-down fa-2x"></i> Modulo Ventas / <label>Nueva venta</label></h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content tituloSistema">
						<div class="row">
							<div class="col-12 text-right">
								<div class="alert alert-danger" role="alert" id="msj_alerta" style="display: none;">
								</div>
							</div>
							<div class="col-12 text-right">
								<div class="alert alert-success" role="alert" id="msj_hecho" style="display: none;">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-1">
								<div class="form-group">
									<label for="exampleFormControlInput1">No. Venta</label>
									<input type="text" class=" form-control form-control-sm" value="<?php echo $noventa; ?>" readonly id="noventa">
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<label for="exampleFormControlInput1">Cliente</label>
									<select class=" form-control form-control-sm" id="cliente"></select>
									<div class="text-left">
										<small id="msj_cliente" class="msj"></small>
									</div>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<label for="exampleFormControlInput1">Direccion</label>
									<input type="text" class=" form-control form-control-sm" id="direccion">
									<div class="text-left">
										<small id="msj_direccion" class="msj"></small>
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<div class="form-group">
									<label for="exampleFormControlInput1">Fecha</label>
									<input type="text" class=" form-control form-control-sm" value="<?php echo $hoy; ?>" readonly>
								</div>
							</div>
							<div class="col-sm-2">
								<div class="form-group">
									<label for="exampleFormControlInput1">Credito</label>
									<div class="row">
										<div class="col-6">
											<div class="form-check">
												<input class="form-check-input" name="credito" type="radio" value="1" id="si">
												<label class="form-check-label" for="defaultCheck1">
													Sí
												</label>
											</div>
										</div>
										<div class="col-6">
											<div class="form-check">
												<input class="form-check-input" name="credito" type="radio" value="0" id="no">
												<label class="form-check-label" for="defaultCheck1">
													No
												</label>
											</div>
										</div>
										<div class="text-left">
											<small id="msj_check" class="msj"></small>
										</div>
									</div>


								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3">
								<div class="form-group">
									<label for="exampleFormControlInput1">Producto</label>
									<select class=" form-control form-control-sm" id="productos"></select>
									<div class="text-left">
										<small id="msj_producto" class="msj"></small>
									</div>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<label for="exampleFormControlInput1">Presentacion:</label>
									<select class=" form-control form-control-sm" id="presentacion">
										<option value="0">--- Selecciona una presentacion ---</option>
									</select>
									<div class="text-left">
										<small id="msj_presentacion" class="msj"></small>
									</div>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<label for="exampleFormControlInput1" id="textopresentacion">Costo
										presentacion:</label>
									<input type="text" class="form-control form-control-sm" id="costopresentacionespecial" style="display: none;">

									<select class=" form-control form-control-sm" id="costopresentacion">
										<option value="0">--- Selecciona un costo ---</option>
									</select>
									<div class="text-left">
										<small id="msj_costopresentacion" class="msj"></small>
									</div>
								</div>
							</div>
							<div class="col-sm-1">
								<div class="form-group">
									<label for="exampleFormControlInput1">Cantidad:</label>
									<input type="number" min="1" class=" form-control form-control-sm" id="cantidad">
									<div class="text-left">
										<small id="msj_cantidad" class="msj"></small>
									</div>
								</div>
							</div>

							<div class="col-sm-2">
								<div class="form-group">
									<label for="exampleFormControlInput1">Subtotal:</label>
									<input class=" form-control form-control-sm" id="subtotal" readonly>
								</div>
							</div>
						</div>
						<?php
						$ses = $this->session->userdata('edegollado');
						if ($ses['rol'] == 1 || $ses['rol'] == 3) { ?>
							<div class="row">
								<div class="col-12 text-right">
									<div class="form-check">
										<input type="checkbox" class="form-check-input" id="pespecial">
										<label class="form-check-label" for="exampleCheck1">Precio especial</label>
									</div>
								</div>
							</div>
						<?php } ?>
						<div class="row">
							<div class="col-sm-3">
								<div class="form-group">
									<label for="exampleFormControlInput1">Vendedor</label>
									<select class=" form-control form-control-sm" id="vendedor"></select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<button class="btn btn-success btn-sm" id="agregar"><i class="fas fa-plus-circle"></i>
									Agregar
								</button>
							</div>
							<div class="col-sm-6 text-right">
								<h1 id="total">0</h1>
							</div>
							<div class="col-12 text-right">
								<button class="btn btn-success btn-sm" id="finalizar"><i class="fas fa-arrow-circle-right"></i>
									Finalizar venta
								</button>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12 text-center">
								<label>Lista de productos</label>
							</div>
							<div class="col-sm-12">
								<table id="tblgeneral" class="table table-active table-bordered">
									<thead>
										<tr>
											<td>Produccto</td>
											<td>Presentacion</td>
											<td>Tipo de venta</td>
											<td>Cantidad</td>
											<td>Precio Unitario</td>
											<td>Subtotal</td>
											<td></td>
										</tr>
									</thead>
									<tbody id="listap">
									</tbody>
								</table>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<script src="<?php echo base_url(); ?>assets/develop/js/ventas.js"></script>

<?php require 'layaout/footer.php';
